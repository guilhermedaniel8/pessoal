# UC3 - Definir Categoria (de tarefa)

## 1. Engenharia de Requisitos

### Formato Breve

O administrativo inicia a especificação de uma nova categoria de tarefa. O sistema solicita os dados necessários sobre a nova categoria de tarefa(i.e. identificador interno, descrição). O administrativo introduz os dados solicitados. O sistema apresenta as áreas de atividade e pede para se selecionar uma destas. O administrativo seleciona a AA pretendida. O sistema apresenta as competências técnicas referentes à área de atividade previamente selecionada, pedindo para se selecionar uma e para eleger se esta é ou não obrigatoria. O administrativo seleciona a competência técnica elegendo se esta é ou não obrigatoria.  A seleção de CT, descrita anteriormente repete-se até que todas as pretendidas tenham sido selecionadas pelo administrativo. O sistema valida e apresenta todos os dados ao administrativo, pedindo que os confirme. O administrativo confirma. O sistema regista os dados da nova categoria de tarefa e informa o administrativo do sucesso da operação.

### SSD
![UC3_SSD](UC3_SSD.svg)


### Formato Completo

#### Ator principal

Admnistrativo

#### Partes interessadas e seus interesses

* **Administrativo:** pretende especificar as categorias de tarefas para que possa posteriormente catalogar as várias tarefas prestadas.
* **Colaborador de Organização:** pretende fornecer a informação das categorias de tarefa catalogadas.
* **T4J:** pretende que a plataforma permita associar as categorias de tarefas às áreas de atividades e às tarefas.

#### Pré-condições

N/A

#### Pós-condições

A informação da categoria é registada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O administrativo inicia a especificação de uma nova categoria de tarefa. 
2. O sistema solicita os dados necessários (i.e. identificador interno, descrição). 
3. O administrativo introduz os dados solicitados.
4. O sistema apresenta as áreas de atividade e pede para selecionar uma.
5. O administrativo seleciona a área de atividade pretendida.
6. O sistema apresenta as competências técnicas referentes à área de atividade previamente selecionada, pedindo para selecionar uma e para eleger se esta é ou não obrigatória.
7. O administrativo seleciona a competência técnica pretendida e elege se esta é ou não obrigatoria.
8. Os passos 6 a 7 repetem-se até que todas as competências pretendidas estejam selecionadas e lhes tenha  sido atribuido se são ou não obrigatorias.
9. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. 
10. O administrativo confirma. 
11. O sistema regista os dados e informa o administrativo do sucesso da operação.

#### Extensões (ou fluxos alternativos)

*a. O administrativo solicita o cancelamento da definição da categoria de tarefa.

> O caso de uso termina.

4a. O sistema deteta que o administrativo apenas possui uma área de atividade.
>    1. O sistema alerta o administrativo para o facto de só existir uma área de atividade definida.
>    2. O sistema questiona o administrativo se deseja definir uma área de atividada, para posterior seleção.
>
        > 2a. O administrativo não introduz areas de atividade.
            O sistema assume a área de atividade conhecida e informa o administrativo disso.
        > 2b. O administrativo só introduz uma area de atividade.
            O sistema assume a área de atividade introduzida e informa o administrativo disso.
            O sistema avança para o passo 6.
        > 2c. O administrativo introduz várias área de atividade.
            O sistema volta ao início do passo 4.

4b. O sistema deteta que o administrativo não possui uma área de atividade. 
>    1. O sistema alerta o administrativo para o facto não existir uma área de atividade definida.
>    2. O sistema questiona o administrativo se deseja definir uma área de atividada, para posterior seleção.
>      
        > 2a. O administrativo não introduz areas de atividade o caso de uso termina.
        > 2b. O administrativo só introduz uma area de atividade.
            O sistema assume a área de atividade conhecida e informa o administrativo disso.
            O sistema avança para o passo 6.
        > 2c. O administrativo introduz várias área de atividade.
            O sistema volta ao início do passo 4.

6a. O sistema deteta que o administrativo apenas possui uma competência
>    1. O sistema alerta o administrativo para o facto de só existir uma competência técnica definida.
>    2. O sistema questiona o administrativo se deseja definir uma nova competência técnica, para posterior seleção.
>
        > 1a. O administrativo não introduz nova competência técnica.
            O sistema assume a competência técnica conhecida e informa o administrativo disso.
        > 1b. O administrativo introduz novas competências técnicas.
            O sistema volta ao início do passo 6.

6b. O sistema deteta que o administrativo não possui competências.
>    1. O sistema alerta o administrativo para o facto de não existir competências técnicas definidas.
>    2. O sistema questiona o administrativo se deseja definir uma competência técnica, para posterior seleção.
>
        > 2a. O administrativo não introduz novas competências tecnicas o caso de uso termina. 
        > 2b. O administrativo introduz novas competências tecnicas.
            O sistema volta ao inicio do passo 6.

6c. O sistema deteta que a competência técnica escolhida já tinha sido introduzida.
>    1. O sistema alerta o administrativo para o facto de estar a repetir a competência técnica não permitindo a sua introdução.
>    2. O sistema continua o processo (passo 6).


9a. Dados mínimos obrigatórios em falta.
>    1. O sistema informa quais os dados em falta( i.e. identificador interno ou descrição).
>    2. O sistema permite a introdução dos dados em falta (passo 3)
>
        > 2a. O administrativo não altera os dados. O caso de uso termina.

9b. O sistema deteta que os dados (i.e. identificador interno) introduzidos devem ser únicos e que já existem no sistema.
>    1. O sistema alerta o administrativo para o facto.
>    2. O sistema permite a sua alteração (passo 3)
>
        > 2a. O administrativo não altera os dados. O caso de uso termina.


9c. O sistema detecta que os dados introduzidos são inválidos.
> 1. O sistema alerta o administrativo para o facto. 
> 2. O sistema permite a sua alteração (passo 3).
>
        > 2a. O administrativo não altera os dados. O caso de uso termina.

#### Requisitos especiais
N/A

#### Lista de Variações de Tecnologias e Dados
N/A

#### Frequência de Ocorrência

N/A

#### Questões em aberto

* Todos os dados introduzidos são obrigatórios?
* Existem outros dados que são necessários?
* Qual o formato do identificador interno?
* Qual o formato da descrição?
* Quais os dados que em conjunto permitem detectar a duplicação de categorias?
* O código único é sempre introduzido pelo administrativo ou o sistema deve gerá-lo automaticamente?
* Qual a frequência de ocorrência deste caso de uso?
* Tem existir sempre pelo menos uma competência de carácter obrigatório e desejável?
* Se só existir uma competência o programa deve defeni-la como de carácter obrigatório?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC
![UC3_MD](UC3_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal                                                                                        | Questão: Que Classe...                                      | Resposta                                       | Justificação                                                                                                         |
|:-------------------------------------------------------------------------------------------------------|:------------------------------------------------------------|:-----------------------------------------------|:---------------------------------------------------------------------------------------------------------------------|
| 1.    O administrativo inicia a especificação de uma nova categoria.  |   Quem interage com o utilizador? |   EspecificarCategoriaDeTarefaUI    |   Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio.    |
||  Quem coordena o UC? |   EspecificarCategoriaDeTarefaController  | Controller. |   |
||  Quem cria/instancia Categoria ? |   Plataforma  |   Creator Regra 1 |   |
| 2.    O sistema solicita os dados necessários (i.e. identificador, descricao).    |   |   |   |
| 3.    O administrativo introduz os dados solicitados. | Que instância guarda os dados introduzidos?   |   CategoriaDeTarefa |   Objeto criado no passo 1    |
| 4.    O sistema mostra a lista de áreas de atividade existentes para que seja selecionada uma. |  Quem conhece as áreas de atividade existentes a listar? |   Plataforma  |   IE: A plataforma contém/agrega todas as áreas de atividade instânciados    |
| 5.    O administrativo seleciona a área de atividade em que pretende catalogar a competência técnica. |   Quem guarda a área de atividade selecionada?    |   CategoriaDeTarefa |   Objeto criado no passo 1    |
| 6.    O sistema mostra a lista de competências técnicas existentes para se selecionar uma, e eleger se esta é ou não obrigatoria.  |   Quem conhece as competências técnicas existentes a listar?  |   Plataforma  |   IE: Plataforma contém/agrega todas as competências técnicas instânciadas |
| 7.    O administrativo seleciona a competência técnica que pretende catalogar, e elege se esta é ou não obrigatoria.    |   Quem guarda a competências técnicas selecionada com informação  se esta é ou não obrigatoria?    |   CategoriaDeTarefa | Objeto criado no passo 1  |
| 8.    Os passos 6 a 7 repetem-se até não se querer selecionar mais competência.
| 9.   O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.   |   Quem valida os dados da Categoria (validação local)?    |   Categoria   |   IE: Categoria possui os seus próprios dados |
|| Quem valida os dados da Categoria (validação global)?    |   Plataforma  | IE: A Plataforma contém/agrega Categorias |   |
| 10.   O administrativo confirma.  |   |   |
| 11.   O sistema regista os dados e informa o administrativo do sucesso da operação.   |   Que classe fica responsável por guarda a categoria de tarefa criada?    |   Plataforma  |   IE: no MD a Plataforma contém/agrega CategoriaDeTarefa   |
|| Quem notifica o utilizador?  |   EspecificarCategoriaDeTarefaUI
### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * CategoriaDeTarefa

Outras classes de software (i.e. Pure Fabrication) identificadas:

 * EspecificarCategoriaDeTarefaUI
 * EspecificarCategoriaDeTarefaController

###	Diagrama de Sequência

![UC3_SD](UC3_SD.svg)


###	Diagrama de Classes

![UC3_CD](UC3_CD.svg)
