# Projeto de ESOFT 2019-2020


# 1. Constituição do Grupo de Trabalho ###

O grupo de trabalho é constituído pelo estudantes identificados na tabela seguinte.

| Aluno Nr.	   | Nome do Aluno			    |
|--------------|------------------------------|
| **1191038**  | Rui Franco                  |
| **1191606**  | Pedro Marques               |
| **1181743**  | Guilherme Daniel            |
| **1171589**  | Lucas Sousa                 |



# 2. Distribuição de Tarefas ###

A distribuição de tarefas ao longo do período de desenvolvimento do projeto pelos elementos do grupo de trabalho realizou-se conforme descrito na tabela seguinte.

**Esta tabela deve estar sempre atualizada.**

| Tarefa                      | [Iteração 1](Iteracao1/README.md) | [Iteração 2](Iteracao2/README.md) | [Iteração 3](Iteracao1/README.md) |
|-----------------------------|------------|------------|------------|
| Diagrama Casos de Uso (DUC) |  [fornecido](Iteracao1/DUC.md)   |   [todos](Iteracao2/DUC.md)  |   [todos](Iteracao3/DUC.md)  |
| Glossário  |  [todos](Iteracao1/Glossario.md)   |   [todos](Iteracao2/Glossario.md)  |   [todos](Iteracao3/Glossario.md)  |
| Especificação Suplementar   |   (n/a)    |   [todos](Iteracao2/FURPS.md)  |   [todos](Iteracao3/FURPS.md)  |
| Modelo de Domínio           |  [todos](Iteracao1/MD.md)   |   [todos](Iteracao2/MD.md)  |   [todos](Iteracao3/MD.md)  |
| UC 1- Registar Organização  |  [fornecido](Iteracao1/UC1_RegistarOrganizacao.md)   |  [1171589](Iteracao2/UC1/UC1_RegistarOrganizacao.md)          |            |
| UC 2- Definir Área de Atividade  |  [fornecido](Iteracao1/UC2_DefinirArea.md)   |  [fornecido](Iteracao2/UC2/UC2_DefinirArea.md)          |            |
| UC 3- Definir Categoria (de Tarefa)  |  [1171589](Iteracao1/UC3_DefinirCategoria.md)   |   [1171589](Iteracao2/UC3/UC3_DefinirCategoria.md)         |            |
| UC 4- Espeficar Competência Técnica  |  [1191606](Iteracao1/UC4_EspecificarCT.md)   |  [1191606](Iteracao2/UC4/UC4_EspecificarCT.md)          |            |
| UC 5- Especificar Colaborador de Organização |  [1191038](Iteracao1/UC5_EspecificarColaborador.md)   |   [1191038](Iteracao2/UC5/UC5_EspecificarColaborador.md)         |            |
| UC 6- Especificar Tarefa  |  [1181743](Iteracao1/UC6_EspecificarTarefa.md)   |   [1181743](Iteracao2/UC6/UC6_EspecificarTarefa.md)         |            |
| UC 7- Registar Freelancer                         |       |  [1171589](Iteracao2/UC7/UC7_RegistarFreelancer.md)      |            |
| UC 8- Publicar Tarefa                                |        |   [1181743](Iteracao2/UC8/UC8_PublicarTarefa.md)         |            |
| UC 9- Efetuar Candidatura                         |        |   [1191606](Iteracao2/UC9/UC9_EfetuarCandidatura.md)         |             |
| UC 10- Seriar Anúncio                               |         |  [1191038](Iteracao2/UC10/UC10_SeriarAnuncio.md)          |            |

