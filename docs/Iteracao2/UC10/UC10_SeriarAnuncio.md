# UC10 - Seriar anuncio

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador de organização faz a seriação dos candidatos à tarefa. O sistema aprenseta uma lista de anuncios disponiveis para seriar. O colaborador de organização seleciona um. O sistema apresenta a lista de candidatos à tarefa. O colaborador de organização começa o processo de seriação. O sistema apresenta um candidato e solicita uma classificação. O colaborador introduz uma classificação. Estes passos repetem-se até não existirem mais candidatos por classificar. O sistema informa o utilizador que todos os candidatos foram classificados e solicita a informação dos outros colaboradores envolvidos. O colaborador introduz o email do colaborador. Estes passos repetem-se até o utilizador inserir um email em branco. O sistema solicita a data e a hora da realização do processo. O colaborador de organização insere a data e a hora. O sistema apresenta os dados e solicita confirmação. O Colaborador de organização confirma. O sistema informa do sucesso da operação e grava as informações.

### SSD
![UC10_SSD.svg](UC10_SSD.svg)

### Formato Completo

#### Ator principal

Colaborador de organização

#### Partes interessadas e seus interesses

* **Organização:** Pretende que os candidatos sejam seriados para poder atribuir a tarefa a um freelancer.
* **Colaborador de organização:** Pretende seriar o anuncio que publicou.


#### Pré-condições

- 

#### Pós-condições

- É criada uma lista dos candidatos qualificados. É gravada a informação dos colaboradores envolvidos no processo e a data de realização.

#### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador de organização inicia o processo de seriação de candidatos.
2. O sistema apresenta uma lista de anuncios por seriar.
3. O colaborador de organização seleciona um dos anuncios.
4. O sistema apresenta a lista de candidatos ao anuncio.
5. O colaborador de organização inicia o processo de seriação.
6. O sistema apresenta um candidato e solicita classificação.
7. O colaborador de organização introduz uma classificação.
8. Os passos 6 e 7 repetem-se até não existirem candidatos por classificar.
9. O sistema solicita o email dos outros colaboradores envolvidos no processo de seriação.
10. O colaborador de organização introduz o email dos outros colaboradores.
11. O passo 9 e 10 repetem-se até o input do colaborador de organização estar em branco.
12. O sistema solicita a data e hora da realização do processo.
13. O colaborador de organização introduz a data e hora-
14. O sistema apresenta os dados e solicita confirmação
15. O colaborador de organização confirma.
16. O sistema informa o utilizador do sucesso da operação.

#### Extensões (ou fluxos alternativos)

*a O colaborador de organização cancela o processo de seriação.
>    O caso de uso termina. 

2a. O sistema deteta que não existem anuncios para seriar.
>   1. O sistema informa o utilizador e o caso de uso termina.
    
3a. Selecção invalida ou em branco.
>   1. O sistema informa o utilizador do erro e solicita um novamente o anuncio para seriar.
>   2. O colaborador de organização seleciona um novo anuncio.
>
    >   a.O colaborador de organização não seleciona novamente o anuncio(o caso de uso termina).
    
7a. Classificação invalida(e.g. repetida) ou em branco.
>   1. O sistema informa do erro e solicita novo avaliação.
>   2. O colaborador de organização insere uma nova avaliação.
>
    >       a. O colaborador de organização não fornece uma nova avaliação (o caso de uso termina).
    
9a. Email invalido.
>   1. O sistema informa o colaborador de organização que o email introduzido era invalido.
>   2. O passo 9 repete-se.

12a. Data e hora introduzida invalida.
>   1. O sistema informa do erro e solicita uma nova data e hora.
>    2. O colaborador de organização introduz uma nova data.
>
    >       a. O colaborador de organização não fornence uma nova data e hora (o caso de uso termina).
    
#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados

\-

#### Frequência de Ocorrência 

\-

#### Questões em aberto

- Qual a frequencia deste caso de uso?
- Quem atribui a tarefa ao freelancer depois do anucio ser seriado?
- Como devera ser dada a informação dos colaboradores envolvidos?
- A data e hora devera ser inserida pelo colaborador de organização ou ser a atual do sistema?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC10_MD.svg](UC10_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O Colaborador de organização inicia o processo de seriar um anuncio.  | ...interage com o utilizador? | SeriarAnuncioUI | Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existente no Modelo de Domínio. |
| | ... coordena a UC? | SeriarAnuncioController | Controller|
| | ... cria a instancia de AnuncioSeriado | Anuncio | Creator (Regra1): no MD o anuncio tem tem AnuncioSeriado.|
| | ... conhece o Anuncio | RegistoAnuncio | HC + LC: o RegistoAnuncio contém/agrega Anuncio. |
| | ... conhece o registo anuncio? | Plataforma | IE: a Plataforma contém/agrega RegistoAnuncio|
| 2. O sistema apresenta uma lista de anuncios por seriar.| ...  conhece a lista de anuncios por seriar | Registo Anuncio | HC + LC: o RegistoAnuncio contém/agrega Anuncio. |
| | ... conhece o registo anuncio? | Plataforma | IE: a Plataforma contém/agrega RegistoAnuncio|
| 3. O colaborador seleciona um dos anuncios | ||
| 4. O sistema apresenta a lista de candidatos ao anuncio | ...  conhece a lista de candidatos? | Anuncio | IE: O anuncio agrega instancias de Candidatura  tem a informação dos candidatos.|
| 5. O colaborador inicia o processo de seriação.||||
| 6. O sistema apresenta um candidato e solicita classificação|||
| 7. O colaborador de organização introduz uma classificação |...  guarda a classificação? | AnuncioSeriado|IE: A classe AnuncioSeriado guarda a lista de freelanceres e a sua classificação.|
|8. Os passos 6 e 7 reptem-se até não existirem candidatos por classificar.|||
|9. O sistema solicitao email dos outros colaboradores envolvidos no processo de seriação. |||
|10. O colaborador de organização introduz o email dos outros colaboradores.|... conhece a lista de colaboradores ? |RegistoOrganizacao|HC + LC: o RegistoTipoRegimento armazena todos os colaboradores.|
| | ... conhece o registo organização? | Plataforma | IE: a Plataforma contém/agrega RegistoOrganização|
| |... guarda os colaboradores envolvidos?| AnuncioSeriado |IE: o AnuncioSeriado guarda a lista de colaboradores envolvidos.|
|11. O passo 9 e 10 repetemse até o input do colaborador de organização estar em branco.| | |
|12. O sistema solicita a data e hora da realização do processo. ||||
|13. O colaborador de organização introduz a date e hora.| ... guarda a informação da data e hora | AnuncioSeriado | IE: A classe AnuncioSeriado guarda as informações |
|14. O sistema apresenta os dados e solicita confirmação. |||
|15. O colaborador de organização confirma| | | 
|16. O sistema valida os dados e informa o utilizador do sucesso da operação. |... validade os dados do AnúncioSeriado(validação local)?|	AnuncioSeriado	| IE: possui os seus próprios dados.|
||...validade os dados do Anúncio (validação global)?	|Anuncio	|IE: o Anuncio contém/agrega Anuncio.||





### Sistematização ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:

*   AnuncioSeriado
*   Plataforma
*   RegistoAnuncio
*   RegistoOrganização
*   Anuncio
*   Candidatura
*   TipoDeRegimento

Outras classes de software (i.e. Pure Fabrication) identificadas:

*   SeriarAnuncioUI
*   SeriarAnuncioController

Outras classes de sistema/componentes externos:

*   SessaoUtilizador
*   AplicacaoPOT

###	Diagrama de Sequência

![UC10_SD.svg](UC10_SD.svg)

### SD_getListaAnuncioPorSeriarByEmail ##

![SD_getListaAnuncioPorSeriarByEmail.svg](SD_getListaAnuncioPorSeriarByEmail.svg)


###	Diagrama de Classes

![UC10_CD.svg](UC10_CD.svg)

