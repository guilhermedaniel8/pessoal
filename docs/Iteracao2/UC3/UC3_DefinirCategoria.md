# UC3 - Definir Categoria de Tarefa

## 1. Engenharia de Requisitos

### Formato Breve

O administrativo inicia a especificação de uma nova categoria de tarefa. O sistema solicita os dados necessários sobre a nova categoria de tarefa(i.e. descrição). O administrativo introduz os dados solicitados. O sistema apresenta as áreas de atividade e pede para se selecionar uma destas. O administrativo seleciona a AA pretendida. O sistema apresenta as competências técnicas referentes à área de atividade previamente selecionada e pede para se selecionar uma e para eleger se é obrigatória, desejável ou se não apresenta caracter de todo. O administrativo seleciona a competência técnica e elege se esta é obrigatoria, desejável ou sem caracter referente. O sistema apresenta os graus de proficiência referentes a competência técnica previamente selecionada, pede para selecionar o grau mínimo exigido. O administrativo seleciona grau de proficiência mínimo. A seleção de CT, o seu caracter assim como o grau de proficiência mínimo descrito anteriormente repete-se até que todas as CT pretendidas tenham sido selecionadas pelo administrativo. O sistema valida e apresenta todos os dados ao administrativo, pedindo que os confirme. O administrativo confirma. O sistema regista os dados da nova categoria de tarefa e informa o administrativo do sucesso da operação.

### SSD
![UC3_SSD.svg](UC3_SSD.svg)


### Formato Completo

#### Ator principal

Administrativo

#### Partes interessadas e seus interesses
* **Administrativo:** pretende definir as categorias de tarefas para que estas possam ser usadas posteriormente na especificação de tarefas.
* **Colaborador:** pretende que as categorias estejam definidas para poder especificar corretamente as suas tarefas.
* **T4J:** pretende que a plataforma permita associar as categorias de tarefas às áreas de atividades e às tarefas.
 
 #### Pré-condições

 N/A

 #### Pós-condições

 A informação da categoria é registada no sistema.

 #### Cenário de sucesso principal (ou fluxo básico)

 1. O administrativo inicia a especificação de uma nova categoria de tarefa. 
 2. O sistema solicita os dados necessários (i.e. descrição). 
 3. O administrativo introduz os dados solicitados.
 4. O sistema apresenta as áreas de atividade e pede para selecionar uma.
 5. O administrativo seleciona a área de atividade pretendida.
 6. O sistema apresenta as competências técnicas referentes à área de atividade previamente selecionada, pedindo para selecionar uma e para eleger o seu caracter(obrigatório, desejável, ou sem caracter).
 7. O administrativo seleciona a competência técnica pretendida e elege o seu caracter.
 8. O sistema apresenta os graus de proficiência referentes a competência técnica previamente selecionada e pede para selecionar o grau mínimo exigido.
 9. O administrativo seleciona grau de proficiência mínimo.
 10. Os passos 6 a 9 repetem-se até que todas as competências pretendidas estejam selecionadas, lhes tenha  sido atribuido o caracter assim como o grau mínimo de proficiência exigido.
 9. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. 
 10. O administrativo confirma. 
 11. O sistema regista os dados e informa o administrativo do sucesso da operação.

 #### Extensões (ou fluxos alternativos)

 *a. O administrativo solicita o cancelamento da definição da categoria de tarefa.

 > O caso de uso termina.

 4a. O sistema deteta que o administrativo apenas possui uma área de atividade.
 >    1. O sistema alerta o administrativo para o facto de só existir uma área de atividade definida.
 >    2. O sistema questiona o administrativo se deseja definir uma área de atividada, para posterior seleção.
 >
         > 2a. O administrativo não introduz areas de atividade.
             O sistema assume a área de atividade conhecida e informa o administrativo disso.
         > 2b. O administrativo só introduz uma area de atividade.
             O sistema assume a área de atividade introduzida e informa o administrativo disso.
             O sistema avança para o passo 6.
         > 2c. O administrativo introduz várias área de atividade.
             O sistema volta ao início do passo 4.

 4b. O sistema deteta que o administrativo não possui uma área de atividade. 
 >    1. O sistema alerta o administrativo para o facto não existir uma área de atividade definida.
 >    2. O sistema questiona o administrativo se deseja definir uma área de atividada, para posterior seleção.
 >      
         > 2a. O administrativo não introduz areas de atividade o caso de uso termina.
         > 2b. O administrativo só introduz uma area de atividade.
             O sistema assume a área de atividade conhecida e informa o administrativo disso.
             O sistema avança para o passo 6.
         > 2c. O administrativo introduz várias área de atividade.
             O sistema volta ao início do passo 4.

 6a. O sistema deteta que o administrativo apenas possui uma competência relativamente a área de atividade selecionada
 >    1. O sistema alerta o administrativo para o facto de só existir uma competência técnica definida.
 >    2. O sistema questiona o administrativo se deseja definir uma nova competência técnica, para posterior seleção.
 >
         > 1a. O administrativo não introduz nova competência técnica.
             O sistema assume a competência técnica conhecida e informa o administrativo disso.
         > 1b. O administrativo introduz novas competências técnicas.
             O sistema volta ao início do passo 6.

 6b. O sistema deteta que o administrativo não possui competências.
 >    1. O sistema alerta o administrativo para o facto de não existir competências técnicas definidas.
 >    2. O sistema questiona o administrativo se deseja definir uma competência técnica, para posterior seleção.
 >
         > 2a. O administrativo não introduz novas competências tecnicas o caso de uso termina. 
         > 2b. O administrativo introduz novas competências tecnicas.
             O sistema volta ao inicio do passo 6.

 7a. O sistema deteta que a competência técnica escolhida já tinha sido introduzida.
 >    1. O sistema alerta o administrativo para o facto de estar a repetir a competência técnica não permitindo a sua introdução.
 >    2. O sistema continua o processo (passo 7).

8a. Sistema deteta que a lista dos graus de proficiência referentes a CT selecionada está vazia.
>   1. O sistema alerta o administrativo para o facto da lista dos graus de proficiência estar vazia e permite a sua introdução.
>    2. O sistema questiona o administrativo se deseja introduzir graus de proficiência, para posterior seleção.
> 
        > 2a. O administrativo não introduz graus de proficiência.
              O caso de uso termina.
        > 2b. O administrativo introduz graus de proficiência.
              O sistema volta ao início do passo 8.
 
 9a. Dados mínimos obrigatórios em falta.
 >    1. O sistema informa quais os dados em falta( i.e. descrição).
 >    2. O sistema permite a introdução dos dados em falta (passo 3)
 >
         > 2a. O administrativo não altera os dados. O caso de uso termina.

 #### Requisitos especiais
 N/A

 #### Lista de Variações de Tecnologias e Dados
 N/A

 #### Frequência de Ocorrência
 N/A

 #### Questões em aberto

 * Existem outros dados que são necessários?
 * Qual o formato da descrição?
  * A lista dos graus de proficiência pode estar vazia?
 * Quais os dados que em conjunto permitem detectar a duplicação de categorias?
 * Qual a frequência de ocorrência deste caso de uso?
 * Tem existir sempre pelo menos uma competência de carácter obrigatório e desejável?
 * Se só existir uma competência o programa deve defeni-la como de carácter obrigatório?
 
## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC3_MD.svg](UC3_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O administrativo inicia a definição de uma nova categoria de tarefa.   		 |	... interage com o utilizador? | DefinirCategoriaUI    |  Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existente no Modelo de Domínio. |
|  		 |	... coordena o UC?	| DefinirCategoriaController | Controller    |
|  		 |	... cria instância de Categoria?| RegistoCategoriaTarefa   | As responsabilidades da plataforma passarão para o RegistoCategoriaTarefa (HC + LC) + Creator com HC + LC: RegistoCategoriaTarefa possui Categoria.   |
|                |      ... conhece o RegistoCategoriaTarefa | IE: a Plataforma contém/agrega RegistoCategoriaTarefa |
||...gera o identificador único da Categoria?|Categoria| O identificador único da categoria é gerado na própria Categoria. |
| 2. O sistema solicita a descrição.  		 |							 |             |                              |
| 3. O administrativo introduz a descrição.  		 |	... guarda os dados introduzidos?  |   RegistoCategoriaTarefa | HC + LC - instância criada no passo 1.     |
| 4. O sistema mostra a lista de áreas de atividade para que seja selecionada uma. 		 |	...conhece as áreas de atividades?					 |  RegistoAreaAtividade          | HC + LC: o RegistoAreaAtividade agrega/contém AreaAtividade.                           |
|    |      ... conhece o RegistoAreaAtividade    | Plataforma   |  IE: a Plataforma contém/agrega RegistoAreaAtividade.  |
| 5. O administrativo seleciona uma área de atividade.  |  ... guarda a área selecionada? |Categoria   | IE: instância criada no passo 1. No MD uma Categoria é referente a uma AreaAtividade.                             |
| 6. O sistema mostra a lista de competências técnicas referentes à área de atividade previamente selecionada para que seja selecionada uma.		 |	.. conhece as competências técnicas?						 |  RegistoCompetenciaTecnica           | HC + LC: O RegistoCompetenciaTecnica agrega/contém CompetenciaTecnica. Técnicas                            |
||...sabe a que área de atividade a competência técnica se refere?| CompetenciaTecnica |IE: cada CompetenciaTecnica conhece a AreaAtividade em que se enquadra.|
|7. O administrativo escolhe uma competência técnica da lista.  		 |	... guarda a competência técnica selecionada?   | CaraterCT       | IE: no MD cada Categoria tem várias CaraterCT sendo cada uma referente a uma CompetenciaTecnica (CT).    |
||...cria instância de CaracterCT|Categoria|IE: no MD cada Categoria tem várias CaraterCT.|
| 8. O sistema solicita a seleção do seu caráter (i.e. obrigatória, desejável ou sem caracter).		 |							 |             |                              |
| 9. O administrativo seleciona a informação relativa ao caráter da competência técnica.  		 |... guarda a informação introduzida?   | CaraterCT  | IE. instância criada no passo 7.     |
| 10. O sistema mostra a lista de graus de proficiência referentes a competências técnicas previamente selecionada para que seja selecionado o grau mínimo.         |    .. conhece os  graus de proficiência?                         |  CompetenciaTecnica          | IE: A CompetenciaTecnica agrega/contém GrauProficiencia.                            |
|11. O administrativo escolhe o grau de proficiência mínimo da lista.           |    ... guarda o grau de proficiência selecionado?   |   CaraterCT    | IE: no MD cada Categoria tem várias CaraterCT sendo cada uma referente a uma CompetenciaTecnica (CT).    |
||...cria instância de  CaraterCT|Categoria|IE: no MD cada Categoria tem  várias CaraterCT.|
| 12. Os passos 7 a 11 repetem-se enquanto não forem introduzidas todas as competências técnicas pretendidas.||||                          
| 13. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.  |	...valida os dados da Categoria (validação local) | Categoria |  IE. A Categoria possui os seus próprios dados.|  	
|	 |	...valida os dados da Categoria (validação global) | RegistoCategoriaTarefa  | HC + LC: O RegistoCategoriaTarefa possui/agrega Categoria.  |
| 14. O administrativo confirma.   		 |							 |             |                              |
| 15. O sistema regista os dados e informa o administrativo do sucesso da operação.  		 |	... guarda a Categoria de tarefa criada? | RegistoCategoriaTarefa  | HC + LC: O RegistoCategoriaTarefa possui Categoria (de tarefa). |  


### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Categoria
 * CaraterCT
 * CompetenciaTecnica
 * RegistoCategoriaTarefa

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * DefinirCategoriaUI  
 * DefinirCategoriaController

###	Diagrama de Sequência

![UC3_SD.svg](UC3_SD.svg)

###	Diagrama de Classes

![UC3_CD.svg](UC3_CD.svg)
