# Glossário

**Os termos devem estar organizados alfabeticamente.**

(Completar)

| **_Termo_**                   	| **_Descrição_**                                           |                                       
|:------------------------|:----------------------------------------------------------------|
| **Administrativo** | Pessoa responsável por realizar na plataforma várias atividades de suporte ao negócio.|
| **ADM** | Acrónimo para Administrativo.|
| **Área de Atividade**| Representa a área de negócio/mercado em que se atua como, por exemplo, IT (Information Technologies), Marketing e Design. |
| **AA** | Acrónimo para Área de Atividade. |
| **Anúncio** | Resultado da publicação de uma tarefa por parte do Colaborador de Organização. |
| **Candidatura (a uma Tarefa)** | Processo a que um determinado freelancer deve concorrer caso pretenda realizar uma determinada tarefa. |
| **Categoria (de Tarefa)** | Corresponde a uma descrição usada para catalogar uma ou mais tarefas (semelhantes).|
| **Colaborador de Organização**| Pessoa que exerce funções (e.g. especificar tarefas) no âmbito de uma Organização. |
| **Competência Técnica**| Refere-se a um conjunto de conhecimentos e competências (habilidades) necessárias à execução de tarefas de uma determinada categoria.|
| **CT** |Acrónimo de Competência Técnica. |
| **_Freelancer_** | Pessoa individual que trabalha por conta própria e não está necessariamente comprometida com uma entidade empregadora (organização) específica a longo prazo.|
| **Gestor de Organização**| Corresponde ao papel desempenhado por um Colaborador dessa Organização na própria Organização.|
| **Grau de Proficiência**| Conhecimento que um determinado indivíduo possui/deverá ter sobre uma determinada Competência Técnica. |
| **GP** | Acrónimo para Grau de Proficiência. |
| **Habilitação (Académica)** | Características do curso ou ciclo de estudos concluídos por um freelancer. |
| **HA** | Acrónimo para Habilitação Académica. |
| **Implantação (de Software)**| Processo que visa colocar um determinado software em produção. Nos casos mais simples, pode corresponder simplesmente à atividade de instalação desse software. Contudo, este processo pode implicar também outras atividades como ativação, configuração, etc.  |
| **NIF**| Acrónimo para Número de Identificação Fiscal. |
| **Organização** | Pessoa coletiva que pretende contratar _freelancers_ para a realização de tarefas necessárias à atividade da mesma.|
| **POT** | Moeda virtual interna à Plataforma. |
| **Processo de Autenticação** | Meio através do qual se procede à verificação da identidade da pessoa que pretende/está a utilizar a plataforma informática.|
| **Tipo de Regimento** | Regras pelas quais se regem os processos de candidatura, seriação e de atribuição de tarefas a um freelancer. |
| **TR** | Acrónimo para Tipo de Regimento. |
| **Utilizador** | Pessoa que interage com a aplicação informática.|
| **Utilizador Não Registado** | Utilizador que interage com a plataforma informática de forma anónima, i.e. sem ter realizado previamente o processo de autenticação previsto.|
| **Utilizador Registado** | Utilizador que interage com a plataforma informática após ter realizado o processo de autenticação previsto e, portanto, a aplicação conhece a sua identidade. Tipicamente, este assume o papel/função de Administrativo ou Gestor de Organização ou Colaborador de Organização ou _Freelancer_.|
