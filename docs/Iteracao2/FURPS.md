# Especificação suplementar

## Funcionalidades

*Especifica as funcionalidades que não se relacionam com os casos de
uso, nomeadamente: Auditoria, Reporte e Segurança.*


|  Função    |  Descrição     |
|:---------- |:-----------------|
| **Segurança:**  |  *As interações dos utilizadores na plataforma devem ser precedidas de um processo de autenticação, com uma password gerada na plataforma.* |
|         |  *A utilização da plataforma é restrita ao registo de organizações.*  |



## Usabilidade

*Avalia a interface com o utilizador. Possui diversas subcategorias,
entre elas: prevenção de erros; estética e design; ajudas (Help) e
documentação; consistência e padrões.*

|  Função    |  Descrição     |
|:---------- |:-----------------|
| **Estética** | *A interface gráfica da aplicação assenta numa paleta de cores estruturada em duas cores (primárias e secundárias).* |

## Fiabilidade/Confiabilidade
*Refere-se a integridade, conformidade e interoperabilidade do software. Os requisitos a serem considerados são: frequência e gravidade de falha, possibilidade de recuperação, possibilidade de previsão, exatidão, tempo médio entre falhas.*

 N/A

## Desempenho
 *Avalia os requisitos de desempenho do software, nomeadamente: tempo de resposta, consumo de memória, utilização da CPU, capacidade de carga e disponibilidade da aplicação..*
 
 N/A

## Suportabilidade
*Os requisitos de suportabilidade agrupam várias características, como:
testabilidade, adaptabilidade, manutibilidade, compatibilidade,
configurabilidade, instalabilidade, escalabilidade entre outros.*

|  Função    |  Descrição     |
|:---------- |:-----------------|
| **Configurabilidade** | *As palavras-passe de todos os utilizadores são geradas pela plataforma, recorrendo a um algoritmo externo que pode ser concebido por terceiros, configurado aquando da sua implementação.* |
|             | *A interface gráfica da aplicação assenta numa paleta de cores estruturada em duas cores (primárias e secundárias) configurada aquando da sua implantação.* |
| **Testabilidade** | *São especificados um conjunto de testes de cobertura e mutação (e.g. unitários, funcionais e de integração), assegurando a qualidade do sistema desenvolvido.* |


## +

### Restrições de design

*Especifica ou restringe o processo de design do sistema. Exemplos podem incluir: linguagens de programação, processo de software, uso de ferramentas de desenvolvimento, biblioteca de classes, etc.*

- **Adotar boas práticas de identificação de requisitos e de análise e design de software OO**

- **Reutilizar o componente de gestão de utilizadores existente na T4J**

### Restrições de implementação

*Especifica ou restringe o código ou a construção de um sistema tais
como: padrões obrigatórios, linguagens de implementação, políticas de
integridade de base de dados, limites de recursos, sistema operativo.*

- **Implementar o núcleo principal do software em Java**
 
- **Adotar normas de codificação reconhecidas**

### Restrições de interface

*Especifica ou restringe as funcionalidades inerentes à interação do
sistema com outros sistemas externos.* 

 N/A

### Restrições físicas

*Especifica uma limitação ou requisito físico do hardware utilizado, por
exemplo: material, forma, tamanho ou peso.*

 N/A
