package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;

public class ListaColaboradores {

    private List<Colaborador> listaColaboradores;

    public ListaColaboradores() {
        listaColaboradores = new ArrayList<>();
    }

    public boolean novoColaborador(String nome, String funcao, String telefone, String email) {

        Colaborador colaborador = new Colaborador(nome, funcao, telefone, email);

        return listaColaboradores.contains(colaborador)
                ? false
                : listaColaboradores.add(colaborador);
    }

    public boolean validaColaborador(Colaborador colab) {
        if (listaColaboradores.contains(colab)) {
            return false;
        }
        return true;
    }

    public boolean addColaborador(Colaborador colab) {

        return listaColaboradores.add(colab);

    }
    
    public boolean registaColaboradorComoUtilizador(Colaborador colab){
        return true;
    }

}
