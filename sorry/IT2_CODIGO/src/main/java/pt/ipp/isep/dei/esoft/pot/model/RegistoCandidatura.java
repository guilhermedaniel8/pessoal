/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author guilhermedaniel
 */
public class RegistoCandidatura {

    private List<Candidatura> listaCandidatura = new ArrayList<Candidatura>();

    public Candidatura novaCandidatura(Freelancer fr) {
        return new Candidatura(fr);
    }

    private boolean validaCandidatura(Candidatura candidatura) {
        boolean bRet = true;
        if (!this.listaCandidatura.isEmpty()) {
            for (Candidatura can : this.listaCandidatura) {
                if (listaCandidatura.equals(can)) {
                    bRet = false;
                }
            }
        }
        return bRet;
    }

    private boolean addCandidatura(Freelancer freelancer, Candidatura candidatura, Anuncio anuncio) {
        if (candidatura.getTexto() == null) {
            Candidatura a = new Candidatura(freelancer, candidatura.getValorReceber(), candidatura.getDiasRealizacao(), anuncio);
        }
        Candidatura a = new Candidatura(freelancer, candidatura.getValorReceber(), candidatura.getDiasRealizacao(), candidatura.getTexto(), anuncio);
        return this.listaCandidatura.add(a);
    }

}
