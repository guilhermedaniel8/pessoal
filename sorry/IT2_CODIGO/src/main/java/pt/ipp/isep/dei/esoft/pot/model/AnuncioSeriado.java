/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class AnuncioSeriado {
    private List<Freelancer> candidatosSeriados;
    private List<Colaborador> colaboradoresEnvoldidos;
    private Date dataHoraSeriacao;

        
    public AnuncioSeriado(){
        candidatosSeriados = new ArrayList<>();
        colaboradoresEnvoldidos = new ArrayList<>();
    }

    /**
     * @return the candidatosSeriados
     */
    public List<Freelancer> getCandidatosSeriados() {
        return candidatosSeriados;
    }

    /**
     * @param candidatosSeriados the candidatosSeriados to set
     */
    public void setCandidatosSeriados(List<Freelancer> candidatosSeriados) {
        this.candidatosSeriados = candidatosSeriados;
    }

    /**
     * @return the dataHoraSeriacao
     */
    public Date getDataHoraSeriacao() {
        return dataHoraSeriacao;
    }

    /**
     * @param dataHoraSeriacao the dataHoraSeriacao to set
     */
    public void setDataHoraSeriacao(Date dataHoraSeriacao) {
        this.dataHoraSeriacao = dataHoraSeriacao;
    }

    /**
     * @return the colaboradoresEnvoldidos
     */
    public List<Colaborador> getColaboradoresEnvoldidos() {
        return colaboradoresEnvoldidos;
    }

    /**
     * @param colaboradoresEnvoldidos the colaboradoresEnvoldidos to set
     */
    public void setColaboradoresEnvoldidos(List<Colaborador> colaboradoresEnvoldidos) {
        this.colaboradoresEnvoldidos = colaboradoresEnvoldidos;
    }

    public String toString(){
        return("Anuncio Seriado:\n" +
                "Numero de candidatos seriados: " + candidatosSeriados.size() +
                "Numero colaboradores envolvidos: " + colaboradoresEnvoldidos.size() + 
                "Data e hora realização processo: " + dataHoraSeriacao);
                
    }
    
}
