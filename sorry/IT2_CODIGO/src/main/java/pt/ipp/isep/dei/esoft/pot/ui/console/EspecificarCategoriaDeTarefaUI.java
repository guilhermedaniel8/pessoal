package pt.ipp.isep.dei.esoft.pot.ui.console;

import java.util.List;
import pt.ipp.isep.dei.esoft.pot.controller.EspecificarCategoriaDeTarefaController;
import pt.ipp.isep.dei.esoft.pot.model.AreaAtividade;
import pt.ipp.isep.dei.esoft.pot.model.CompetenciaTecnica;
import pt.ipp.isep.dei.esoft.pot.model.GrauProficiencia;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

public class EspecificarCategoriaDeTarefaUI {

    private EspecificarCategoriaDeTarefaController m_controller;

    public EspecificarCategoriaDeTarefaUI() {
        m_controller = new EspecificarCategoriaDeTarefaController();
    }

    public void run() {
        boolean resposta;
        System.out.println("\nEspecificar Categoria De Tarefa:");

        if (introduzDados()) {
            
            introduzDados1();

            do {
                introduzDados2();
                resposta = Utils.confirma("Deseja Selecionar mais competências técnicas? (S/N)");
            } while (resposta == true);

            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaCategoriaDeTarefa()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        } else {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }

    }

    private boolean introduzDados() {
        String descricao = Utils.readLineFromConsole("Descrição: ");
        return m_controller.novaCategoriaDeTarefa(descricao);
    }

    private boolean introduzDados2() {
        
        List<CompetenciaTecnica> listaCTcomAA = m_controller.getlistaCTcomAA();

        String competenciaTecnicaId = "";
        
        CompetenciaTecnica competencia = (CompetenciaTecnica) Utils.apresentaESeleciona(listaCTcomAA , "Seleciona a competência técnica pretendida:");
        
        if (competencia != null) {
            competenciaTecnicaId = competencia.getCodigo();
        }
        boolean obrigatoriedade = Utils.confirma("A competências técnica selecionada é obrigatória? (S/N)");

        GrauProficiencia grauMinimo = (GrauProficiencia) Utils.apresentaESeleciona(competencia.getListaGrauProficiencia(), "Seleciona o grau minimo pretendida:");
      
        return m_controller.addCaraterCT(competencia, obrigatoriedade, grauMinimo);
    }

    private void apresentaDados() {
        System.out.println("\nCategoria de Tarefa:\n" + m_controller.getCategoriaDeTarefaString());
    }

    private void introduzDados1() {
        List<AreaAtividade> listaAreasAtividade = m_controller.getAreasAtividade();

        String areaAtividadeId = "";
        AreaAtividade areaAtividade = (AreaAtividade) Utils.apresentaESeleciona(listaAreasAtividade, "Seleciona a área de atividade pretendida:");
        if (areaAtividade != null) {
            areaAtividadeId = areaAtividade.getCodigo();
        }
        m_controller.setAT(areaAtividadeId);  
    }

}