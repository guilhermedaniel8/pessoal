package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Categoria {

    private String identificador;

    private String descricao;

    private AreaAtividade areaAtividade;

    private List<CaraterCT> listaCaraterCT;

    public Categoria(String descricao) {
        if ( (descricao == null) ||  (descricao.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }
        this.identificador = "0";
        this.descricao = descricao;
        this.areaAtividade = new AreaAtividade();
        this.listaCaraterCT = new ArrayList<>();
    }

    public String getIdentificador() {
        return identificador;
    }

    public String getDescricao() {
        return descricao;
    }

    public AreaAtividade getAreaAtividade() {
        return new AreaAtividade(areaAtividade);
    }
    
    public List<CaraterCT> getListaCaraterCT(){
        return this.listaCaraterCT;
    }
    
    public List<CompetenciaTecnica> getListaCTObrigatorias(){
        List<CompetenciaTecnica> lista = new ArrayList<>();
        
        for(CaraterCT ct : listaCaraterCT){
            if(ct.isObrigatoria()){
                lista.add(ct.getCompetencia());
            }
        }
        
        return lista;
        
    }
    public boolean validaCaraterCT(CaraterCT carater) {
        boolean bRet = true;
        if (!this.listaCaraterCT.isEmpty()) {
            for (CaraterCT tc : listaCaraterCT) {
                if (carater.equals(tc)) {
                bRet = false;
                }
                if (tc.hasCompetencia(carater.getCompetencia())) {
                bRet = false;    
                }
            }
        }
        return bRet;
    }

    
    public List<GrauProficiencia> getListGPMinimosObrigatorios(){
        List<GrauProficiencia> lista = new ArrayList<>();
        
        for(CaraterCT ct : listaCaraterCT){
            if(ct.isObrigatoria()){
                lista.add(ct.getGrauMinimo());
            }
        }
        return lista;
    }
   
    public List<CaraterCT> getListaCTeGPObrig(){
        List<CaraterCT> lista = new ArrayList<>();
        
        for(CaraterCT ct : listaCaraterCT){
            if(ct.isObrigatoria()){
                lista.add(ct);
            }
        }
        return lista;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setAreaAtividade(AreaAtividade areaAtividade) {
        this.areaAtividade = new AreaAtividade(areaAtividade);
    }

    public boolean addCaraterCT(CompetenciaTecnica competencia, boolean obrigatoriedade, GrauProficiencia grauMinimo) {
        CaraterCT ctt = new CaraterCT(competencia, obrigatoriedade, grauMinimo);
        if (this.listaCaraterCT.contains(ctt)) {
            return false;
        }
        return this.listaCaraterCT.add(ctt);
    }

    public boolean hasIdentificador(String identificador) {
        return this.identificador.equalsIgnoreCase(identificador);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.identificador);
        return hash;
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }
        Categoria cate = (Categoria) outroObjeto;
        return this.identificador.equalsIgnoreCase(cate.identificador) && this.descricao.equalsIgnoreCase(cate.descricao) && this.areaAtividade.equals(cate.areaAtividade);
    }

    @Override
    public String toString() {
        return String.format("%s - %s - Área Atividade: %s - %s"
                , this.identificador
                , this.descricao
                , this.areaAtividade.toString()
                , this.listaCaraterCT);
    }
    
    
}
