
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;

/**
 *
 * @author paulomaio
 */
public class Organizacao
{
    private AutorizacaoFacade m_oAutorizacao;
    private String nome;
    private String nif;
    private EnderecoPostal end;
    private String site;
    private String tlf;
    private String email;
    private Colaborador colab;
    
    private ListaTarefas listaTarefas;
    
    public Organizacao(String nome, String nif, String site, String tlf, 
            String email, EnderecoPostal end, Colaborador colab)
    {
        if ( (nome == null) || (nif == null) || (tlf == null) ||
                (email == null) || (end == null) || (colab == null) ||
                (nome.isEmpty())|| (nif.isEmpty()) || (tlf.isEmpty()) || 
                (email.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.nome = nome;
        this.nif = nif;
        this.end = end;
        this.site = email;
        this.tlf = tlf;
        this.email = email;
        this.colab = colab;
     }

    public String getNome() {
        return nome;
    }

    public String getNif() {
        return nif;
    }

    public EnderecoPostal getEnd() {
        return end;
    }

    public String getSite() {
        return site;
    }

    public String getTlf() {
        return tlf;
    }

    public String getEmail() {
        return email;
    }
    
    public Colaborador getGestor()
    {
        return this.colab;
    }
    
    public ListaTarefas getListaTarefas(){
        return this.listaTarefas;
    }
   
    
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.nif);
        return hash;
    }
    
    @Override
    public boolean equals(Object o) {
        
        if (this == o)
            return true;
        
        if (o == null)
            return false;
        
        if (getClass() != o.getClass())
            return false;
        
        Organizacao obj = (Organizacao) o;
        return (Objects.equals(nif, obj.nif));
    }
    
    @Override
    public String toString()
    {
        String str = String.format("%s - %s - %s - %s - %s - %s - %s", this.nome, this.nif, this.site, this.tlf, this.email, this.end.toString(),this.colab.toString());
        return str;
    }
    
    public static Colaborador novoColaborador(String strNome, String strFuncao, String strTelefone, String strEmail)
    {
        return new Colaborador(strNome,strFuncao,strTelefone,strEmail);
    }
    
    public static EnderecoPostal novoEnderecoPostal(String endLocal, String endCodPostal, String endLocalidade)
    {
        return new EnderecoPostal(endLocal,endCodPostal,endLocalidade);
    }
    
    public Colaborador novoColaboradorOrganizacao(String strNome, String strFuncao, String strTelefone, String strEmail){
        return new Colaborador(strNome,strFuncao,strTelefone,strEmail);
    }
    
    public boolean criarColaborador(Colaborador oColaborador, AutorizacaoFacade m_oAutorizacao){
        String nome = oColaborador.getNome();
        String email = oColaborador.getEmail();
        String pwd = oColaborador.gerarPWD();
        this.m_oAutorizacao = m_oAutorizacao;
        if(this.m_oAutorizacao.registaUtilizadorComPapeis(nome,email, pwd, 
                    new String[] {Constantes.PAPEL_COLABORADOR_ORGANIZACAO})){
            return addColaborador(oColaborador);
        }else{
            return false;
        }
    }
    
    public boolean addColaborador(Colaborador oColaborador){
        return listaColaborador.add(oColaborador);
    }
}
