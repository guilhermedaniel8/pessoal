package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;

public class RegistoCategoriaTarefa {

    public List<Categoria> listaCategoriaTarefa = new ArrayList<Categoria>();


    public Categoria getCategoriaTarefaById(String categoriaDeTarefaById) {
        for (Categoria ct : this.listaCategoriaTarefa) {
            if (ct.hasIdentificador(categoriaDeTarefaById)) {
                return ct;
            }
        }
        return null;
    }

    public Categoria novaCategoriaDeTarefa(String descricao) {
        return new Categoria(descricao);
    }

    public boolean registaCategoriaDeTarefa(Categoria oCateTarefa) {
        if (this.validaCategoriaDeTarefa(oCateTarefa)) {
            return addCategoriaDeTarefa(oCateTarefa);
        }
        return false;
    }

    private boolean addCategoriaDeTarefa(Categoria oCateTarefa) {
        oCateTarefa.setIdentificador(geraCategoriaId());
        return this.listaCategoriaTarefa.add(oCateTarefa);
    }

    public boolean validaCategoriaDeTarefa(Categoria oCateTarefa) {
        boolean bRet = true;
        if (!this.listaCategoriaTarefa.isEmpty()) {
            for (Categoria ct : this.listaCategoriaTarefa) {
                if (oCateTarefa.equals(ct)) {
                    bRet = false;
                }
                if (ct.hasIdentificador(oCateTarefa.getIdentificador())) {
                    bRet = false;
                }
            }
        }
        return bRet;
    }

    public CaraterCT novoCaraterCT(CompetenciaTecnica competenciaId, boolean obrigatorio, GrauProficiencia grauMinimo) {
        return new CaraterCT(competenciaId, obrigatorio, grauMinimo);
    }


    public List<Categoria> getCategoriaTarefa() {
        List<Categoria> listaCategoriasTarefa = new ArrayList<>();
        listaCategoriasTarefa.addAll(this.listaCategoriaTarefa);
        return listaCategoriasTarefa;
    }

    private String geraCategoriaId() {
        return String.valueOf((listaCategoriaTarefa.size()) + 1);
    }

    public List<Categoria> procuraCategoriasElegivel(List<Reconhecimento> lrc) {
        List<Categoria> listaCE = new ArrayList<>();

        List<CompetenciaTecnica> listaCT = new ArrayList<>();
        List<GrauProficiencia> listaGP = new ArrayList<>();

        for (Reconhecimento r : lrc) {
            listaCT.add(r.getCompetenciaTecnica());
        }
        for (Reconhecimento r : lrc) {
            listaGP.add(r.getGrauProficiencia());
        }

        for (Categoria c : listaCategoriaTarefa) {
            if (listaCT.containsAll(c.getListaCTObrigatorias())) {
                listaCE.add(c);
            }
        }

        for (Categoria c : listaCategoriaTarefa) {
            for (CaraterCT listaCTeGPObrig : c.getListaCTeGPObrig()) {
                for (Reconhecimento rh : lrc) {
                    if (c.getListaCTObrigatorias().contains(rh.getCompetenciaTecnica())) {
                        
                    }
                }
            }
        }

        return listaCE;
    }

}
