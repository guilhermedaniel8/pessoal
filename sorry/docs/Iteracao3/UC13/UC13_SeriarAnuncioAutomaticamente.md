# UC 13 - Seriar Anuncio Automáticamente

## 1. Engenharia de Requisitos

### Formato Breve

Quando atingir o Tempo/Data agendado no final da UC8 inicia-se o processo de seriação automático. O sistema verifica se estão reunidas todas as condições para que esta seriação seja possivel, ou seja, não ter havido seriação manual prévia e o anuncio em questão possuir critérios de seriação objetivos. Inicia-se assim o processo de seriação automático, respeitando tipo de regimento imposto no anúncio. No final da seriação esta informação é registada no sistema, prosseguimos para a adjudicação automática do anuncio. No final o sistema regista os dados da adjudicação.

### SSD
![UC13_SSD.svg](UC13_SSD.svg)

### Formato Completo

#### Ator principal

Tempo

#### Partes interessadas e seus interesses
* **Organização:** pretende contratar a pessoa externa(outsourcing) mais apta, de todas que se candidataram, para a realização de uma determinada tarefa e com competências técnicas apropriadas.

* **Freelancer:** pretende conhecer as classificaçõs das suas candidaturas à realização de determinados anúncios publicados na plataforma.

* **Colaborador de Organização:**  No cenário de atribuição ser opcional o Colaborador de Organização pretende conhecer a classificação das candidaturas para poder ajdudicá-los. 

* **T4J:** pretende satisfazer as organizações e os freelancer facilitando a contratação de freelancers pelas organizações e vice-versa.

#### Pré-condições
* Haver o agendamento da seriação automática no final da UC8.
* A seriação automática ser agendada para depois do final do período de apresentação de candidaturas e antes do termino do periodo de publicitação.
* Não ter havido seriação manual prévia do anuncio.
* Haver candidatos ao anuncio a seriar.
* Estar especificado no tipo de regimento o critério objetivo desta seriação.

#### Pós-condições
* A informação do processo de seriação é registada no sistema.
* A tarefa fica adjudicada e é registada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. Quando atingir o Tempo/Data agendado o Timer inicia o processo de seriação automático.
2. O sistema verifica se estão reunidas as condições para para que esta seriação seja possivel, ou seja, não ter havido seriação manual prévia e o anuncio em questão possuir critérios de seriação objetivos.
3. Depois de ter sido verificado as condições, o processo de seriação automático inicia-se, respeitando o tipo de regimento imposto no anúncio.
4. Os dados do processo de seriação são registados no sistema.
4. Após os dados da seriação serem registados é verificado se o processo de atribuição é obrigatorio, se sim é feita uma adjudicação automática do anuncio.
5. A adjudicação é registada no sistema.

#### Extensões (ou fluxos alternativos)

4a. Após os dados da seriação serem registados é verificado se o processo de atribuição é obrigatorio, se sim é feita uma adjudicação automática do anuncio.
>   
        > 1. O sistema verifica o processo de atribuição é opcional.
              O processo de adjudicação é feito no caso de uso 14.


#### Requisitos especiais
* **Os Tipos de regimento podem ser especificados por terceiros "TimeBasedRanking"**

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência

* Uma vez, sempre na data/hora que for estipulado no final da UC8.

#### Questões em aberto

* Alguém deve ser notificado da conclusão da operação?
* O mesmo lugar/classificação pode ser atribuído a mais do que uma candidatura (e.g. em caso de empate)?
* Há algum motivo que possa levar à desclassificação de uma candidatura?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC13_MD.svg](UC13_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O timer inicia o processo automático de seriação de candidaturas.| ...cordena o UC? |  SeriarCandidaturasAnuncioTask | IE: possui o anuncio a ser seriado informações sobre todos os candidatos que devem ser seriados. |
| | ... fica responsável por criar o(s) agendador(es)? | Anuncio | IE: contém as configurações definidas para a execução |
| | ... cria a instância de ProcessoSeriacao? | Anuncio | IE: No MD, 1 anúncio espoleta zero ou mais ProcessoSeriacao. |
| 2. O sistema seria as candidaturas de anúncio considerando as várias restrições objetivas. | ... conheces as restrições ? | Anuncio | IE: O anúncio possui as informações sobre como o mesmo deve ser seriado. |
| | ... conhece as candidaturas do anúncio? | ListaCandidatura | HC+LC (Anuncio delega funções para ListaCandidatura) |
| | ... deve ser tida em conta para a seriação? | Candidatura | IE: A seriação será realizada de acordo com aspetos de cada candidatura|
| | ... cria instância de Candidatura? | ProcessoSeriacao | Creator: No MD de um ProcessoSeriacao resultam várias classificações. |
| 3. Os passo 2 só ocorre no caso de esta ser a primeira seriação |... conhece as seriações? | Anuncio | IE: possui os dados sobre se já ocorreu alguma seriação. |
| 4. O sistema valida e regista a informacao da seriacao. | ...valida os dados do ProcessoSeriação (validação local)? | ProcessoSeriacao| IE: Possui os seus própios dados. |
| | ..valida os dados do ProcessoSeriação (validação global)? | Anuncio  | IE: Anuncio possui/agrega ProcessoSeriacao |
| | ...guarda o ProcessoSeriacao criado | Anuncio |  IE: Anuncio possui/agrega ProcessoSeriacao |
| 5. O processo automático de adjudicacao de candidaturas de anúncio resulta do termino do processo de seriação. | ... cria a instância de Adjudicacao? | RegistoAdjudicacao| Creator + HC+LC (Plataforma delega funções para o RegistoAdjudicacao). |
| 6. O sistema atribui a tarefa ao respetivo freelancer. |  ... conhece o Freelancer a que a tarefa foi atribuída? | Candidatura | IE: No MD uma candidatura refere 1 Freelancer. |
| | ... informa o freelancer? | RegistoAdjudicacao | HC+LC (Plataforma delega funções para RegistoAdjudicacao) |
| 7. Os passos 5 a 6 só ocorrem no caso a atribuição esteja especificada pelo regimento como sendo obrigatória. |... conhece o caráter da atribuição? | TipoRegimento | IE: possui os dados sobre seriação e atribuição de um anúncio. |
| 8. O sistema valida e regista a informacao e informa do sucesso da operação. | ..valida os dados da Adjudicacao (validação local)? | Adjudicacao | IE: Possui os seus própios dados. |
| | ...valida os dados da Adjudicacao (validação global)? | RegistoAdjudicacao | HC+LC (Plataforma delega funções para RegistoAdjudicacao) |
| | ...guarda a Adjudicacao criada | RegistoAdjudicacao |  HC+LC (Plataforma delega funções para RegistoAdjudicacao) |

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Anuncio
 * TipoRegimento
 * ProcessoSeriacao
 * Classificacao
 * Candidatura
 * Freelancer
 * Adjudicacao

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * RegistoAdjudicacao
 * ListaCandidatura
 * Timer
 * SeriarCandidaturasAnuncioTask

###	Diagrama de Sequência

![UC13_SD.svg](UC13_SD.svg)

![UC13_SD_SeriarCandidaturaSubmetida.svg](UC13_SD_SeriarCandidaturaSubmetida.svg)

![UC13_SD_AdjudicarCandidatura.svg](UC13_SD_AdjudicarCandidatura.svg)

###	Diagrama de Classes

![UC13_CD.svg](UC13_CD.svg)

![CD_TipoRegimento.svg](CD_TipoRegimento.svg)

