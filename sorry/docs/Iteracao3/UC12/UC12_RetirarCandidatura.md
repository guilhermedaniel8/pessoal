# UC 12 - Retirar Candidatura

## 1. Engenharia de Requisitos

### Formato Breve

O freelancer inicia a remoção de uma candidatura previamente efetuada por si.  O sistema solicita a escolha de uma candidatura de uma lista de candidaturas efetuada por si **que ainda esteja dentro do período de apresentação de candidaturas**.  O freelancer identifica a candidatura que pretende remover. O sistema apresenta a candidatura selecionada para ser removida e solicita confirmação. O freelancer confirma. O sistema remove a candidatura e informa o freelancer do sucesso da operação.


### SSD
![UC12-SSD.svg](UC12_SSD.svg)


### Formato Completo

#### Ator principal

* Freelancer

#### Partes interessadas e seus interesses


* **Colaborador de Organização**: pretende receber candidaturas devidamente atualizadas aos Anúncios publicados.
* **Freelancer:** pretende remover a Candidatura previamente efetuada por si .
* **Organização:** pretende receber Candidaturas para as Tarefas publicadas com o devido consentimento do freelancer.
* **T4J:** pretende receber as Candidaturas que os freelancer realmente se pretendem candidatar para posterior atribuição.

#### Pré-condições

* O freelancer deve ter efetuado, pelo menos, uma candidatura, e que esta ainda se encontre no período de candidaturas..

#### Pós-condições

* Remoção da candidatura pretendida pelo freelancer.

#### Cenário de sucesso principal (ou fluxo básico)

1. O freelancer inicia a remoção de uma candidatura previamente efetuada por si. 
2. O sistema solicita a escolha de uma candidatura de uma lista de candidaturas efetuada por si **que ainda esteja dentro do período de apresentação de candidaturas**. 
3. O freelancer identifica a candidatura que pretende remover. 
4. O sistema apresenta a candidatura selecionada para ser removida e solicita confirmação.
5. O freelancer confirma.
6. O sistema remove a candidatura e informa o freelancer do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O freelancer solicita o cancelamento da remoção da candidatura.
> O caso de uso termina.

2a. Não existem candidaturas efetuadas pelo freelancer.
> O caso de uso termina.



#### Requisitos especiais

-

#### Lista de Variações de Tecnologias e Dados

-

#### Frequência de Ocorrência

-

#### Questões em aberto

* Pode remover mais que uma candidatura em simultâneo?
* Qual a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC12_MD.svg](UC12_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O freelancer inicia a remoção de uma candidatura previamente efetuada por si.   |  ...interage com o utilizador?   |   RetirarCandidaturaUI     |   Pure Fabrication   |
|    |   ...coordena a UC?  |  RemoverCandidaturaController  |  Controller  |
|    |   ...conhece o utilizador/Freelancer a usar o sistema? | SessaoUtilizador | IE: cf. documentação do componente de gestão de utilizadores.  |
|    |  ...conhece o freelancer? | Plataforma  |  Conhece todos os Freelancers. |
|    |    |      RegistoFreelancer  | Por aplicação de HC+LC delega a RegistoFreelancer |
| 2. O sistema solicita a escolha de uma candidatura de uma lista de candidaturas efetuada por si **que ainda esteja dentro do período de apresentação de candidaturas**.   |   ...conhece as candidaturas?   |    Anúncio |  IE: no MD o Anúncio possui Candidaturas.       |
|    |     |      ListaCandidaturas  |  IE: no MD o Anúncio possui Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas.  |
|    |  ...conhece os períodos de apresentação de candidaturas? |  Anúncio | O anúncio agrega diversos períodos de uma tarefa. |
| 3. O freelancer identifica a candidatura que pretende remover.  |                 |             |                              |
| 4. O sistema apresenta a candidatura selecionada para ser removida e solicita confirmação.           |  ... valida os dados da remoção da Candidatura (validação global)?   |   ListaCandidaturas   | IE: no MD o Anúncio recebe Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas  |
| 5. O freelancer confirma.          |      |        |              |
| 6. O sistema remove a candidatura e informa o freelancer do sucesso da operação.         |  ...informa o colaborador?       |  RemoverCandidaturaUI           |                  |              

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Candidatura
 * Freelancer
 * Anúncio

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * AtualizarCandidaturaUI  
 * AtualizarCandidaturaController
 * RegistoFreelancer
 * RegistoAnuncios
 * ListaCandidaturas

Outras classes de sistemas/componentes externos:

* SessaoUtilizador


###    Diagrama de Sequência

![UC12_SD.svg](UC12_SD.svg)

![UC12-SD-getCandidaturasFree.svg](UC12_SD_getCandidaturasFree.svg)


###    Diagrama de Classes

![UC12_CD.svg](UC12_CD.svg)
