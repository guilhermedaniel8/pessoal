# UC 14 - Adjudicar/Atribuir(manualmente)Anúncio

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador de organização inicia o processo de adjudicação de um anuncio. O sistema mostra uma lista de anuncios disponiveis para adjudicar. O colaborador de organização seleciona um anuncio. O sistema apresenta o candidato a atribuir a tarefa, o periodo afeto à realização da mesma, o valor acordado entra ambas as partes e solicita confirmação. O colaborador de organização confirma. O sistema guarda as informações e informa o colaborador de organização do sucesso da operação.


### SSD
![UC14-SSD](UC14_SSD.svg)


### Formato Completo

#### Ator principal

Colaborador de Organização

#### Partes interessadas e seus interesses

(identificar as partes interessadas e seus interesses)

* **Colaborador de Organização:** pretende adjudicar o anúncio a um freelancer.
* **Freelancer:** pretende ter a possiblidade de um anúncio lhe ser atribuido.
* **Organização:** pretende que o anúncio seja adjudicado a um freelancer de modo que este possa realizar a tarefa proposta.

#### Pré-condições

-

#### Pós-condições

- A tarefa fica adjudicada a um dos candidatos

#### Cenário de sucesso principal (ou fluxo básico)

1. O Colaborador de Organização inicia o processo de atribuição de uma tarefa.
2. O sistema mostra uma lista de anuncios disponiveis para adjudicar.
3. O Colaborador de Organização seleciona um anúncio.
4. O sistema apresenta o candidato a atribuir o anúncio e a informação relavante.
5. O Colaborador de Organização confirma.
6. O sistema guarda as informações e informa o Colaborador de Organização do sucesso da operação.


#### Extensões (ou fluxos alternativos)


*a. O colaborador solicita o cancelamento do processo de adjudicação de uma anúncio.
> O caso de uso termina.

5. O colaborador de organização decide não adjudicar o anúncio ao freelancer.
>	1. O sistema aprensenta a decisão e pede confirmação.
>   2. O colaborador de organização confirma.
>   3. O caso de uso termina.
>
    >2a. O colaborador de organização não confirma.
    >3a. O passo 4 repete-se.

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

- Que informações sobre o freelancer e a candidatura devem ser apresentadas ao colaborador?
- O colaborador de organização pode adjudicar o anúncio a outro freelancer sem ser o apresentado?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC14_MD.svg](UC14_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1.O colaborador de organização inicia o processo de ajudicação de um anuncio|...Interage com o utilizador?|AdjudicarAnuncioUI|PureFabrication|
||... coordena o UC?|AdjudicarAnuncioController|Controller|
||...conhece o utilizador a usar o sistema? |SessaoUtilizador|IE: documentação do componente de gestão de utilizadores|
|||Organização|IE: conhece os seus colaboradores|
||...conhece a que organização o utilizador pertence?|Registo Organização|IE: Conhece todas as organizações (HC+LC)|
||...conhece o registo organização?|Plataforma|IE: Plataforma tem registo organização (HC+LC)|
|2.O sistema mostra uma lista de anuncios disponiveis para adjudicar|...conhece os anuncios publicados pelo colaborador?|Registo Anuncio| IE: Conhece todos os anuncios(HC+LC)|
||...conhece o registo anuncio?|Plataforma|IE: Plataforma tem registo anuncio (HC+LC)
|3.O colaborador seleciona um anuncio|...criar a instancia de adjudicação?|Registo Adjudicação|Creator(regra1) : O registo adjudicação contem/agrega adjudicação|
||...conhece os dados necessarios para criar a adjudicação?| Anuncio | O anuncio é relativo a uma tarefa|
|||Candidatura| A candidatura guarda o tempo realização, montante a receber e o freelancer|
|||RegistoAdjudicacao| O registo adjudicacao gera o numero sequencial e regista a data da adjudicação.
||...valida os dados da adjudicaçao?(local)|Adjudicação|IE: Possui os seus proprios dados
|4. O sistema apresenta os dados da adjudicação e solicita confirmação.|||
|5. O colaborador de organização confirma|... valida os dados da adjudicação?(global)|RegistoAdjudicacao|IE: no MD a Plataforma guarda Adjudicacao. Por aplicação de HC+LC delega a RegistoAdjudicacao.|




           

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Organização
 * Colaborador
 * Freelancer
 * Anuncio
 * ProcessoSeriacao
 * Tarefa
 * Candidatura

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * AdjudicarAnuncioUI  
 * AdjudicarAnuncioController
 * ListAnuncios
 * ListColaboradores
 * ListAdjudicacoes
 * RegistoOrganizacoes
 * RegistoAdjudicacao
 * RegistoAnuncios


###	Diagrama de Sequência

![UC14_SD.png](UC14_SD.svg)

![UC14_SD_getAnunciosPorAdjudicar.svg](UC14_SD_getAnunciosPorAdjudicar.svg)


###	Diagrama de Classes

![UC14_CD.svg](UC14_CD.svg)
