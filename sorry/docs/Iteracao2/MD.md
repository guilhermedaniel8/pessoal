# Análise OO #
O processo de construção do modelo de domínio é baseado nos casos de uso, em especial os substantivos utilizados, e na descrição do enunciado.
## Racional para identificação de classes de domínio ##
Para a identificação de classes de domínio usa-se a lista de categorias das aulas TP (sugeridas no livro). Como resultado temos a seguinte tabela de conceitos (ou classes, mas não de software) por categoria.

### _Lista de Categorias_ ###

**Transações (do negócio)**

*

---

**Linhas de transações**

*

---

**Produtos ou serviços relacionados com transações**

*  Tarefa
*  Anúncio
*  Candidatura
*  Anúncio Seriado

---


**Registos (de transações)**

*

---  


**Papéis das pessoas**

* Administrativo
* Freelancer
* Colaborador (de Organização)
* Gestor (de Organização)
* Utilizador
* Utilizador Não Registado

---


**Lugares**

*  Endereço Postal
*  Plataforma

---

**Eventos**

*  Candidatura

---


**Objectos físicos**

*

---


**Especificações e descrições**

*  Área de Atividade
*  Competência Técnica
*  Categoria (de Tarefa)
*  Tarefa
*  Reconhecimento
*  Tipo de Regimento
*  Anúncio
*  Grau de Proficiência
*  Habilitação
*  Anúncio Seriado

---


**Catálogos**

* 

---


**Conjuntos**

* 

---


**Elementos de Conjuntos**

*  Caráter de Competência Técnica Requerida
*  Grau de Proficiência


---


**Organizações**

*  T4J (Plataforma)
*  Organização

---

**Outros sistemas (externos)**

*  (Componente Gestão Utilizadores)


---


**Registos (financeiros), de trabalho, contractos, documentos legais**

*

---


**Instrumentos financeiros**

*

---


**Documentos referidos/para executar as tarefas/**

*

---



## **Racional sobre identificação de associações entre classes** ##

Uma associação é uma relação entre instâncias de objetos que indica uma conexão relevante e que vale a pena recordar, ou é derivável da Lista de Associações Comuns:

+ A é fisicamente (ou logicamente) parte de B
+ A está fisicamente (ou logicamente) contido em B
+ A é uma descrição de B
+ A é conhecido/capturado/registado por B
+ A usa ou gere B
+ A está relacionado com uma transação de B
+ etc.



| Conceito (A) 		|  Associação   		|  Conceito (B) |
|----------	   		|:-------------:		|------:       |
| Administrativo  	| define    		 	| Área de Atividade  |
|   					| define            | Competência Técnica  |
|   					| trabalha para     | Plataforma  |
|						| atua como			| Utilizador |
| Plataforma			| tem registadas    | Organização  |
|						| tem/usa    			| Freelancer  |
|						| tem     			| Administrativo  |
| 						| possui     			| Competência Técnica  |
| 						| possui     			| Área de Atividade  |
|                                             | possui                          | Tarefa  |
| 						| possui     			| Categoria (de Tarefa)  |
|                                             | possui                         | Tipo de Regimento  |
| Competência Técnica| referente a       | Área de Atividade  |
|                                   | aplica    | Grau de Proficiência  |
| Categoria (de Tarefa)| enquadra-se em | Área de Atividade  |
|						| requer 					| Competência Técnica |
| Caráter de Competência Técnica Requerida |  é referente a | Competência Técnica |
|                           | exige (como mínimo) | Grau de Proficiência |
| Organização			| localizada em 	   | Endereço Postal  |
|						| tem gestor     	| Colaborador |
|						| tem		     		| Colaborador |
| Tarefa		    	| enquadra-se	em 		| Categoria |
|         		    	| especificada por 	| Colaborador |
|                              | publicada por  | Colaborador |
|                              | resulta | Anúncio |
| Gestor (de Organização)| é um (papel de) | Colaborador |
| Freelancer			| atua como			| Utilizador |
|                                     | efetua                          | Candidatura |
|                                     | localizado em              | Endereço Postal |
|                                     | tem                              | Habilitação |
|                                     | tem                              | Reconhecimento |
| Colaborador			| atua como			| Utilizador |
|                                     | especifica                    | Anúncio |
|                                     | seria                             | Anúncio |
| Reconhecimento         | tem                              | Competência Técnica |
|                                     | tem                              | Grau de Proficiência |
| Anúncio                       | tem                              | Candidatura |
|                                     | possui                          | Tipo de Regimento |
|                                     | resulta                          | Anuncio Seriado |


## Modelo de Domínio

![MD.svg](MD.svg)

### Sumário

Um Modelo de Domínio representa uma visão parcial e aproximada do problema/negócio em mãos.

Pretendeu-se com isto demonstrar que não existe apenas um Modelo de Domínio correto.
Outros podem existir e serem aceitáveis.
Também não se pretendeu ser exaustivo nas possíveis alternativas e, portanto, podem existir ainda outras alternativas aceitáveis.

O importante é que este capture (sem falhas) os principais conceitos e associações existentes entre conceitos.

Salienta-se ainda que se introduziu a notação UML relativa a classes associativas.
