# UC7 - Registar Freelancer

## 1. Engenharia de Requisitos

### Formato Breve

O administrativo inicia o registo de um novo freelancer. O sistema solicita os dados necessários sobre o novo freelancer(i.e. nome, nif, endereço postal, contacto e email). O administrativo introduz os dados solicitados. O sistema solicita os dados necessários sobre as habilitações académicas(i.e. grau, designação do curso, instituição e a média do curso). O administrativo introduz os dados solicitados. A introdução da HA repete-se até que todas as HA que o freelancer possui tenham sido introduzidas. O sistema apresenta todas as CT e pede que seja selecionada a CT que o freelancer possui (tendo em conta os reconhecimentos de competências técnicas conduzidos). O administrativo seleciona a CT correspondente. O sistema pede o grau de proficiência que o freelancer possui para esta competência técnica. O administrativo seleciona o grau de proficiência correspondente. O sistema solicita a data do reconhecimento da CT. O administrativo introduz a data. A seleção da CT assim como do seu GD e a introdução da data, repete-se até que todas as competências técnicas que o Freelancer possui tenham sido selecionadas pelo administrativo. O sistema valida e apresenta todos os dados ao administrativo, pedindo que os confirme. O administrativo confirma. O sistema regista o Freelancer e informa o administrativo do sucesso da operação.

### SSD

![UC7_SSD.svg](UC7_SSD.svg)

### Formato Completo

#### Ator principal

Administrativo

#### Partes interessadas e seus interesses
* **Administrativo:** pretende registar o Freelancer para este posteriormente poder-se candidatar as tarefas.
* **Colaborador de Organização:** pretende que o Freelancer se registe para poder candidatar-se as tarefas.
* **Freelancer:** pretende candidatar-se ao anúncio que resulta da publicação da tarefa.
* **T4J:** pretende que os freelancers se candidatem às tarefas anunciadas.
 
#### Pré-condições

N/A

#### Pós-condições

A informação do Freelancer é registada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O administrativo inicia o registo de um novo freelancer. 
2. O sistema solicita os dados necessários (i.e. nome, nif, endereço postal, contacto e email). 
3. O administrativo introduz os dados solicitados.
4. O sistema solicita os dados necessários sobre as habilitações académicas(i.e. grau, designação do curso, instituição e média).
5. O administrativo introduz os dados solicitados.
6. Os passos 4 a 5 repetem-se até que todas as  habilitações académicas que o Freelancer possui tenham sido introduzidas.
7. O sistema apresenta todas as competências técnicas existentes e pede para selecionar a que é referente ao Freelancer.
8. O administrativo seleciona a competência técnica referente ao Freelancer.
9. O sistema apresenta os graus de proficiência referentes a competência técnica previamente selecionada e pede para selecionar o grau de proficiência que o Freelancer possui.
10. O administrativo seleciona grau de proficiência.
11. O sistema solicita a data do reconhecimento da competência técnica.
12. O administrativo introduz a data do reconhecimento da competência técnica.
13. Os passos 7 a 12 repetem-se até que todas as competências pretendidas estejam selecionadas assim como o grau de proficiência do Freelancer.
14. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. 
15. O administrativo confirma. 
16. O sistema **regista o Freelancer e torna-o um utilizador registado** e informa o administrativo do sucesso da operação.

#### Extensões (ou fluxos alternativos)

*a. O administrativo solicita o cancelamento do registo de freelancer.

> O caso de uso termina.

 7a. O sistema deteta que o administrativo não possui competências técnicas.
 >    1. O sistema alerta o administrativo para o facto não existir competências técnicas definida.
 >    2. O sistema questiona o administrativo se deseja definir competências técnicas, para posterior seleção.
 >
         > 2a. O administrativo não introduz competência técnica.
               O caso de uso termina.
         > 2b. O administrativo introduz competências técnicas.
               O sistema volta ao início do passo 6.

 8a. O sistema deteta que a competência técnica escolhida já tinha sido introduzida.
 >    1. O sistema alerta o administrativo para o facto de estar a repetir a competência técnica não permitindo a sua introdução.
 >    2. O sistema continua o processo (passo 7).

9a. Sistema deteta que a lista dos graus de proficiência referentes a CT selecionada está vazia.
>   1. O sistema alerta o administrativo para o facto da lista dos graus de proficiência estar vazia e permite a sua introdução.
>    2. O sistema questiona o administrativo se deseja introduzir graus de proficiência, para posterior seleção.
> 
        > 2a. O administrativo não introduz graus de proficiência.
              O caso de uso termina.
        > 2b. O administrativo introduz graus de proficiência.
              O sistema volta ao início do passo 8.

12a. O sistema deteta que não foi introduzido uma data para o reconhecimento da competência técnica.
>    1. O sistema alerta o administrativo para o facto de não ter introduzido uma data.
>    2. O sistema volta ao início do passo 10.

12b. O sistema deteta que a data para o reconhecimento da competência técnica não é válida.
>    1. O sistema alerta o administrativo para o facto de não ter introduzido uma data válida.
>    2. O sistema volta ao início do passo 10.

14a. Dados mínimos obrigatórios em falta(i.e. nome, nif, endereço postal, contacto e email).
>    1. O sistema informa quais os dados em falta.
>    2. O sistema permite a introdução dos dados em falta (passo 3)
>
        >    2a. O administrativo não altera os dados. O caso de uso termina.

14b. O sistema deteta que os dados (NIF e email) introduzidos devem ser únicos e que já existem no sistema.
>    1. O sistema alerta o administrativo para o facto.
>    2. O sistema permite a sua alteração (passo 3)
>
        >    2a. O administrativo não altera os dados. O caso de uso termina.

14c. Dados mínimos obrigatórios em falta sobre as habilitações académicas(i.e. grau, designação do curso, instituição e média).
>    1. O sistema informa quais os dados em falta.
>    2. O sistema permite a introdução dos dados em falta (passo 5)
>
        >    2a. O administrativo não altera os dados. O caso de uso termina.


#### Requisitos especiais
N/A

#### Lista de Variações de Tecnologias e Dados
N/A

#### Frequência de Ocorrência
N/A

#### Questões em aberto

* Existem outros dados que são necessários?
* Qual o formato da designação do curso?
* Qual o formato da data para o reconhecimento da competência técnica?
* Como é que o administrativo recebe a informação relativa ao freelancer?
* Qual a frequência de ocorrência deste caso de uso?
* O contacto telefónico tem de ser unico para cada freelancer?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC7_MD.svg](UC7_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O administrativo inicia o registo de um novo freelancer.  | ...interage com o utilizador? | RegistarFreelancerUI | Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existente no Modelo de Domínio. |
| | ... coordena o UC?    | RegistarFreelancerController | Controller |
| | ... cria instâncias de Freelancer? | RegistarFreelancer | As responsabilidades da plataforma passarão para o RegistarFreelancer (HC + LC) + Creator com HC + LC: RegistarFreelancer possui Freelancer.|
| | ...conhece o utilizador/administrativo a usar o sistema? | SessaoUtilizador | IE: cf. documentação do componente de gestão de utilizadores. |
| 2. O sistema solicita os dados necessários (i.e. nome, nif, endereço postal, contacto e email). | | | |
| 3. O administrativo introduz os dados solicitados. | ... guarda os dados introduzidos? | RegistoFreelancer | HC + LC -instância criada no passo 1: possui os dados do Freelancer. |
| 4. O sistema solicita os dados necessários sobre as habilitações académicas(i.e. grau, designação do curso, instituição e média). | | | |
| 5. O administrativo introduz os dados solicitados. | ... guarda os dados sobre as habilitações académicas introduzidos? | HabilitacaoAcademica | IE: Possui os seus próprios dados |
| | quem guarda habilitações académicas ? | Freelancer | IE: O Freelancer agrega/contém HabilitacaoAcademica. |
| 6. Os passos 4 a 5 repetem-se até que todas as  habilitações académicas que o Freelancer possui tenham sido introduzidas. | | | |              
| 7. O sistema apresenta a lista de competências técnicas e pede para selecionar a que o Freelancer possui. | ... conhece as competências técnicas? | RegistoCompetenciaTecnica | HC + LC : O RegistoCompetenciaTecnica possui todas as instâncias de CompetenciaTecnica |
| 8. O administrativo seleciona a competência técnica referente ao Freelancer. |  ... guarda a competência técnica selecionada? | Reconhecimento | IE: no MD cada Freelancer tem vários Reconhecimentos sendo cada um referente a uma CompetenciaTecnica (CT). |
| 9. O sistema apresenta os graus de proficiência referentes a competência técnica previamente selecionada e pede para selecionar o grau de proficiência que o Freelancer possui. | ... conhece os graus de proficiência? | RegistoCompetenciaTecnica | HC + LC : O RegistoCompetenciaTecnica possui todas as instâncias de CompetenciaTecnica |
| 10. O administrativo seleciona grau de proficiência. |  ... guarda o grau proficiência selecionado? | Reconhecimento | IE: no MD cada Freelancer tem vários Reconhecimentos sendo cada uma referente a um GrauProficiencia. |
| 11. O sistema solicita a data do reconhecimento da competência técnica. | | | |
| 12. O administrativo introduz a data do reconhecimento da competência técnica. | ... guarda a data do reconhecimento da competência técnica introduzida? | Reconhecimento | IE: Possui os seus próprios dados | 
| | quem guarda o reconhecimento? | Freelancer | IE: O Freelancer agrega/contém Reconhecimento. |
| 13. Os passos 7 a 12 repetem-se até que todas as competências pretendidas estejam selecionadas assim como o grau de proficiência do Freelancer. | | | |
| 14. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.  | ... valida os dados do Freelancer (validação local)? | RegistoFreelancer | HC + LC : possui os dados do Freelancer. |      
| | ... valida os dados do Freelancer (validação global)? | RegistoFreelancer | HC + LC : o RegistoFreelancer contém/agrega Freelancer. |
| 15. O administrativo confirma. | | | |
| 16. O sistema **regista o Freelancer e torna-o um utilizador registado** e informa o administrativo do sucesso da operação. | ... guarda o Freelancer criado? | RegistoFreelancer | HC + LC: o RegistoFreelancer contém/agrega Freelancer. |
| | ... regista/guarda o Utilizador referente ao Freelancer?| AutorizacaoFacade | IE. A gestão de utilizadores é responsabilidade do componente externo respetivo cujo ponto de interação é através da classe "AutorizacaoFacade" |


### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * RegistoFreelancer
 * RegistoCompetenciaTecnica
 * Freelancer
 * EnderecoPostal
 * HabilitacaoAcademica
 * Reconhecimento

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * RegistarFreelancerUI  
 * RegistarFreelancerController
 
 Outras classes de sistema/componentes externos:
 
 * SessaoUtilizador
 * AplicacaoPOT
* AutorizacaoFacade

### Diagrama de Sequência

![UC7_SD.svg](UC7_SD.svg)

### SD_RegistarFreelancerComoUtilizador ##

![SD_RegistarFreelancerComoUtilizador.svg](SD_RegistarFreelancerComoUtilizador.svg)

### Diagrama de Classes

![UC7_CD.svg](UC7_CD.svg)
