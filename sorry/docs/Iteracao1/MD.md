# Análise OO #
O processo de construção do modelo de domínio é baseado nos casos de uso, em especial os substantivos utilizados, e na descrição do enunciado.
## Racional para identificação de classes de domínio ##
Para a identificação de classes de domínio usa-se a lista de categorias das aulas TP (sugeridas no livro). Como resultado temos a seguinte tabela de conceitos (ou classes, mas não de software) por categoria.

### _Lista de Categorias_ ###

**Transações (do negócio)**

*

---

**Linhas de transações**

*

---

**Produtos ou serviços relacionados com transações**

*  

---


**Registos (de transações)**

*  

---  


**Papéis das pessoas**

* Administrativo
* Freelancer
* Colaborador (de Organização)
* Gestor (de Organizacao)
* Utilizador
* Utilizador Não Registado

---


**Lugares**

*  Endereço Postal

---

**Eventos**

* 

---


**Objectos físicos**

*

---


**Especificações e descrições**

*  Área de Atividade
*  Competência Técnica


---


**Catálogos**

*  

---


**Conjuntos**

*  

---


**Elementos de Conjuntos**

*  

---


**Organizações**

*  T4J (Plataforma)
*  Organização

---

**Outros sistemas (externos)**

*  (Componente Gestão Utilizadores)


---


**Registos (financeiros), de trabalho, contractos, documentos legais**

* 

---


**Instrumentos financeiros**

*  

---


**Documentos referidos/para executar as tarefas/**

* 
---



**Racional sobre identificação de associações entre classes**

| Conceito (A) 		|  Associação   		|  Conceito (B) |
|----------	   		|:-------------:		|------:       |
| Administrativo  	| define    		 	| Área de Atividade  |
|   					| define            | Competência Técnica  |
|   					| trabalha para     | Plataforma  |
|						| atua como			| Utilizador |
|  Área de Atividade     |  definido pelo                   | Administrativo  | 
|                                   | registado na                     | Plataforma  |
|                                   | contido em                       | Categoria de Tarefa  |
|                                   | referido em                       | Competência Técnica  |
| Categoria de Tarefa  | registada na                      | Plataforma  |
|                                   |  utilizada na                       | Tarefa  |
|                                   | possui                               | Área de Atividade  |
|                                   | tem                                    | Tipo de Competência  |
| Colaborador            | atua como            | Utilizador |
|                                     | especificado pelo           | Gestor  |
|                                     | trabalha para                  | Organização  |
|                                     | especifica                      | Tarefa  |
| CompetênciaTécnica | referente a       | Área de Atividade  |
|                                   | referido em       | Tipo de Competência  |
|                                   | defenida pelo     | Administrativo  |
|                                   | registada na        | Plataforma       |
| Endereço Postal             |  localiza  | Organização  |
| Freelancer            | atua como            | Utilizador |
|                                     | registado na               | Plataforma |
| Gestor (de Organização) | é um (papel de)| Colaborador |
|                                        | define  | Colaborador |
|                                        | trabalha para  | Organização  |
| Plataforma			| tem registadas    | Organização  |
|						| tem/ usa    			| Freelancer  |
|						| tem     			| Administrativo  |
| 						| possui     			| Competência Técnica  |
| 						| possui     			| Área de Atividade  |
|                                             | tem registadas              | Organização  |
|                                             | possui                          | Tarefa  |
|                                             | possui                          | Categoria de Tarefa  |
|                                             | possui                          | Tipo de Competência  |
| Organizacao			| localizada em      	   | EndereçoPostal  |
|						| tem     	| Colaborador |
|                                             | tem                              | Gestor           |
|                                       | registada na  | Plataforma  |
| Tarefa                         | especificada pelo            | Colaborador  |
|                                    | registada na                     | Plataforma  |
|                                    | possui                              | Categoria de Tarefa  |
|  Tipo de Competência  | registada na                  | Plataforma   |
|                                   | usada em                          | Categoria de Tarefa  |
|                                   |  referente a                        | Competencia Técnica  |



## Modelo de Domínio

![MD.svg](MD.svg)



