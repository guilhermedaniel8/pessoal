# UC6 - Especificar Tarefa

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador de organização inicia a especificação de uma nova tarefa. O sistema solicita os dados necessários (i.e. referência única, designação, descrição informal, descrição técnica, estimativa de duração, estimativa de custo). O colaborador de organização introduz os dados solicitados. O sistema apresenta categorias de tarefas e pede para selecionar uma. O colaborador de organização seleciona a categoria pretendida.  O sistema valida e apresenta todos os dados ao colaborador de organização e solicita confirmação. O colaborador de organização confirma. O sistema armazena os dados da nova tarefa e informa o colaborador de organização do sucesso da operação.


### SSD
![UC6-SSD](UC6_SSD.svg)


### Formato Completo

#### Ator principal

Colaborador de Organização

#### Partes interessadas e seus interesses


* **Colaborador de Organização:** pretende especificar as tarefas para posterior publicação.
* **Organização:** pretende que as tarefas se apresentem especificadas para publicação.
* **Freelancer:** pretende conhecer quais tarefas tem competências para posterior candidatura.

#### Pré-condições
As categorias de tarefas já devem estar definidas na plataforma pelo administrativo.

#### Pós-condições
Os dados das tarefas são armazenados no sistema para posterior publicação.

#### Cenário de sucesso principal (ou fluxo básico)


1. O colaborador de organização inicia a especificação de uma nova tarefa.
2. O sistema solicita os dados necessários (i.e. referência única, designação, descrição informal, descrição técnica, estimativa de duração, estimativa de custo).
3. O colaborador de organização introduz os dados solicitados .
4. O sistema apresenta as categorias de tarefas e pede para selecionar uma .
5. O colaborador de organização seleciona a categoria de tarefa pretendida.
6. O sistema valida e apresenta todos os dados ao colaborador de organização e solicita confirmação.
7. O colaborador de organização confirma.
8. O sistema armazena os dados da tarefa e informa o colaborador de organização do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O colaborador de organização solicita o cancelamento da especificação da tarefa.

> O caso de uso termina.

6a. Dados mínimos obrigatório em falta.
>    1. O sistema informa quais os dados em falta.
>    2. O sistema permite a sua alteração (passo 3).
>
        >   2a. O colaborador de organização não altera os dados. O caso de uso termina.

6b. O sistema deteta que os dados (ou algum subconjuntos dos dados) introduzidos devem ser únicos e que já existem no sistema
>    1. O sistema informa quais os dados em falta.
>    2. O sistema permite a introducação dos dados em falta (passo 3).
>
        >   2a. O colaborador de organização não introduz os dados. O caso de uso termina.

6c. O sistema deteta os dados introduzidos (ou algum conjunto dos dados) são inválidos.
>    1. O sistema alerta o administrativo para o facto.
>    2. O sistema permite a sua alteração (passo 3).
>
        >   2a. O colaborador de organização não altera os dados. O caso de uso termina.

#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados

\-

#### Frequência de Ocorrência

\-

#### Questões em aberto

* Quem define a referência única por organização?
* Quem está encarregue de publicar as tarefas?
* Qual a frequência de ocorrência deste caso de uso?
* Seria mais fácil saber também a área de atividade para a seleção da categoria de tarefa?
* Poderá existir mais que uma categoria de tarefa para uma tarefa?
* É necessário notificar o administrativo caso não exista uma categoria de tarefa representativa da tarefa a ser criada?
* E se a categoria de tarefa que caracteriza a tarefa não existir no sistema?
* Quais os colaboradores que irão ter acesso à tarefa antes da sua publicação?
* Todos os dados são obrigatórios?
* Existem mais dados necessários para além dos já conhecidos?
* Qual o formato das descrições?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC6_MD.svg](UC6_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O colaborador de organização inicia a especificação de uma nova tarefa  		 |	... interage com o colaborador de organização?						 |   EspecificarTarefaUI          |      Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe no Modelo de Domínio.                        |
| 		 |	... coordena a UC?						 |    EspecificarTarefaController         |      Controller                        |
|  		 |	... cria instância de Tarefa						 |     Plataforma        |       Creator (Regra 1)                       |
| 2. O sistema solicita os dados necessários (i.e. referência única, designação, descrição informal, descrição técnica, estimativa de duração, estimativa de custo)  		 |							 |             |                              |
| 3. O colaborador de organização introduz os dados solicitados. 		 |		... guarda os dados introduzidos?					 |    Tarefa         |     Information Expert (IE) - instância criada no passo 1                         |
| 4. O sistema mostra a lista de categorias de tarefa existentes para que seja selecionada uma.  		 |	... conhece as categorias de tarefa a listar?						 |   Plataforma          |       IE: Plataforma tem/agrega todas as categorias de tarefa                       |      
| 5. O colaborador de organização seleciona a categoria em que pretende catalogar a tarefa.           |     ... guarda a categoria selecionada?                        |     Tarefa        |     IE: Tarefa catalogada numa Categoria - instância criada no passo 1                         |  
| 6. A sistema valida e apresenta os dados, pedindo que os confirme.            |    ... valida os dados da Tarefa (validação local)                         |    Tarefa         |     IE: Tarefa possui os seus próprios dados                         |  
|               |       ... valida os dados da Tarefa (validação global)?                         |    Plataforma         |     IE: A Plataforma possui/agrega Tarefa                       |
| 7. O colaborador de organização confirma.           |                             |             |                              |
|                              |
| 8. O sistema regista os dados e informa o colaborador de organização do sucesso da operação           |       ... guarda a Tarefa criada?                         |             |      IE: No MD a plataforma possui Tarefa                       |                     

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Tarefa
 * CategoriaDeTarefa

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * EspecificarTarefaUI  
 * EspecificarTarefaController


###	Diagrama de Sequência

![UC6-SD](UC6_SD.svg)


###	Diagrama de Classes

![UC6-CD](UC6_CD.svg)
