
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Objects;

public class TipoCompetencia {
    
    private boolean obrigatoriedade;
    
    private CompetenciaTecnica competencia;
    
    public TipoCompetencia(CompetenciaTecnica competencia , boolean obrigatoriedade){
        this.obrigatoriedade = obrigatoriedade;
        this.competencia = new CompetenciaTecnica(competencia);
    }

    public boolean isObrigatoriedade() {
        return obrigatoriedade;
    }

    public CompetenciaTecnica getCompetencia() {
        return competencia;
    }

    public void setObrigatoriedade(boolean obrigatoriedade) {
        this.obrigatoriedade = obrigatoriedade;
    }

    public void setCompetencia(CompetenciaTecnica competencia) {
        this.competencia = competencia;
    }
    
        public boolean hasCompetencia(CompetenciaTecnica competencia) {
        return this.competencia.equals(competencia);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.competencia);
        return hash;
    }
    
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }
        TipoCompetencia outroTipo = (TipoCompetencia) outroObjeto;
        return this.obrigatoriedade==outroTipo.obrigatoriedade && this.competencia.equals(outroTipo.competencia);
    }
    
    @Override
    public String toString(){
        if (obrigatoriedade == true) {
        return String.format("Competência Tecnica é Obrigatória: %s", this.competencia.toString());  
        }
        return String.format("Competência Tecnica é Desejável: %s", this.competencia.toString());
        }
}
