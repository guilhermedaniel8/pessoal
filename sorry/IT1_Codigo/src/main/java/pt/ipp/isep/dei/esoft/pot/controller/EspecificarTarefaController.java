
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.pot.model.CategoriaDeTarefa;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Tarefa;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;


public class EspecificarTarefaController {

    private Plataforma m_oPlataforma;
    private Tarefa m_oTarefa;

    public EspecificarTarefaController() {
        if (!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_COLABORADOR_ORGANIZACAO)) {
            throw new IllegalStateException("Colaborador não Autorizado");
        }
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }

    public List<CategoriaDeTarefa> getCategoriasTarefa() {
        return this.m_oPlataforma.getCategoriasTarefa();
    }

    public boolean novaTarefa(String refUnica, String designacao, String dInformal, String dTecnica, Double duracao, Double custo, String catId) {

        try {
            CategoriaDeTarefa categ = this.m_oPlataforma.getCategoriaByID(catId);
            this.m_oTarefa = this.m_oPlataforma.novaTarefa(refUnica, designacao, dInformal, dTecnica, duracao, custo, categ);
            return this.m_oPlataforma.validaTarefa(this.m_oTarefa);

        } catch (RuntimeException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oTarefa = null;
            return false;
        }
    }

    public boolean registaTarefa() {
        return this.m_oPlataforma.registaTarefa(this.m_oTarefa);
    }
    
    public String getTarefaString(){
        return this.m_oTarefa.toString();
    }

}
    
