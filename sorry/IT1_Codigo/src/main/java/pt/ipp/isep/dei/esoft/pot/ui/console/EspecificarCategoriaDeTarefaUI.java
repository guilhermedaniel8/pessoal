package pt.ipp.isep.dei.esoft.pot.ui.console;

import java.util.List;
import pt.ipp.isep.dei.esoft.pot.controller.EspecificarCategoriaDeTarefaController;
import pt.ipp.isep.dei.esoft.pot.model.AreaAtividade;
import pt.ipp.isep.dei.esoft.pot.model.CompetenciaTecnica;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

public class EspecificarCategoriaDeTarefaUI {

    private EspecificarCategoriaDeTarefaController m_controller;

    public EspecificarCategoriaDeTarefaUI() {
        m_controller = new EspecificarCategoriaDeTarefaController();
    }

    public void run() {
        boolean resposta;
        System.out.println("\nEspecificar Categoria De Tarefa:");

        if (introduzDados()) {
            do {
                introduzDados2();
                resposta = Utils.confirma("Deseja Selecionar mais competências técnicas? (S/N)");
            } while (resposta == true);

            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaCategoriaDeTarefa()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        } else {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }

    }

    private boolean introduzDados() {
        String identificador = Utils.readLineFromConsole("Identificador: ");
        String descricao = Utils.readLineFromConsole("Descrição: ");

        List<AreaAtividade> listaAreasAtividade = m_controller.getAreasAtividade();

        String areaAtividadeId = "";
        AreaAtividade areaAtividade = (AreaAtividade) Utils.apresentaESeleciona(listaAreasAtividade, "Seleciona a área de atividade pretendida:");
        if (areaAtividade != null) {
            areaAtividadeId = areaAtividade.getCodigo();
        }
   
        return m_controller.novaCategoriaDeTarefa(identificador, descricao, areaAtividadeId);
    }

    private boolean introduzDados2() {
        List<CompetenciaTecnica> listaCompetenciasTecnicas = m_controller.getCompetenciaTecnica();
        
        String competenciaTecnicaId = "";
        CompetenciaTecnica competencia = (CompetenciaTecnica) Utils.apresentaESeleciona(listaCompetenciasTecnicas , "Seleciona a competência técnica pretendida:");
        if (competencia != null) {
            competenciaTecnicaId = competencia.getCodigo();
        }
        boolean obrigatoriedade = Utils.confirma("A competências técnica selecionada é obrigatória? (S/N)");
        m_controller.addTipoCompetencia(competencia, obrigatoriedade);
        return m_controller.novoTipoCompetencia(competenciaTecnicaId, obrigatoriedade);
    }
    
    private void apresentaDados() {
        System.out.println("\nCategoria de Tarefa:\n" + m_controller.getCategoriaDeTarefaString());
    }
}
