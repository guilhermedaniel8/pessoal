
package pt.ipp.isep.dei.esoft.pot.ui.console;

import java.util.ArrayList;
import java.util.List;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

class MenuColaboradorOrganizacaoUI {
    
    public MenuColaboradorOrganizacaoUI()
    {
    }

    public void run() 
    {
        List<String> options = new ArrayList<String>();
        options.add("Especificar Tarefa");
        options.add("Publicar Tarefa");
        options.add("Seriar Anúncio");
        options.add("Adjudicar anuncio");
        
        int opcao = 0;
        do
        {            
            opcao = Utils.apresentaESelecionaIndex(options, "\n\nMenu Colaborador");

            switch(opcao) {
                case 0:
                    EspecificarTarefaUI uiCat = new EspecificarTarefaUI();
                    uiCat.run();
                    break;
                case 1:
                    PublicarTarefaUI uiServ = new PublicarTarefaUI();
                    uiServ.run();
                    break;
                
                case 2:
                    SeriarAnuncioUI uiUti = new SeriarAnuncioUI();
                    uiUti.run();
                    break;
                case 3:
                    AdjudicarAnuncioUI uiAdju = new AdjudicarAnuncioUI();
                    uiAdju.run();
                    break;
            }

            // Incluir as restantes opções aqui
            
        }
        while (opcao != -1 );
    }
    
}
