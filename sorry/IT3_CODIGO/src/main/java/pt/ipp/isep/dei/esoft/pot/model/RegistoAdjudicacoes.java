
package pt.ipp.isep.dei.esoft.pot.model;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author rui3m
 */
public class RegistoAdjudicacoes {
        private List<Adjudicacao> listaAdjudicacao;
        private static int cnt;

        public RegistoAdjudicacoes(){
            listaAdjudicacao = new ArrayList<>();
            cnt = 0;
        }
        
        public Adjudicacao novaAdjudicacao(String numSeq, Date dataAdjudi, String descTarefa, Integer periodoReali, Double valorRemu, String anuncioId){
            return new Adjudicacao(numSeq,dataAdjudi, descTarefa, periodoReali, valorRemu, anuncioId);
        }
        
        public boolean validaAdjudicacao(Adjudicacao adjudi){
            return getListaAdjudicacao().contains(adjudi);
        }
        
        public boolean addAdjudicacao(Adjudicacao adjudi){
            return getListaAdjudicacao().add(adjudi);
        }
        public int gerarNumPorAno(){
            int year = Calendar.getInstance().get(Calendar.YEAR);
            cnt++;
            return year+cnt;
        }
        public Date getDataAdjudicacao(){
            Date in = new Date();
            LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
            Date out = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
            return out;
        }


        public List<Adjudicacao> getListaAdjudicacao() {
            return listaAdjudicacao;
        }
}
