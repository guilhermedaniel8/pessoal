/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
/**
 *
 * @author guilhermedaniel
 */
public class RegistoAnuncio {

    private List<Anuncio> listaAnuncio = new ArrayList<Anuncio>();

    Date dataInicialCand;
    long duracaoPeriodocand;
    
    public List<Candidatura> getCandidaturasFreelancer(Freelancer free){
        List<Candidatura> lista = new ArrayList<>();
        for(Anuncio anu : listaAnuncio){
            dataInicialCand = anu.getDataInicialCand();
            duracaoPeriodocand = anu.duracaoPeriodoCand();
            boolean isCandidato = anu.isCandidatoAoAnuncio(free);
            ListaCandidaturas lstCand = anu.getListaCandidaturas();
            
            if(isCandidato == true){
                Candidatura can = lstCand.getCandidaturaByFree(free);
                if(can.verificaPeriodoCandidatura(dataInicialCand, dataInicialCand)){
                    lista.add(can);
                }
            }
        }
        return lista;
    }
    
    public Candidatura getCandSelecionada(String candidaturaId){
        Candidatura cand = null;
        for(Anuncio anu : listaAnuncio){
            ListaCandidaturas lstCand = anu.getListaCandidaturas();
            cand = lstCand.temIdCandidatura(candidaturaId);
        }
        return cand;
    }
    
    public Anuncio getAnuncioCand(Candidatura cand){
        Anuncio anuncioCand = null;
        for(Anuncio anu : listaAnuncio){
            ListaCandidaturas lstCand = anu.getListaCandidaturas();
            if(lstCand.temCandidatura(cand)){
                anuncioCand = anu;
            }
        }
        return anuncioCand;
    }
    public List<Anuncio> getAnunciosPorSeriarNaoAutomaticos(Colaborador colab){
        return null;
    }
    
    public Anuncio getAnuncioPublicadoPor(Colaborador colab, String anuncioId){
        Anuncio an;
        for(Anuncio anu : listaAnuncio){
            if(anu.getAnunId().equals(anuncioId)){
                if(anu.getColaborador().equals(colab)){
                    return anu;
                }
            }
        }
        return null;
            
    }

    
    public boolean validaPeriodos(Date dtIniP, Date dtFimP, Date dtIniC, Date dtFimC, Date dtInS, Date dtFimS){
        if(dtFimS.after(dtInS) && dtInS.after(dtFimC) && dtFimC.after(dtIniC) && dtIniC.after(dtFimP) && dtFimP.after(dtIniP)){
            return true;
        }
        return false;
    }
    public void novoAnuncio(Colaborador c, Tarefa tarefa,Date dtIniP, Date dtFimP, Date dtIniC, Date dtFimC, Date dtInS, Date stFimS, TipoRegimento regT){
        String anunId = geraAnuncioId();
        Anuncio anu = new Anuncio(c,tarefa,anunId, dtIniP,  dtFimP,  dtIniC,  dtFimC,  dtInS,  stFimS,  regT);
        
    }
    
    private String geraAnuncioId() {
        return String.valueOf(10000 + (this.listaAnuncio.size()));
    }

    public boolean validaAnuncio(Anuncio anuncio) {
        boolean bRet = true;
        if (!this.listaAnuncio.isEmpty()) {
            for (Anuncio an : listaAnuncio) {
                if (anuncio.equals(an)) {
                    return false;
                }
                if (an.getTarefa().hasRefUnica(anuncio.getTarefa().getRefUnica())) {
                    return false;
                }
            }
        }
        return bRet;
    }

    public boolean registaAnuncio(Anuncio anuncio) {
        if (this.validaAnuncio(anuncio)) {
            return addAnuncio(anuncio);
        }
        return false;
    }

    private boolean addAnuncio(Anuncio anuncio) {
        return listaAnuncio.add(anuncio);
    }

    public Anuncio getAnuncioById(String anuncioId) {
        for (Anuncio an : this.listaAnuncio) {
            if (an.hasIdentificador(anuncioId)) {
                return an;
            }
        }
        return null;
    }

    
    public List<Anuncio> getAnunciosElegiveisDoFreelancer(Freelancer freel){
        List<Anuncio> lista = null;
        for(Anuncio anuncio : listaAnuncio){
            if(anuncio.aceitaCandidaturas()){
                boolean b = anuncio.eFreelancerElegivel(freel);
                if(b == true){
                    lista.add(anuncio);
                }
            }
        }
        return lista;
    }
    
    public List<Anuncio> getAnunciosPorAdjudicar(Colaborador colab, List<Adjudicacao> listAdju){
        List<Anuncio> listAnuncio = new ArrayList<>();
        for(Anuncio an : this.listaAnuncio){
            if(an.getTarefa().getColaborador().equals(colab)){
                boolean seriado = an.isSeriadio();
                if(seriado){
                    boolean adjudicar = true; //To do 
                    if(adjudicar){
                        boolean b = false;
                        for(Adjudicacao adju: listAdju){
                            if(adju.getAnuncioId().equals(an.getAnunId())){
                                b = true;
                            }
                        }
                        if(b){
                            listAnuncio.add(an);
                        }
                }
                }
            }
        }
        return listAnuncio;
    }
   
}
