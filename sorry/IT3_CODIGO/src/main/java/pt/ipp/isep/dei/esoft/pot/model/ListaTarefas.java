
package pt.ipp.isep.dei.esoft.pot.model;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListaTarefas {

    /**
     * Cria uma lista de tarefas
     */
    private final List<Tarefa> listaTarefas;

    /**
     * Instância uma lista de tarefas
     */
    public ListaTarefas() {
        listaTarefas = new ArrayList<>();
    }

    


    /**
     * Adiciona uma tarefa à lista de tarefa
     * @param colab
     * @param refUnica referência única da tarefa
     * @param desig desginação da tarefa
     * @param descInf descrição informal da tarefa
     * @param descTec descrição técnica da tarefa
     * @param duracao duração da tarefa
     * @param custo custo da tarefa
     * @param categ categoria de tarefa da tarefa
     * @return true se adicionar a tarefa, false se não adicionar
     */
    public boolean novaTarefa(Colaborador colab, String refUnica, String desig, String descInf, String descTec, Integer duracao, Double custo, Categoria categ) {
        Tarefa tarefa = new Tarefa(colab, refUnica, desig, descInf, descTec, duracao, custo, categ);
        return this.listaTarefas.contains(tarefa)
                ? false
                : this.listaTarefas.add(tarefa);
    }
   

    /**
     * Devolve uma tarefa pela sua referência única
     * @param refUnica referência única da tarefa a ser devolvida
     * @return Tarefa com a respetiva referência única
     */
    public Tarefa getTarefaByRefUnica(String refUnica) {
        for (Tarefa tar : listaTarefas) {
            if (tar.getRefUnica().equalsIgnoreCase(refUnica)) {
                return tar;
            }
        }
        return null;
    }

//    /**
//     * Adiciona um anúncio a uma tarefa
//     * @param refUnica referência única da tarefa em que vai ser adicionada a lista
//     * @param anuncio anúncio que vai ser adicionado à tarefa
//     * @return true se adicionar o anúncio à tarefa, false se não adicionar
//     */
//    public boolean adicionarAnuncio(String refUnica, Anuncio anuncio) {
//        Tarefa tar = getTarefaByRefUnica(refUnica);
//        return tar.addAnuncio(anuncio);
//    }

    public boolean validaTarefa(Tarefa tarefa){
        for(Tarefa t : listaTarefas){
            if(t == tarefa){
                return false;
            }
        }
        return true;
    }
    
    public boolean addTarefa(Tarefa tarefa){
        return this.listaTarefas.add(tarefa);
    }
    
    public List<Tarefa> getTarefasParaPublicar(Colaborador c){
        List<Tarefa> tar = new ArrayList<>();
        for(Tarefa t : listaTarefas){
            if(t.getColaborador().equals(c)){
                tar.add(t);
            }
        }
        return tar;
    }
    
    public Tarefa getTarefaParaPublicarByRef(String ref, Colaborador c){
        Tarefa tt = null;
        List<Tarefa> tar = getTarefasParaPublicar(c);
        for(Tarefa t : tar){
            if(t.getRefUnica().equals(t)){
                tt = t;
            }
        }
        return tt;
    }

}
