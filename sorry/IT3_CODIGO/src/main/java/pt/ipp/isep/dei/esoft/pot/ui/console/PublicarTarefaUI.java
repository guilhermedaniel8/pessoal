/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.ui.console;

import java.util.Date;
import java.util.List;
import java.util.Scanner;
import pt.ipp.isep.dei.esoft.pot.controller.PublicarTarefaController;
import pt.ipp.isep.dei.esoft.pot.model.Tarefa;
import pt.ipp.isep.dei.esoft.pot.model.TipoRegimento;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

/**
 *
 * @author guilhermedaniel
 */
public class PublicarTarefaUI {
    
    private PublicarTarefaController m_controller;
    Scanner ler = new Scanner(System.in);
    
    public PublicarTarefaUI(){
        m_controller = new PublicarTarefaController();
    }
    
    public void run(){
        System.out.println("\nPublicar Tarefa:");
        
         if (introduzDados()) {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaAnuncio()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possível concluir o registo com sucesso.");
                }
            }
        } else{
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }
    
    public boolean introduzDados(){
        List<Tarefa> listaTarefa = m_controller.getTarefasParaPublicar();
        
        String refUnica = "";
        Tarefa tarefa = (Tarefa) Utils.apresentaESeleciona(listaTarefa, "Seleciona a tarefa, por si criada, que pretende publicar");
        if(tarefa != null){
            refUnica = tarefa.getRefUnica();
        }
        
        Date pTarIn = Utils.readDateFromConsole("Data inicial de publicação do anúncio na plataforma:");
        Date pTarFi = Utils.readDateFromConsole("Data final de publicação do anúncio na plataforma:");
        Date pAprIn = Utils.readDateFromConsole("Data inicial de apresentação de candidaturas:");
        Date pAprFi = Utils.readDateFromConsole("Data final de apresentação de candidaturas:");
        Date pSerIn = Utils.readDateFromConsole("Data inicial de atribuição da tarefa a um freelancer:");
        Date pSerFi = Utils.readDateFromConsole("Data final de atribuição da tarefa a um freelancer:");
        System.out.println("Descrição do tipo de regimento:");
        String desTr = ler.next();
        
        m_controller.novoAnuncio(pTarIn, pTarFi, pTarIn, pTarFi, pTarIn, pSerFi, refUnica);
        
        List<TipoRegimento> listaTipoRegimento = m_controller.getTiposRegimento();
        
        
        return m_controller.novoAnuncio(pTarIn, pTarFi, pTarIn, pTarFi, pTarIn, pSerFi, desTr);
        
    }
    private void apresentaDados() {
        System.out.println("\nTarefa:\n" + m_controller.toString());
    }
    
}
