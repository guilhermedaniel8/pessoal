package pt.ipp.isep.dei.esoft.pot.model;

public class Classificacao {

    private Candidatura candidatura;
    private Integer ordem;
    private String texto;

    public Classificacao(Candidatura candidatura, Integer ordem) {
        this.candidatura = candidatura;
        this.ordem = ordem;
    }

    public Classificacao(Candidatura candidatura, Integer ordem, String texto) {
        this.candidatura = candidatura;
        this.ordem = ordem;
        this.texto = texto;
    }

    /**
     * @return the candidatura
     */
    public Candidatura getCandidatura() {
        return candidatura;
    }

    /**
     * @return the ordem
     */
    public Integer getOrdem() {
        return ordem;
    }

    

}
