
package pt.ipp.isep.dei.esoft.pot.ui.console;

import java.util.Date;
import java.util.List;
import java.util.Scanner;
import pt.ipp.isep.dei.esoft.pot.controller.AtualizarCandidaturaController;
import pt.ipp.isep.dei.esoft.pot.model.Candidatura;
import pt.ipp.isep.dei.esoft.pot.model.Tarefa;
import pt.ipp.isep.dei.esoft.pot.model.TipoRegimento;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

public class AtualizarCandidaturaUI {
     
    private AtualizarCandidaturaController m_controller;
    Scanner ler = new Scanner(System.in);
    
    public AtualizarCandidaturaUI(){
        m_controller = new AtualizarCandidaturaController();
    }
    
    public void run(){
        System.out.println("\nAtualiza Candidatura:");
        
         if (introduzDados()) {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.atualizaCandidatura()) {
                    System.out.println("Atualização efetuada com sucesso.");
                } else {
                    System.out.println("Não foi possível concluir a atualização com sucesso.");
                }
            }
        } else{
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }
    
    public boolean introduzDados(){
        List<Candidatura> listaCandidatura = m_controller.getCandidFree();
        
        Candidatura cand = (Candidatura) Utils.apresentaESeleciona(listaCandidatura, "Seleciona a candidatura, por si criada, que pretende atualizar");
        
        
        Double valorPrt = Utils.readDoubleFromConsole("Valor pretendido: ");
        Integer nrDias = Utils.readIntegerFromConsole("Número de dias: ");
        String txtApres = Utils.readLineFromConsole("Texto de apresentação: ");
        String txtMotiv = Utils.readLineFromConsole("Texto de motivação: ");
        
        return m_controller.atualizaDados(valorPrt, nrDias, txtApres, txtMotiv);
        
    }
    private void apresentaDados() {
        System.out.println("\nDados atualizados:\n" + m_controller.toString());
    }
}
