
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class CompetenciaTecnica  {
     
    private String codigo;
    
    private String descBreve;
    
    private String descDetalhada;
    
    private AreaAtividade areaAtividade;
    
    private List<GrauProficiencia> listaGrauProficiencia;
    
    public CompetenciaTecnica(String cod, String dsBreve, String dsDet, AreaAtividade areaAtividade) {
        if ( (cod == null) || (dsBreve == null) || (dsDet == null) || (areaAtividade == null) ||(cod.isEmpty()) || (dsBreve.isEmpty()) || (dsDet.isEmpty()))
        throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
            this.codigo = cod; 
            this.descBreve = dsBreve;
            this.descDetalhada = dsDet;
            this.areaAtividade = areaAtividade;
            this.listaGrauProficiencia = new ArrayList<>();      
    } 
    
    public CompetenciaTecnica(CompetenciaTecnica comp) {
        this.codigo = comp.codigo;
        this.descBreve = comp.descBreve;
        this.descDetalhada = comp.descDetalhada;
        this.listaGrauProficiencia = comp.listaGrauProficiencia;
    }
    
    public boolean addListaGrauProficiencia(GrauProficiencia gp){ 
    if(this.listaGrauProficiencia.contains(gp) && (this.listaGrauProficiencia.isEmpty())){
        return false;  
    }   
    this.listaGrauProficiencia.add(gp);
        return true;    
    }
     
    public String getCodigo() {
        return codigo;
    }

    public String getDescBreve() {
        return descBreve;
    }

    public String getDescDetalhada() {
        return descDetalhada;
    }

    public AreaAtividade getAreaAtividade() {
        return new AreaAtividade(areaAtividade);
    }
    
    public List<GrauProficiencia> getListaGrauProficiencia(){
        return listaGrauProficiencia;
    }
    
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setDescBreve(String descBreve) {
        this.descBreve = descBreve;
    }

    public void setDescDetalhada(String descDetalhada) {
        this.descDetalhada = descDetalhada;
    }

    public void setAreaAtividade(AreaAtividade areaAtividade) {
        this.areaAtividade = areaAtividade;
    }
    
    public boolean hasCodigo(String codigo) {
        return this.codigo.equalsIgnoreCase(codigo);
    }
    
    
    public List<GrauProficiencia> getListaCTeGPObriga(){
        return this.listaGrauProficiencia;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.codigo);
        return hash;
    }
    
      @Override
        public boolean equals(Object outroObjeto) {
            if (this == outroObjeto) {
                return true;
            }
            if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
                return false;
            }
            CompetenciaTecnica outraComp = (CompetenciaTecnica) outroObjeto;
            
            if (listaGrauProficiencia.size() == outraComp.listaGrauProficiencia.size()) {
               for (int i = 0; i < this.listaGrauProficiencia.size(); i++) {
                if(!this.listaGrauProficiencia.get(i).equals(outraComp.listaGrauProficiencia.get(i))){
                    return false;
                }
               }
            }
            return this.codigo.equalsIgnoreCase(outraComp.codigo) && this.descBreve.equalsIgnoreCase(outraComp.descBreve) && this.descDetalhada.equalsIgnoreCase(outraComp.descDetalhada) && this.areaAtividade.equals(outraComp.areaAtividade);
        }
    
    @Override
    public String toString() {
        Collections.sort(listaGrauProficiencia);
        return String.format("%s - %s - %s - Grau Proficiência: %s ", this.codigo
                , this.descBreve
                , this.descDetalhada
                , this.listaGrauProficiencia);
    }

}
