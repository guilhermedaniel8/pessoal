/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author soniasousa
 */
public class RegistoOrganizacao {
    
    private Plataforma m_oPlataforma;
    
    private List<Organizacao> listaOrganizacao = new ArrayList<Organizacao>();
    
    public Organizacao getOrganizacaoByEmailUtilizador(String emailUtlz){
//        for(Organizacao org : listaOrganizacao){
//            if(org.getColaboradores().get(listaOrganizacao.indexOf(org)).getEmail().equals(emailUtlz)){
//                return org;
//            }
//        }
        return null;
    }
      
    public Colaborador getColabByEmail(String emailColab){
//        for(Organizacao org : listaOrganizacao){
//            
//            for(Colaborador colab : org.getColaboradores())
//                if(emailColab.equals(colab.getEmail()))
//                    return colab;
//            }
        return null;
    }
    
    public Organizacao novaOrganizacao(String strNome, String strNIF, String strWebsite,String strTelefone, String strEmail, EnderecoPostal oMorada, Colaborador oGestor)
    {
        return new Organizacao(strNome,strNIF, strWebsite, strTelefone, strEmail, oMorada, oGestor);
    }

    public boolean registaOrganizacao(Organizacao oOrganizacao, String strPwd)
    {
        if (this.validaOrganizacao(oOrganizacao))
        {
            Colaborador oGestor = oOrganizacao.getGestor();
            String strNomeGestor = oGestor.getNome();
            String strEmailGestor = oGestor.getEmail();
            if (this.m_oPlataforma.getAutorizacaoFacade().registaUtilizadorComPapeis(strNomeGestor,strEmailGestor, strPwd, 
                    new String[] {Constantes.PAPEL_GESTOR_ORGANIZACAO,Constantes.PAPEL_COLABORADOR_ORGANIZACAO}))
                return addOrganizacao(oOrganizacao);
        }
        return false;
    }

    private boolean addOrganizacao(Organizacao oOrganizacao)
    {
        return this.listaOrganizacao.add(oOrganizacao);
    }
    
    public boolean validaOrganizacao(Organizacao oOrganizacao)
    {
        for(Organizacao o : this.listaOrganizacao){
            if(o ==  oOrganizacao){
                return false;
            }
        }
        return true;
    }
    
    public Organizacao procurarOrganizacao(String emailGestor){
        for (Organizacao o : this.listaOrganizacao) {
             if(o.getGestor().getEmail().equalsIgnoreCase(emailGestor)){
                 return o;
             }
        }
        return null;
    }
    
    
}