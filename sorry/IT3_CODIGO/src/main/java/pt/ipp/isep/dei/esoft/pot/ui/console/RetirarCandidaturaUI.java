
package pt.ipp.isep.dei.esoft.pot.ui.console;

import java.util.List;
import java.util.Scanner;
import pt.ipp.isep.dei.esoft.pot.controller.AtualizarCandidaturaController;
import pt.ipp.isep.dei.esoft.pot.controller.RetirarCandidaturaController;
import pt.ipp.isep.dei.esoft.pot.model.Candidatura;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

public class RetirarCandidaturaUI {
    
     
    private RetirarCandidaturaController m_controller;
    Scanner ler = new Scanner(System.in);
    
    public RetirarCandidaturaUI(){
        m_controller = new RetirarCandidaturaController();
    }
    
    public void run(){
        System.out.println("\nRemover Candidatura:");
        
         if (introduzDados()) {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.removerCandidatura()) {
                    System.out.println("Remoção efetuada com sucesso.");
                } else {
                    System.out.println("Não foi possível concluir a remoção com sucesso.");
                }
            }
        } else{
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }
    
    public boolean introduzDados(){
        List<Candidatura> listaCandidatura = m_controller.getCandidFree();
        
        Candidatura cand = (Candidatura) Utils.apresentaESeleciona(listaCandidatura, "Seleciona a candidatura, por si criada, que pretende remover");
        return m_controller.removerCandidatura();
        
    }
    private void apresentaDados() {
        System.out.println("\nDados atualizados:\n" + m_controller.toString());
    }
}
