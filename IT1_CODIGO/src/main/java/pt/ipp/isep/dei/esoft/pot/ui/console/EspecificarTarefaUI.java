
package pt.ipp.isep.dei.esoft.pot.ui.console;


import java.util.List;
import pt.ipp.isep.dei.esoft.pot.controller.EspecificarTarefaController;
import pt.ipp.isep.dei.esoft.pot.model.CategoriaDeTarefa;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

public class EspecificarTarefaUI {

    private EspecificarTarefaController m_controller;

    public EspecificarTarefaUI() {
        m_controller = new EspecificarTarefaController();
    }

    public void run() {
        System.out.println("\nEspecificar Tarefa:");

        if (introduzDados()) {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaTarefa()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possível concluir o registo com sucesso.");
                }
            }
        } else{
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }

    private boolean introduzDados() {
        String strRef = Utils.readLineFromConsole("Referência Única: ");
        String strDesignacao = Utils.readLineFromConsole("Designação: ");
        String strInformal = Utils.readLineFromConsole("Descrição informal: ");
        String strTecnica = Utils.readLineFromConsole("Descrição técnica: ");
        Double dblDuracao = Utils.readDoubleFromConsole("Duração: ");
        Double dblCusto = Utils.readDoubleFromConsole("Custo: ");

        List<CategoriaDeTarefa> listaCategorias = m_controller.getCategoriasTarefa();

        String catId = "";
        CategoriaDeTarefa categ = (CategoriaDeTarefa) Utils.apresentaESeleciona(listaCategorias, "Seleciona a Categoria de tarefa que é referente a esta Tarefa:");
        if (categ != null) {
            catId = categ.getIdentificador();
        }

        return m_controller.novaTarefa(strRef, strDesignacao, strInformal, strTecnica, dblDuracao, dblCusto, catId);
    }

    private void apresentaDados() {
        System.out.println("\nTarefa:\n" + m_controller.getTarefaString());
    }

}

