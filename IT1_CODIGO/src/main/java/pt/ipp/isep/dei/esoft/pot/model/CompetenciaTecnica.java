
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Objects;

public class CompetenciaTecnica {
     
    private String codigo;
    
    private String descBreve;
    
    private String descDetalhada;
    
    private AreaAtividade areaAtividade;
    
    public CompetenciaTecnica(String cod, String dsBreve, String dsDet, AreaAtividade areaAtividade) {
        if ( (cod == null) || (dsBreve == null) || (dsDet == null) || (areaAtividade == null) ||(cod.isEmpty()) || (dsBreve.isEmpty()) || (dsDet.isEmpty()))
        throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
            this.codigo = cod;
            this.descBreve = dsBreve;
            this.descDetalhada = dsDet;
            this.areaAtividade = areaAtividade;
            
    } 
    
    public CompetenciaTecnica(CompetenciaTecnica comp) {
        this.codigo = comp.codigo;
        this.descBreve = comp.descBreve;
        this.descDetalhada = comp.descDetalhada;
    }
    
    public String getCodigo() {
        return codigo;
    }

    public String getDescBreve() {
        return descBreve;
    }

    public String getDescDetalhada() {
        return descDetalhada;
    }

    public AreaAtividade getAreaAtividade() {
        return new AreaAtividade(areaAtividade);
    }
    
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setDescBreve(String descBreve) {
        this.descBreve = descBreve;
    }

    public void setDescDetalhada(String descDetalhada) {
        this.descDetalhada = descDetalhada;
    }

    public void setAreaAtividade(AreaAtividade areaAtividade) {
        this.areaAtividade = areaAtividade;
    }
    
    public boolean hasCodigo(String codigo) {
        return this.codigo.equalsIgnoreCase(codigo);
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.codigo);
        return hash;
    }
    
      @Override
        public boolean equals(Object outroObjeto) {
            if (this == outroObjeto) {
                return true;
            }
            if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
                return false;
            }
            CompetenciaTecnica outraComp = (CompetenciaTecnica) outroObjeto;
            return this.codigo.equalsIgnoreCase(outraComp.codigo) && this.descBreve.equalsIgnoreCase(outraComp.descBreve) && this.descDetalhada.equalsIgnoreCase(outraComp.descDetalhada) && this.areaAtividade.equals(outraComp.areaAtividade);
        }
    
    @Override
    public String toString() {
        return String.format("%s - %s - %s  - Área Atividade: %s", this.codigo, this.descBreve, this.descDetalhada, this.areaAtividade.toString());
    }
}
