package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;

public class Plataforma {

    private String m_strDesignacao;
    private final AutorizacaoFacade m_oAutorizacao;
    private final Set<Organizacao> m_lstOrganizacoes;
    private final Set<CompetenciaTecnica> m_lstCompetencias;
    private final Set<AreaAtividade> listaDeAreaAtividade;
    private final Set<CompetenciaTecnica> listaDeCompetenciasTecnicas;
    private final Set<CategoriaDeTarefa> listaCategoriasTarefa;
    private final Set<TipoCompetencia> listaTipoCompetencia;
    private final Set<Tarefa> listaTarefas;

    public Plataforma(String strDesignacao) {
        if ((strDesignacao == null)
                || (strDesignacao.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.m_strDesignacao = strDesignacao;

        this.m_oAutorizacao = new AutorizacaoFacade();

        this.m_lstOrganizacoes = new HashSet<>();
        this.listaDeAreaAtividade = new HashSet<>();
        this.listaDeCompetenciasTecnicas = new HashSet<>();
        this.listaCategoriasTarefa = new HashSet<>();
        this.listaTipoCompetencia = new HashSet<>();
        this.listaTarefas = new HashSet<>();
        this.m_lstCompetencias = new HashSet<>();
    }

    public AutorizacaoFacade getAutorizacaoFacade() {
        return this.m_oAutorizacao;
    }
    
    

//-----ORGANIZAÇÕES----------------------------------
    
    // <editor-fold defaultstate="collapsed">
    
public Organizacao novaOrganizacao(String strNome, String strNIF, String strWebsite,String strTelefone, String strEmail, EnderecoPostal oMorada, Colaborador oGestor)
    {
        return new Organizacao(strNome,strNIF, strWebsite, strTelefone, strEmail, oMorada, oGestor);
    }

    public boolean registaOrganizacao(Organizacao oOrganizacao, String strPwd)
    {
        if (this.validaOrganizacao(oOrganizacao,strPwd))
        {
            Colaborador oGestor = oOrganizacao.getGestor();
            String strNomeGestor = oGestor.getNome();
            String strEmailGestor = oGestor.getEmail();
            if (this.m_oAutorizacao.registaUtilizadorComPapeis(strNomeGestor,strEmailGestor, strPwd, 
                    new String[] {Constantes.PAPEL_GESTOR_ORGANIZACAO,Constantes.PAPEL_COLABORADOR_ORGANIZACAO}))
                return addOrganizacao(oOrganizacao);
        }
        return false;
    }

    private boolean addOrganizacao(Organizacao oOrganizacao)
    {
        return m_lstOrganizacoes.add(oOrganizacao);
    }
    
    public boolean validaOrganizacao(Organizacao oOrganizacao,String strPwd)
    {
        boolean bRet = true;
        
        // Escrever aqui o código de validação
        if (this.m_oAutorizacao.existeUtilizador(oOrganizacao.getGestor().getEmail()))
            bRet = false;
        if (strPwd == null)
            bRet = false;
        if (strPwd.isEmpty())
            bRet = false;
        //
      
        return bRet;
    }
    
    public Organizacao procurarOrganizacao(String emailGestor){
        for (Organizacao o : m_lstOrganizacoes) {
             if(o.getGestor().getEmail().equalsIgnoreCase(emailGestor)){
                 return o;
             }
        }
        return null;
    }
    
    // </editor-fold>
  
    // Tipo Competencia
    
    // <editor-fold defaultstate="collapsed">
    
    public TipoCompetencia novoTipoCompetencia(CompetenciaTecnica competenciaId, boolean obrigatoriedade) {
        return new TipoCompetencia(competenciaId, obrigatoriedade);
    }
    
    public boolean registaTipoCompetencia(TipoCompetencia oTipoComp) {
        if (this.validaTipoCompetencia(oTipoComp)) {
            return addTipoCompetencia(oTipoComp);
        }
        return false;
    }

    private boolean addTipoCompetencia(TipoCompetencia oTipoComp) {
        return this.listaTipoCompetencia.add(oTipoComp);
    }
  
    public boolean validaTipoCompetencia(TipoCompetencia oTipoComp) {
        boolean bRet = true;
        if (!this.listaTipoCompetencia.isEmpty()) {
            for (TipoCompetencia tc : listaTipoCompetencia) {
                if (oTipoComp.equals(tc)) {
                bRet = false;
                }
                if (tc.hasCompetencia(oTipoComp.getCompetencia())) {
                bRet = false;    
                }
            }
        }
        return bRet;
    }
    
    // </editor-fold>
    
    
    // Categoria de Tarefa
    
    // <editor-fold defaultstate="collapsed">
    
    public CategoriaDeTarefa getCategoriaDeTarefaById(String categoriaDeTarefaById) {
        for(CategoriaDeTarefa ct : this.listaCategoriasTarefa) {
            if (ct.hasIdentificador(categoriaDeTarefaById)) {
                return ct;
            }
        }
        return null;
    }
    
    public CategoriaDeTarefa novaCategoriaDeTarefa(String identificador, String descricao, AreaAtividade areaAtividadeId) {
        return new CategoriaDeTarefa(identificador, descricao, areaAtividadeId);
    }

    public boolean registaCategoriaDeTarefa(CategoriaDeTarefa oCateTarefa) {
        if (this.validaCategoriaDeTarefa(oCateTarefa)) {
            return addCategoriaDeTarefa(oCateTarefa);
        }
        return false;
    }

    private boolean addCategoriaDeTarefa(CategoriaDeTarefa oCateTarefa) {
        return this.listaCategoriasTarefa.add(oCateTarefa);
    }
    
    public boolean validaCategoriaDeTarefa(CategoriaDeTarefa oCateTarefa) {
        boolean bRet = true;
        if (!this.listaCategoriasTarefa.isEmpty()) {
            for (CategoriaDeTarefa ct : listaCategoriasTarefa) {
                if (oCateTarefa.equals(ct)) {
                bRet = false;
                }
                if (ct.hasIdentificador(oCateTarefa.getIdentificador())) {
                bRet = false;    
                }
            }
        }
        return bRet;
    }

    // </editor-fold>
    
    // Competências Tecnicas
    
    // <editor-fold defaultstate="collapsed">
    
    public CompetenciaTecnica getCompetenciaTecnicaById(String CompetenciaTecnicaById) {
        for(CompetenciaTecnica oCompTecnica : this.listaDeCompetenciasTecnicas) {
            if (oCompTecnica.hasCodigo(CompetenciaTecnicaById)) {
                return oCompTecnica;
            }
        }
        return null;
    }

    public CompetenciaTecnica novaCompetenciaTecnica(String cod, String dsBreve, String dsDet, AreaAtividade areaAtividade) {
        return new CompetenciaTecnica(cod, dsBreve, dsDet, areaAtividade);
    }

    public boolean registaCompetenciaTecnica(CompetenciaTecnica oCompTecnica) {
        if (this.validaCompetenciaTecnica(oCompTecnica)) {
            return addCompetenciaTecnica(oCompTecnica);
        }
        return false;
    }

    private boolean addCompetenciaTecnica(CompetenciaTecnica oCompTecnica) {
        return listaDeCompetenciasTecnicas.add(oCompTecnica);
    }
    
    public boolean validaCompetenciaTecnica(CompetenciaTecnica oCompTecnica) {
        boolean bRet = true;
        if (!this.listaDeCompetenciasTecnicas.isEmpty()) {
            for (CompetenciaTecnica ct : listaDeCompetenciasTecnicas) {
                if (oCompTecnica.equals(ct)) {
                bRet = false;
                }
                if (ct.hasCodigo(oCompTecnica.getCodigo())) {
                bRet = false;    
                }
            }
        }
        return bRet;
    }
    
    public List<CompetenciaTecnica> getCompetenciasTecnicas(){
        List<CompetenciaTecnica> listaCompetenciasTecnicas = new ArrayList<>();
        listaCompetenciasTecnicas.addAll(this.listaDeCompetenciasTecnicas);
        return listaCompetenciasTecnicas;
    }
    // </editor-fold>
    
    // Areas de Atividade 
    // <editor-fold defaultstate="collapsed">
            
    public AreaAtividade getAreaAtividadeById(String AreaAtividadeById) {
        for(AreaAtividade area : this.listaDeAreaAtividade) {
            if (area.hasCodigo(AreaAtividadeById)) {
                return area;
            }
        }
        return null;
    }
    
    public AreaAtividade novaAreaAtividade(String strCodigo, String strDescricaoBreve, String strDescricaoDetalhada) {
        return new AreaAtividade(strCodigo, strDescricaoBreve,strDescricaoDetalhada);
    }

    public boolean registaAreaAtividade(AreaAtividade oArea) {
        if (this.validaAreaAtividade(oArea)) {
            return addAreaAtividade(oArea);
        }
        return false;
    }

    private boolean addAreaAtividade(AreaAtividade oArea) {
        return listaDeAreaAtividade.add(oArea);
    }
    
    public boolean validaAreaAtividade(AreaAtividade oArea) {
        boolean bRet = true;
        if (!this.listaDeAreaAtividade.isEmpty()) {
            for (AreaAtividade area : listaDeAreaAtividade) {
                if (oArea.equals(area)) {
                bRet = false;
                }
                if (area.hasCodigo(oArea.getCodigo())) {
                bRet = false;    
                }
            }
        }
        return bRet;
    }
    
    public List<AreaAtividade> getAreasAtividade(){
        List<AreaAtividade> listaAreasAtividade = new ArrayList<>();
        listaAreasAtividade.addAll(this.listaDeAreaAtividade);
        return listaAreasAtividade;
    }
    
    // </editor-fold>

    //---------TAREFA------------------------------------
    
    // <editor-fold defaultstate="collapsed">
    public List<CategoriaDeTarefa> getCategoriasTarefa() {

        List<CategoriaDeTarefa> listaCateg = new ArrayList<>();
        listaCateg.addAll(listaCategoriasTarefa);
        return listaCateg;
    }
    
    public CategoriaDeTarefa getCategoriaByID(String catId) {
        for (CategoriaDeTarefa oCategoria : this.listaCategoriasTarefa) {
            if (oCategoria.hasIdentificador(catId)) {
                return oCategoria;
            }
        }

        return null;
    }

    public Tarefa novaTarefa(String refUnica, String designacao, String dInformal, String dTecnica, Double duracao, Double custo, CategoriaDeTarefa categ) {
        return new Tarefa(refUnica, designacao, dInformal, dTecnica, duracao, custo, categ);
    }

    public boolean validaTarefa(Tarefa tarefa) {
        boolean bRet = true;
        if(!this.listaTarefas.isEmpty()) {
            for(Tarefa tf : listaTarefas){
                if(tarefa.equals(tf)){
                    return false;
                }
                if(tf.hasRefUnica(tarefa.getRefUnica())){
                    return false;
                }
            }
        }
        return bRet;
    }

    public boolean registaTarefa(Tarefa tarefa){
        if (this.validaTarefa(tarefa)) {
            return addTarefa(tarefa);
        }
        return false;
    }
    
    private boolean addTarefa(Tarefa tarefa){
        return listaTarefas.add(tarefa);
    }
    // </editor-fold>
}
    
    
