
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
public class CategoriaDeTarefa {
    
    private String identificador;
    
    private String descricao;
    
    private AreaAtividade areaAtividade;
    
    private List<TipoCompetencia> competenciaPorTipo;
    
    
    public CategoriaDeTarefa(String identificador, String descricao, AreaAtividade areaAtividade) {
        if ( (identificador == null) || (descricao == null) || (areaAtividade == null) || (identificador.isEmpty()) || (descricao.isEmpty()) )
        throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
            this.identificador = identificador;
            this.descricao = descricao;
            this.areaAtividade = areaAtividade;
            this.competenciaPorTipo = new ArrayList<>();
    } 

    public String getIdentificador() {
        return identificador;
    }

    public String getDescricao() {
        return descricao;
    }

    public AreaAtividade getAreaAtividade() {
        return new AreaAtividade(areaAtividade);
    }
    
    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setAreaAtividade(AreaAtividade areaAtividade) {
        this.areaAtividade = new AreaAtividade(areaAtividade);
    }
    
    public boolean addCompetenciaPorTipo(CompetenciaTecnica competencia, boolean obrigatoriedade){ 
    TipoCompetencia tp = new TipoCompetencia(competencia, obrigatoriedade); 
    if(this.competenciaPorTipo.contains(tp)){
        return false;  
    }
        return this.competenciaPorTipo.add(tp);    
    }
    
    public boolean hasIdentificador(String identificador) {
        return this.identificador.equalsIgnoreCase(identificador);
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.identificador);
        return hash;
    }
    
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
            }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
            }
        CategoriaDeTarefa cate = (CategoriaDeTarefa) outroObjeto;
        return this.identificador.equalsIgnoreCase(cate.identificador) && this.descricao.equalsIgnoreCase(cate.descricao) && this.areaAtividade.equals(cate.areaAtividade);
    }
  
    @Override
    public String toString() {
        return String.format("%s - %s - Área Atividade: %s - %s", this.identificador, this.descricao, this.areaAtividade.toString(), this.competenciaPorTipo);
    }
}
