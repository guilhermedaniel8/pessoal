package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Objects;

public class CaraterCT {

    private boolean obrigatoria;

    private CompetenciaTecnica competencia;

    private GrauProficiencia grauMinimo;

    public CaraterCT(CompetenciaTecnica competencia, boolean obrigatoria, GrauProficiencia grauMinimo) {
        this.obrigatoria = obrigatoria;
        this.competencia = new CompetenciaTecnica(competencia);
        this.grauMinimo = new GrauProficiencia(grauMinimo);
    }

    public CaraterCT(CompetenciaTecnica competencia, GrauProficiencia grauMinimo) {
        this.competencia = new CompetenciaTecnica(competencia);
        this.grauMinimo = new GrauProficiencia(grauMinimo);
    }

    public boolean isObrigatoria() {
        return obrigatoria;
    }

    public CompetenciaTecnica getCompetencia() {
        return competencia;
    }

    public GrauProficiencia getGrauMinimo() {
        return grauMinimo;
    }

    public void setObrigatoriedade(boolean obrigatoriedade) {
        this.obrigatoria = obrigatoriedade;
    }

    public void setCompetencia(CompetenciaTecnica competencia) {
        this.competencia = competencia;
    }

    public boolean hasCompetencia(CompetenciaTecnica competencia) {
        return this.competencia.equals(competencia);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.competencia);
        return hash;
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }
        CaraterCT outroTipo = (CaraterCT) outroObjeto;
        return this.obrigatoria == outroTipo.obrigatoria && this.competencia.equals(outroTipo.competencia) && this.grauMinimo.equals(outroTipo.grauMinimo);
    }

    @Override
    public String toString() {
        if (obrigatoria == true) {
            return String.format("Competência Tecnica é Obrigatória: %s , posuindo o grau mínimo para concorrer de %s", this.competencia.toString(), this.grauMinimo.toString());
        }
        return String.format("Competência Tecnica é Desejável: %s , posuindo o grau mínimo para concorrer de %s", this.competencia.toString(), this.grauMinimo.toString());
    }
}
