/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.ui.console;

import java.util.List;
import pt.ipp.isep.dei.esoft.pot.controller.AdjudicarAnuncioController;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;


public class AdjudicarAnuncioUI {
    
    private AdjudicarAnuncioController m_controller;
    
    
    public AdjudicarAnuncioUI()
    {
        m_controller = new AdjudicarAnuncioController();
    }
    
    public void run(){
        if(introduzDados())
        {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.adjudicarAnuncio()) {
                    System.out.println("Adjudicação realizada com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir a adjudicação com sucesso.");
                }
            }
        }
        else
        {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }
    

    public boolean introduzDados(){
        List<Anuncio> listaAnuncio = m_controller.getListaAnunciosPorAdjudicar();
        
        Anuncio an = (Anuncio) Utils.apresentaESeleciona(listaAnuncio, "Selecione o anuncio para adjudicar.");
        return m_controller.novaAdjudicacao(an.getAnunId());
    }
    
    public String apresentaDados(){
        return ("\nDados da adjudicação:\n" + m_controller.toString());
    }
}
