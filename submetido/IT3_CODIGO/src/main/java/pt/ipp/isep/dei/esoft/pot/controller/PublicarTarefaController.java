/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.Colaborador;

import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.ListaTarefas;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.RegistoAnuncio;
import pt.ipp.isep.dei.esoft.pot.model.RegistoOrganizacao;
import pt.ipp.isep.dei.esoft.pot.model.RegistoTarefa;
import pt.ipp.isep.dei.esoft.pot.model.RegistoTipoRegimento;
import pt.ipp.isep.dei.esoft.pot.model.Tarefa;
import pt.ipp.isep.dei.esoft.pot.model.TipoRegimento;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;



/**
 *
 * @author guilhermedaniel
 */
public class PublicarTarefaController {
    
    private Plataforma m_oPlataforma;
    private Anuncio anuncio;
    private ListaTarefas listaTarefas;
    
    public PublicarTarefaController() {
        if (!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_COLABORADOR_ORGANIZACAO)) {
            throw new IllegalStateException("Colaborador não Autorizado");
        }
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }
    
    private RegistoTarefa rt = this.m_oPlataforma.getRegistoTarefa();
    private RegistoOrganizacao ro = this.m_oPlataforma.getRegistoOrganizacao();
    
    
    
    private String email = AplicacaoPOT.getInstance().getSessaoAtual().getEmailUtilizador();
    Colaborador c;
    
    public List<Tarefa> getTarefasParaPublicar(){
        Organizacao org = ro.getOrganizacaoByEmailUtilizador(email);
        c= org.getColaboradorByEmail(email);
        return listaTarefas.getTarefasParaPublicar(c);
    }
    Tarefa tarefa;
    
    public void selecionaTarefa(String ref){
        tarefa = listaTarefas.getTarefaParaPublicarByRef(ref, c);
    }
    
    
    private RegistoAnuncio regA = this.m_oPlataforma.getRegistoAnuncio();
    
    //fish
    private final RegistoTipoRegimento rreg = this.m_oPlataforma.getRegistoTipoRegimento();
    
    public List<TipoRegimento> getTiposRegimento(){
        return this.rreg.getTR();
    }
    
    public boolean novoAnuncio(Date dtIniP, Date dtFimP, Date dtIniC, Date dtFimC, Date dtInS, Date stFimS, String desTR){
        
        regA.validaPeriodos(dtIniP, dtFimP, dtIniC, dtFimC, dtInS, stFimS);
        TipoRegimento regT = rreg.getTipoRegimentoByDesc(desTR);
        Anuncio a = new Anuncio(c,tarefa, dtIniP, dtFimP, dtIniC, dtFimC, dtInS, stFimS, regT);
        if(a!= null){
            return true;
        }
        return false;
    }
    
    
    
    
    
    public boolean registaAnuncio(){
        return this.regA.registaAnuncio(anuncio);
    }
    
    public String toString(){
        return this.anuncio.toString();
    }
    
    
    
    
    
    
    
    
}
