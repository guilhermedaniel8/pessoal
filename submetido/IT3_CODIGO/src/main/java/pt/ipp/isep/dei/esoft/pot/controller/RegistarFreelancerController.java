
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.pot.model.CaraterCT;
import pt.ipp.isep.dei.esoft.pot.model.CompetenciaTecnica;
import pt.ipp.isep.dei.esoft.pot.model.EnderecoPostal;
import pt.ipp.isep.dei.esoft.pot.model.Freelancer;
import pt.ipp.isep.dei.esoft.pot.model.GrauProficiencia;
import pt.ipp.isep.dei.esoft.pot.model.HabilitacaoAcademica;
import pt.ipp.isep.dei.esoft.pot.model.Reconhecimento;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

public class RegistarFreelancerController {
    
    private AplicacaoPOT m_oApp;
    private Plataforma m_oPlataforma;
    private Organizacao m_oOrganizacao;
    private String m_strPwd;
    private Freelancer m_oFreelancer;
    
    private CaraterCT m_oTipo;

    public RegistarFreelancerController() {
    this.m_oApp = AplicacaoPOT.getInstance();
    this.m_oPlataforma = m_oApp.getPlataforma();
    }
    
    public List<CompetenciaTecnica> getCompetenciaTecnica() {
        return this.m_oPlataforma.getRegistoCompetenciaTecnica().getCompetenciasTecnicas();
    }
    
    
    public boolean novoFreelancer(String nome, String NIF, String contacto, String email, String local, String codPostal, String localidade) {
        try
        {   
            EnderecoPostal oMorada = Freelancer.novoEnderecoPostal(local, codPostal, localidade);     
            this.m_oFreelancer = this.m_oPlataforma.getRegistoFreelancer().novoFreelancer(nome, NIF, contacto, email, oMorada);   
            return this.m_oPlataforma.getRegistoFreelancer().validaFreelancer(m_oFreelancer);
        }
        catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oFreelancer = null;
            return false;
        }
    }
    
    
    public boolean registaFreelancer()
    {
        return this.m_oPlataforma.getRegistoFreelancer().registaFreelancer(m_oFreelancer);
    }

    public String getFreelancerString() {
        return this.m_oFreelancer.toString();
    }
 
    public boolean addHabilitacaoAcademica(String grau, String desigCurso, String instituicao, Double media) {
        HabilitacaoAcademica habilitacaoAcademica = new HabilitacaoAcademica(grau,desigCurso,instituicao,media);
        return this.m_oFreelancer.addHabilitacaoAcademica(habilitacaoAcademica);
    }
    
    public boolean addReconhecimento(String ctcod, GrauProficiencia gp, Date data) {
        CompetenciaTecnica ct = m_oPlataforma.getRegistoCompetenciaTecnica().getCompetenciaTecnicaById(ctcod);        
        Reconhecimento reconhecimento = new Reconhecimento(ct,gp,data);
        return this.m_oFreelancer.addReconhecimento(reconhecimento);
    }
    
}