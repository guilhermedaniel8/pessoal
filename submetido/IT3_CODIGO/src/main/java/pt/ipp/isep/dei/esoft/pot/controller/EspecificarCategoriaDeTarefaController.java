

package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.pot.model.AreaAtividade;
import pt.ipp.isep.dei.esoft.pot.model.Categoria;
import pt.ipp.isep.dei.esoft.pot.model.CompetenciaTecnica;
import pt.ipp.isep.dei.esoft.pot.model.GrauProficiencia;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.CaraterCT;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

public class EspecificarCategoriaDeTarefaController {
    
    private Plataforma m_oPlataforma;
    private Categoria m_oCategoria;
    private CaraterCT m_oCaraterCT;
    private AreaAtividade m_oAreaAtividade;

    public EspecificarCategoriaDeTarefaController() {
        if (!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_ADMINISTRATIVO)) {
            throw new IllegalStateException("Utilizador não Autorizado");
        }
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }

    public List<AreaAtividade> getAreasAtividade() {
        return this.m_oPlataforma.getRegistoAreaAtividade().getAreasAtividade();
    }
    
    public List<CompetenciaTecnica> getlistaCTcomAA() {
        return this.m_oPlataforma.getRegistoCompetenciaTecnica().listaCTcomAA(this.m_oCategoria.getAreaAtividade());
    }
    
    
    public List<CompetenciaTecnica> getCompetenciaTecnica() {
        return this.m_oPlataforma.getRegistoCompetenciaTecnica().getCompetenciasTecnicas();
    }

    public boolean novoCaraterCT(String competenciaId, boolean obrigatoriedade, GrauProficiencia grauMinimo) {
        try {           
            CompetenciaTecnica competencia = this.m_oPlataforma.getRegistoCompetenciaTecnica().getCompetenciaTecnicaById(competenciaId);
            competencia.setAreaAtividade(m_oAreaAtividade);
            this.m_oCaraterCT = this.m_oPlataforma.getRegistoCategoriaTarefa().novoCaraterCT(competencia, obrigatoriedade, grauMinimo);
            return this.m_oCategoria.validaCaraterCT(m_oCaraterCT) ;
        } catch (RuntimeException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oCaraterCT = null;
            return false;
        }
    }
 
    public boolean addCaraterCT(CompetenciaTecnica competenciaTecnica, boolean obrigatoriedade, GrauProficiencia grauMinimo) {
        return this.m_oCategoria.addCaraterCT(competenciaTecnica, obrigatoriedade, grauMinimo);
    }
    
    public boolean registaCategoriaDeTarefa() {
        return this.m_oPlataforma.getRegistoCategoriaTarefa().registaCategoriaDeTarefa(m_oCategoria);
    }
    
    public String getCategoriaDeTarefaString() {
        return this.m_oCategoria.toString();
    }

    public boolean novaCategoriaDeTarefa(String descricao) {
        try {
            
            this.m_oCategoria = this.m_oPlataforma.getRegistoCategoriaTarefa().novaCategoriaDeTarefa(descricao);
            return this.m_oPlataforma.getRegistoCategoriaTarefa().validaCategoriaDeTarefa(this.m_oCategoria);
        } catch (RuntimeException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oCategoria = null;
            return false;
        }
    }

    public void setAT(String areaAtividadeId) {
        AreaAtividade areaAtividade = this.m_oPlataforma.getRegistoAreaAtividade().getAreaAtividadeById(areaAtividadeId);
        this.m_oAreaAtividade = areaAtividade;
        this.m_oCategoria.setAreaAtividade(areaAtividade);
    }
}