
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.List;

public class ListaColaboradores {
    private List<Colaborador> listaColab;

    public Colaborador novoColaborador(String nome, String funcao, String tlf, String email) {
        Colaborador colab = new Colaborador(nome, funcao, tlf, email);
        return colab;
    }

    public boolean validaColaborador(Colaborador colab) {
        if (listaColab != null) {
            return true;
        }
        return false;
    }

    public boolean registaColaborador(Colaborador colab) {
        if (validaColaborador(colab)) {
            String nome = colab.getNome();
            String email = colab.getEmail();
            listaColab.add(colab);
            return true;
        }
        return false;
    }
    
    public boolean adicionarColaborador(Colaborador colab){
        return listaColab.add(colab);
    }
    
    public Colaborador getColaboradorByEmail(String email){
        for(Colaborador c : listaColab){
            if(c.getEmail().equals(email)){
                return c;
            }
        }
        return null;
    }
    
    public List<Colaborador> getColaboradores(){
        return listaColab;
    }

    Object get(int indexOf) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
