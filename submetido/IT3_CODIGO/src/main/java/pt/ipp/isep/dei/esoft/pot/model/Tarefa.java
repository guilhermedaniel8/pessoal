
package pt.ipp.isep.dei.esoft.pot.model;

public class Tarefa {
    private Colaborador colab;
    private String refUnica;
    private String designacao;
    private String dInformal;
    private String dTecnica;
    private Integer duracao;
    private Double custo; 
    private Categoria categ;
    private Organizacao org;
    private String emailUtilizador;
    private Anuncio anuncio;
    
    
    public Tarefa(Colaborador colab, String refUnica, String designacao, String dInformal, String dTecnica, Integer duracao, Double custo, Categoria categ){ //CategoriaDeTarefa categ
        if((refUnica == null) || (designacao == null) || (dInformal == null) || (dTecnica == null) || (duracao == null) || (custo == null) || (categ == null) ||
                (refUnica.isEmpty()) || (dInformal.isEmpty()) || (dTecnica.isEmpty()) || (custo.isNaN()))
                throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.colab = colab;
        this.refUnica = refUnica;
        this.designacao = designacao;
        this.dInformal = dInformal;
        this.dTecnica = dTecnica;
        this.duracao = duracao;
        this.custo = custo;
        this.categ = categ;
        
    }
    
    public Tarefa(Colaborador colab, String refUnica, String designacao, String dInformal, String dTecnica, Integer duracao, Double custo, Categoria categ, Organizacao org, String emailUtilizador){ //CategoriaDeTarefa categ
        if((refUnica == null) || (designacao == null) || (dInformal == null) || (dTecnica == null) || (duracao == null) || (custo == null) || (categ == null) || (org ==null) || (emailUtilizador == null) ||
                (refUnica.isEmpty()) || (dInformal.isEmpty()) || (dTecnica.isEmpty()) || (custo.isNaN()) || (emailUtilizador.isEmpty()))
                throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.colab = colab;
        this.refUnica = refUnica;
        this.designacao = designacao;
        this.dInformal = dInformal;
        this.dTecnica = dTecnica;
        this.duracao = duracao;
        this.custo = custo;
        this.categ = categ;
        this.org = org;
        this.emailUtilizador = emailUtilizador;
    }

    public Colaborador getColaborador(){
        return colab;
    }
    public String getRefUnica() {
        return refUnica;
    }
    
    public String getDesignacao() {
        return designacao;
    }

    public String getdInformal() {
        return dInformal;
    }

    public String getdTecnica() {
        return dTecnica;
    }

    public Integer getDuracao() {
        return duracao;
    }

    public Double getCusto() {
        return custo;
    }

    public Categoria getCateg() {
        return categ;
    }
    
    public Organizacao getOrg() {
        return org;
    }
    
    public String getEmailCriadorTarefa(){
        return emailUtilizador;
    }
    
    public void setRefUnica(String refUnica) {
        this.refUnica = refUnica;
    }
    
    public void setDesignacao(String designacao){
        this.designacao = designacao;
    }

    public void setdInformal(String dInformal) {
        this.dInformal = dInformal;
    }

    public void setdTecnica(String dTecnica) {
        this.dTecnica = dTecnica;
    }

    public void setDuracao(Integer duracao) {
        this.duracao = duracao;
    }

    public void setCusto(Double custo) {
        this.custo = custo;
    }
    

    public boolean hasRefUnica(String refUnica){
        return this.refUnica.equalsIgnoreCase(refUnica);
    }
    
    public String toString(){
        String str = String.format("%s - %s - %s - %s - %s - %s -%s", this.refUnica, this.designacao, this.dInformal, this.dTecnica, this.duracao, this.custo, this.categ.toString());
        return str;
    }
    
    public boolean equals(Object outroObjeto){
        if(this == outroObjeto){
            return true;
        }
        if(outroObjeto == null || this.getClass() != outroObjeto.getClass()){
            return false;
        }
        Tarefa tarefa = (Tarefa) outroObjeto;
        return this.refUnica.equalsIgnoreCase(tarefa.refUnica) &&
                this.designacao.equalsIgnoreCase(tarefa.designacao) &&
                this.dInformal.equalsIgnoreCase(tarefa.dInformal) &&
                this.dTecnica.equalsIgnoreCase(tarefa.dTecnica) &&
                this.duracao.equals(tarefa.duracao) &&
                this.custo.equals(tarefa.custo) &&
                this.categ.equals(tarefa.categ) &&
                this.org.equals(tarefa.org);
                
    }
    
    public void publicar(Anuncio anu){
        this.anuncio = anu;
    }
    
}
