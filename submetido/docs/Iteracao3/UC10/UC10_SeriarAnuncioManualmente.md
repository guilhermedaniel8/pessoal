# UC 10 - Seriar Manualmente Candidaturas de Anúncio

## 1. Engenharia de Requisitos


### Formato Breve

O colaborador de organização inicia o processo manual de seriação dos candidatos à realização de um anúncio. O sistema mostra os anúncios publicados pelo colaborador em fase de seriação não automática e que ainda não foram seriadas e pede-lhe para escolher um. O colaborador seleciona um anúncio. O sistema mostra as candidaturas existentes e solicita a introdução da sua classificação e justificação. O colaborador introduz a classificação das candidaturas e a justificação da mesma. O sistema mostra os colaboradores da organização e pede para selecionar os outros participantes no processo. O colaborador seleciona. O sistema solicita a introdução do texto de conclusão do processo como um todo. O colaborador introduz o dado solicitado. O sistema valida e apresenta os dados, pedindo que os confirme. O colaborador confirma. O sistema regista os dados juntamente com a data/hora atual e informa o colaborador do sucesso da operação. Após a seriação é verificado se o processo de atribuição é obrigatório, se sim é feita uma adjudicação manual do anuncio, sendo atribuido a tarefa ao freelancer respetivo.

![UC10-SSD.scg](UC10_SSD.svg)


### Formato Completo

#### Ator principal

* Colaborador de Organização

#### Partes interessadas e seus interesses

* **Colaborador de Organização:** pretende seriar as candidaturas que um anúncio recebeu.
* **Freelancer:** pretende conhecer a classificação das suas candidaturas à realização de determinados anúncios publicados na plataforma.
* **Organização:** pretende contratar pessoas externas (outsourcing) para a realização de determinadas tarefas e com competências técnicas apropriadas.
* **T4J:** pretende satisfazer as organizações e os freelancer facilitando a contratação de freelancers pelas organizações e vice-versa.


#### Pré-condições

* Existir pelo menos um anúncio de tarefa em condições de ser seriado manualmente pelo colaborador ativo no sistema.


#### Pós-condições
* A informação do processo de seriação é registada no sistema.
* A tarefa fica adjudicada e é registada no sistema.


#### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador inicia o processo manual de seriação das candidaturas a um anúncio.
2. O sistema mostra os anúncios publicadas pelo colaborador em fase de seriação não automática e que ainda não foram seriados e pede-lhe para escolher um.
3. O colaborador seleciona um anúncio.
4. O sistema mostra as candidaturas que o anúncio selecionado recebeu e que ainda estejam por classificar e solicita a escolha de uma dessas candidaturas.
5. O colaborador seleciona uma candidatura.
6. O sistema solicita a introdução da classificação da candidatura selecionada e a sua respetiva justificação.
7. O colaborador indica a classificação e a justificação de suporte da mesma.
8. Os passos 4 a 7 repetem-se até que estejam classificadas e com a devida justificação todas as candidaturas.
9. O sistema mostra a lista dos outros colaboradores da mesma organização (não selecionado) e solicita a seleção de um colaborador participante no processo de seriação.
10. O colaborador seleciona um colaborador.
11. Os passos 9 e 10 repetem-se até que estejam selecionados todos os outros colaboradores participantes no processo de seriação.
12. O sistema solicita introdução do texto de conclusão do processo como um todo.
13. O colaborador introduz o texto de conclusão.
14. O sistema valida e apresenta os dados, pedindo ao colaborador para confirmar.
15. O colaborador confirma.
16. O sistema regista todos os dados do processo de seriação juntamente com a data/hora atual.
17. Após os dados da seriação serem registados é verificado se o processo de atribuição é obrigatório, se sim é feita uma adjudicação manual do anuncio, sendo atribuido a tarefa ao freelancer respetivo.
18. A adjudicação é registada no sistema.
19. O sistema informa o colaborador do sucesso da operação.

#### Extensões (ou fluxos alternativos)

*a. O colaborador solicita o cancelamento do processo de seriação das candidaturas.
> O caso de uso termina.

4a. O sistema deteta que não existem candidaturas efetuadas ao anúncio selecionado.
>   1. O sistema alerta o colaborador que não existem candidaturas efetuadas ao anúncio selecionado.
>
        > 1a. O caso de uso termina.

16a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 2, 7 e 13)
>
	> 2a. O colaborador não altera os dados. O caso de uso termina.

16b. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
>   1. O sistema alerta o colaborador para o facto. 
>   2. O sistema permite a sua alteração (passo 7 e 13).
> 
	> 2a. O colaborador não altera os dados. O caso de uso termina.

17a. Após os dados da seriação serem registados é verificado se o processo de atribuição é obrigatório, se sim é feita uma adjudicação manual do anuncio.
>   1. O sistema verifica se o processo de atribuição é opcional.
>
        > 1a. O processo de adjudicação é feito no caso de uso 14. O caso de uso termina.

#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados

\-

#### Frequência de Ocorrência

\-

#### Questões em aberto

* O mesmo lugar/classificação pode ser atribuído a mais do que uma candidatura (e.g. em caso de empate)?
* O processo de seriação pode ser concluído havendo candidaturas por classificar?
* Há algum motivo que possa levar à desclassificação de uma candidatura?
* Qual o formato do texto de justificação de suporte a classificação das candidaturas efetuadas?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC10-MD.scg](UC10_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional


| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O colaborador inicia o processo manual de seriação das candidaturas a um anúncio.  |  ...interage com o utilizador?	 |  SeriarAnuncioUI.  |  Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existente no Modelo de Domínio.  |
|    |  ...coordena o UC?  |  SeriarAnuncioController  |  Controller.  |
|    |  ... cria a instância de ProcessoSeriacao?  |  Anuncio  |  IE: No MD, 1 anúncio espoleta zero ou mais ProcessoSeriacao.  |
| 2. O sistema mostra os anúncios publicadas pelo colaborador em fase de seriação não automática e que ainda não foram seriados e pede-lhe para escolher um.  |  ...conhece os anúncios?  |  RegistoAnuncio  |  HC + LC: O RegistoAnuncio possui/agrega  todos os anúncios elegíveis  |
| 3. O colaborador seleciona um anúncio.  |	   |    |    | 
| 4. O sistema mostra as candidaturas que o anúncio selecionado recebeu e que ainda estejam por classificar e solicita a escolha de uma dessas candidaturas. |  ... conhece as candidaturas?  |  Anuncio  |  IE: no MD o Anúncio possui Candidaturas.  |
|   |    |  ListaCandidaturas  |  IE: no MD o Anúncio possui Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas.  |
| 5. O colaborador seleciona uma candidatura.  |    |    |    |
| 6. O sistema solicita a introdução da classificação da candidatura selecionada e a sua respetiva justificação.  |    |    |    |
| 7. O colaborador indica a classificação e a justificação de suporte da mesma.  |  ... guarda os dados introduzidos?  |  ProcessoSeriacao  |  No MD é o ProcessoSeriacao que contém a classificacao. |
| 8. Os passos 4 a 7 repetem-se até que estejam classificadas e com a devida justificação todas as candidaturas.  |    |    |    |
| 9. O sistema mostra a lista dos outros colaboradores da mesma organização (não selecionado) e solicita a seleção de um colaborador participante no processo de seriação.  |  ... conhece os colaboradores?  |  ListaColaboradores  |  IE: no MD a Organizacao possui Colaboradores. Por aplicação de HC+LC delega a ListaColaboradores.  |
| 10. O colaborador seleciona um colaborador.  |    |    |    |
| 11. Os passos 9 e 10 repetem-se até que estejam selecionados todos os outros colaboradores participantes no processo de seriação.  |    |    |    |
| 12. O sistema solicita introdução do texto de conclusão do processo como um todo.  |    |    |    |
| 13. O colaborador introduz o texto de conclusão.  |  ... guarda os dados introduzidos?  |  ProcessoSeriacao  |  IE: No MD o ProcessoSeriacao guarda os dados do texto de conclusao.  |
| 14. O sistema valida e apresenta os dados, pedindo ao colaborador para confirmar.  |  ...valida os dados do ProcessoSeriacao(validacao local)?  |  ProcessoSeriacao  |  IE: Possui os seus própios dados.  |
|    |  …valida os dados do ProcessoSeriação (validação global)?  |  Anuncio  |  IE: Anuncio possui/agrega ProcessoSeriacao  |
|    |  …guarda o ProcessoSeriacao criado  |  Anuncio  | IE: Anuncio possui/agrega ProcessoSeriacao
| 15. O colaborador confirma.  |    |    |    |
| 16. O sistema regista todos os dados do processo de seriação juntamente com a data/hora atual.  |  ...guarda a Classificacao criada?  |  ProcessoSeriacao  |  IE: O processoSeriacao contém/agrega Classificacao.  |
| 17. Após os dados da seriação serem registados é verificado se o processo de atribuição é obrigatório, se sim é feita uma adjudicação manual do anuncio, sendo atribuido a tarefa ao freelancer respetivo.  |  …valida os dados do ProcessoSeriação (validação global)?  |  RegistoAdjudicacoes  |   IE: RegistoAdjudicacoes possui/agrega Adjudicacao   |
| 18. A adjudicação é registada no sistema.  |  ...guarda a Adjudicacao criada? |  RegistoAdjudicacoes  |  IE: RegistoAdjudicacoes possui/agrega Adjudicacao  |
| 19. O sistema informa o colaborador do sucesso da operação.  |    |  SeriarAnuncioUI  |    |


### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Anuncio
 * Candidatura
 * ProcessoSeriacao
 * TipoRegimento
 * Classificacao
 * Organizacao
 * Colaborador
 * Adjudicacao

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * SeriarAnuncioUI  
 * SeriarAnuncioController
 * RegistoAnuncios
 * RegistoOrganizacoes
 * ListaColaboradores
 * ListaCandidaturas
 * RegistoAdjudicacoes


###	Diagrama de Sequência

![UC10_SD.svg](UC10_SD.svg)

![UC10_SD2.svg](UC10_SD2.svg)

###	Diagrama de Classes

![UC10_CD.svg](UC10_CD.svg)
