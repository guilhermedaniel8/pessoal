# Diagrama de Casos de Uso

![Diagrama de Casos de Uso](DUC.svg)

**Para cada caso de uso deve ser realizada a sua descrição (perspetiva de engenharia de requisitos) e a sua realização (perspetiva de design).**

# Casos de Uso
| UC  | Descrição                                                               |                   
|:----|:------------------------------------------------------------------------|
| UC1 | [Registar Organização](UC1/UC1_RegistarOrganizacao.md)   |
| UC2 | [Definir Área de Atividade](UC2/UC2_DefinirArea.md)  |
| UC3 | [Definir Categoria (de Tarefa)](UC3/UC3_DefinirCategoria.md)|
| UC4 | [Especificar Competência Técnica](UC4/UC4_EspecificarCT.md)|
| UC5 | [Especificar Colaborador de Organização](UC5/UC5_EspecificarColaborador.md) |
| UC6 | [Especificar Tarefa](UC6/UC6_EspecificarTarefa.md)|
| UC7 | [Registar Freelancer](UC7/UC7_RegistarFreelancer.md)|
| UC8 | [Publicar Tarefa](UC8/UC8_PublicarTarefa.md)|
| UC9 | [Efetuar Candidatura](UC9/UC9_EfetuarCandidatura.md)|
| UC10 | [Seriar Anuncio Manualmente](UC10/UC10_SeriarAnuncioManualmente.md)|
| UC11 | [Atualizar Candidatura](UC11/UC11_AtualizarCandidatura.md)|
| UC12 | [Retirar Candidatura](UC12/UC12_RetirarCandidatura.md)|
| UC13 | [Seriar Anúncio Automaticamente](UC13/UC13_SeriarAnuncioAutomaticamente.md)|
| UC14 | [Adjudicar Anúncio](UC14/UC14_AdjudicarAnuncio.md)|
