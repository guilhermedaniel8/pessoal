# UC 8 - Publicar Tarefa

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador de organização inicia a publicação de uma tarefa. O sistema apresenta as tarefas por si anteriormente criadas e pede para selecionar uma destas. O colaborador de organização seleciona a tarefa pretendida. O sistema solicita os dados necessários para criação de um anúncio referente à tarefa (i.e. período de publicação da tarefa na plataforma, período de apresentação de candidaturas pelos freelancers, período de seriação e decisão de atribuição da tarefa a um freelancer). O colaborador de organização introduz os dados solicitados. O sistema apresenta tipos de regimento. O colaborador de organização seleciona o tipo de pretendido. O sistema valida e apresenta os dados ao colaborador de organização, pedindo que os confirme. O colaborador de organização confirma. O sistema regista os dados e informa o colaborador de organização do sucesso da operação.


### SSD
![UC8-SSD](UC8_SSD.svg)


### Formato Completo

#### Ator principal

* Colaborador de Organização

#### Partes interessadas e seus interesses

* **Colaborador de Organização:** pretende publicar as tarefas.
* **Organização:** pretende que os seus colaboradores possam publicar tarefas para posterior seriação de freelancers.
* **Freelancer:** pretende candidatar-se ao anúncio que resulta da publicação da tarefa.
* **T4J:** pretende que os freelancers se candidatem às tarefas anunciadas.


#### Pré-condições

* Já devem estar definidos na plataforma 3 tipos de regimento aplicáveis: seriação subjetiva com atribuição opcional; seriação subjetiva com atribuição obrigatória; seriação e atribuição automática com base no segundo preço mais baixo.

#### Pós-condições
* O anúncio da tarefa é publicado.

#### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador de organização inicia a publicação de uma tarefa. 
2. O sistema apresenta as tarefas por si anteriormente criadas e pede para selecionar uma destas. 
3. O colaborador de organização seleciona a tarefa pretendida.
4. O sistema solicita os dados necessários para criação de um anúncio referente à tarefa (i.e. período de publicação da tarefa na plataforma, período de apresentação de candidaturas pelos freelancers, período de seriação e decisão de atribuição da tarefa a um freelancer). 
5. O colaborador de organização introduz os dados solicitados.
6. O sistema apresenta tipos de regimento.
7. O colaborador de organização seleciona o tipo regimento pretendido.
8. O sistema valida e apresenta os dados ao colaborador de organização, pedindo que os confirme.
9. O colaborador de organização confirma.
10. O sistema regista os dados e informa o colaborador de organização do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a.  O colaborador de organização solicita o cancelamento da publicação da tarefa.

> O caso de uso termina.

3a.  A tarefa pretendida não está definida no sistema.
>    1. O sistema informa o colaborador de organização de tal facto.
>    2. O sistema permite a criação de uma nova tarefa (UC 6).
>
        >2a. O colaborador de organização não altera os dados. O caso de uso termina.

7a. O tipo de regimento aplicável não está definido no sistema.
>    1. O sistema informa o colaborador de tal facto. O caso de uso termina.

8a. Dados mínimos obrigatórios em falta.
>    1. O sistema informa quais os dados em falta.
>    2. O sistema permite a introdução dos dados em falta (passo 5).
>
        >2a. O colaborador de organização não altera os dados. O caso de uso termina.

8b. O sistema deteta que os dados (ou algum subconjunto de dados) introduzidos são inválidos.
>    1. O sistema alerta o colaborador de organização para tal facto.
>    2. O sistema permite a sua alteração (passo 5).
>
        >2a. O colaborador de organização não altera os dados. O caso de uso termina.


#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados

\-

#### Frequência de Ocorrência

\-

#### Questões em aberto

* Existem outros dados que são necessários?
* Todos os dados são obrigatórios?
* Quando o tipo de regimento aplicável não existe, é necessário recolher alguma informação ou notificar alguém que isso aconteceu?
* Quem está encarregue de adicionar novos tipo de regimento?
* O colaborador de organização está habilitado a adicionar novos tipos de regimento?
* Qual a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC8-MD.svg](UC8_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O colaborador de Organização inicia a publicação de uma tarefa. 		 |	... interage com o utilizador?		 |   PublicarTarefaUI          |    Pure Fabrication: não se justifica atribuir esta responsabilidade a nenhuma classe existente no Modelo de Domínio.      |
|               |   ...coordena a UC?   | PublicarTarefaController  |  Controller  |
|               |   ...cria instância de anúncio? |  RegistoAnuncio |  As responsabilidades da plataforma passarão para o RegistoAnuncio (HC + LC) + Creator com HC + LC: Registo possui Anuncio.   |
|               |   ...conhece o RegistoAnuncio? | Plataforma | IE: a Plataforma contém/agrega RegistoAnuncio |
| 2. O sistema apresenta as tarefas por si anteriormente criadas e pede para selecionar uma destas 		 |	... conhece a lista de tarefas criadas pelo Colaborador de Organização?	 |   RegistoTarefa     |    HC + LC : o RegistoTarefa armazena a tarefa, a organização e o email no qual o utilizador registou a tarefa.         |
|       | ...conhece o RegistoTarefa  | Plataforma  |  IE: a Plataforma contém/agrega RegistoTarefa.  |
| 3. O colaborador de organização seleciona a tarefa pretendida.  		 |	... guarda a tarefa selecionada?		 |   Anuncio          |   IE: o Anuncio possui a tarefa a ser anunciada.   |
| 4. O sistema solicita os dados necessários para criação de um anúncio referente à tarefa (i.e. período de publicação da tarefa na plataforma, período de apresentação de candidaturas pelos freelancers, período de seriação e decisão de atribuição da tarefa a um freelancer).  		 |							 |             |                              |
| 5. O colaborador de organização introduz os dados solicitados. |   ...guarda os dados introduzidos?	 | Anuncio      |  IE: o Anuncio armazena os diferentes períodos.            |
| 6. O sistema apresenta tipos de regimento.  |	...conhece a lista de tipos de regimento?		 |   RegistoTipoRegimento          |  HC + LC: o RegistoTipoRegimento armazena todos os tipos de Regimento.     |         
|               |   ... conhece RegistoTipoRegimento? | Plataforma  |  IE: a Plataforma contém/agrega RegistoTipoRegimento.  |
| 7. O colaborador de organização seleciona o tipo regimento pretendido. | ...guarda o tipo de regimento pretendido?  |  Anuncio  |  IE: o Anuncio armazena o tipo de regimento aplicável.  |
| 8. O sistema valida e apresenta os dados ao colaborador de organização, pedindo que os confirme. | ... validade os dados do Anúncio(validação local)? | Anuncio  | IE: possui os seus próprios dados.  |
|       |   ...validade os dados do Anúncio (validação global)?  |  RegistoAnuncio  |  HC + LC: o RegistoAnuncio contém/agrega Anuncio.  |
| 9. O colaborador de organização confirma.|      |      |     |
| 10. O sistema regista os dados e informa o colaborador de organização do sucesso da operação. | ...guarda o Anuncio criado?  |  RegistoAnuncio  |  HC + LC: o RegistoAnuncio contém/agrega Anuncio.   |

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Organização
 * Plataforma
 * Tarefa
 * Anúncio
 * RegistoAnuncio
 * RegistoOrganizacao
 * RegistoTarefa
 * RegistoTR

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * PublicarTarefaUI  
 * PublicarTarefaController
 
 Outras classes de sistema/componentes externos:
 
 * SessaoUtilizador
 * AplicacaoPOT


###	Diagrama de Sequência

![UC8-SD.svg](UC8_SD.svg)


###	Diagrama de Classes

![UC8-CD.svg](UC8_CD.svg)
