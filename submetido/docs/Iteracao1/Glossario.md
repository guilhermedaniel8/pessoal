# Glossário


| **_Termo_**                   	| **_Descrição_**                                           |                                       
|:------------------------|:----------------------------------------------------------------|
| **Administrativo** | Pessoa responsável por realizar na plataforma várias atividades de suporte ao negócio. |
| **ADM** | Acrónimo para Administrativo. |
| **Área de atividade** | Ramo de negócio em que uma determinada tarefa se enquadra. |
| **AA** | Acrónimo para área de atividade. |
| **Categoria (de Tarefa)** | Corresponde a uma descrição usada para catalogar um ou mais tarefas (semelhantes), dentro da mesma área de atividade. |
| **Colaborador de Organização** | Pessoa individual que atua em representação de uma determinada organização, que, de entre outras responsabilidades, especifica tarefas para posterior publicação pela organização_ |
| **CT** | Acrónimo para competência técnica. |
| **GP** | Acrónimo para graus de proficiência. |
| **_Freelancer_** | Pessoa individual que trabalha por conta própria e não está necessariamente comprometida com uma entidade empregadora (organização) específica a longo prazo. |
| **Gestor de Organização** | Pessoa individual, considerado também colaborador da organização, que especifica na plataforma outros colaboradores da mesma (organização). |
| **Organização** | Pessoa coletiva que pretende contratar _freelancers_ para a realização de tarefas necessárias à atividade da mesma. |
| **Outsourcing** | Serviço a que uma organização recorre para que seja desenvolvida uma determinada tarefa por um entidade externa. |
| **Processo de Autenticação** | Meio através do qual se procede à verificação da identidade da pessoa que pretende/está a utilizar a plataforma informática. |
| **PWD** | Acrónimo para password. |
| **Utilizador** | Pessoa individual que interage com a aplicação informática. |
| **Utilizador Não Registado** | Utilizador que interage com a plataforma informática de forma anónima, i.e. sem ter realizado previamente o processo de autenticação previsto. |
| **Utilizador Registado** | Utilizador que interage com a plataforma informática após ter realizado o processo de autenticação previsto e, portanto, a aplicação conhece a sua identidade. Tipicamente, este assume o papel/função de Administrativo ou Gestor de Organização ou Colaborador de Organização ou _Freelancer_. |
| **Tarefa** | Função definida pela organização, para a posterior realização pelo _Freelancer_. |
| **Tasks for Joe (T4J)** | Pessoa coletica que permite e facilita o contacto entre o _Freelancer_ e uma determinada Organização que pretenda contratar o seu serviço. |











