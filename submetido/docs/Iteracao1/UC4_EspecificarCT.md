# UC4 - Especificar Competência Técnica

## 1. Engenharia de Requisitos

### Formato Breve

O administrativo inicia a especificação de uma nova competência técnica. O sistema solicita os dados necessários sobre a nova CT (i.e. código, descrição breve, descrição detalhada). O administrativo introduz os dados solicitados. O sistema apresenta as áreas de atividade e pede para selecionar uma destas. O administrativo seleciona a AA pretendida. O sistema valida e apresenta todos os dados, pedindo que os confirme. O administrativo confirma. O sistema regista os dados da nova CT e informa o administrativo do sucesso da operação.


### SSD

![UC4_SSD.svg](UC4_SSD.svg)

### Formato Completo

#### Ator principal

Administrativo

#### Partes interessadas e seus interesses

* **Administrativo:** pretende especificar competências técnicas, através de áreas de atividade ja definidas, para que possa posteriormente catalogar as categorias de tarefas.

#### Pré-condições
n/a

#### Pós-condições
A informação da competência técnica é registada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O administrativo inicia a especificação de uma nova competência técnica. 
2. O sistema solicita os dados necessários(i.e. código, descrição breve, descrição detalhada).
3. O administrativo introduz os dados solicitados.
4. O sistema apresenta as áreas de atividade e pede para selecionar uma.
5. O administrativo seleciona a área de atividade pretendida.
6. O sistema valida e apresenta todos os dados ao administrativo, pedindo que os confirme.
7. O administrativo confirma.
8. O sistema regista os dados e informa o administrativo do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O administrativo solicita o cancelamento da especificação da competência técnica.
> O caso de uso termina.

4a. O sistema deteta que não existem áreas de atividade definidas no sistema.
>   1. O sistema alerta o administrativo que não existem áreas de atividade definidas.
>   2. O sistema questiona o administrativo se deseja definir uma área de atividada, para posterior seleção.
>
    >   2a. O administrativo não define uma área de atividade. O caso de uso termina.

4b. O administrativo não seleciona uma das área de atividade apresentadas no sistema, porque não encontra a desejada.
>   1. O sistema alerta o administrativo que não foi selecionada uma área de atividade.
>   2. O sistema questiona o administrativo se deseja definir uma área de atividade nova, para posterior seleção.
>
    >   2a. O administrativo não define uma área de atividade nova. O caso de uso termina.

6a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3 e 5).
>
	>   2a. O administrativo não altera os dados. O caso de uso termina.

6b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o administrativo para o facto.
>	2. O sistema permite a sua alteração (passo 3).
>
	>   2a. O administrativo não altera os dados. O caso de uso termina.

6c. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o administrativo para o facto. 
> 2. O sistema permite a sua alteração (passo 3).
> 
	>   2a. O administrativo não altera os dados. O caso de uso termina.


#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados

\-

#### Frequência de Ocorrência

\-

#### Questões em aberto

* Existem outros dados que são necessários?
* Todos os dados são obrigatórios?
* O código único é sempre introduzido pelo administrativo ou o sistema deve gerá-lo automaticamente?
* Qual o formato das descrições?
* Quais os dados que em conjunto permitem detectar a duplicação de competências técnicas?
* Qual a frequência de ocorrência deste caso de uso?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC4_MD](UC4_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O administrativo inicia a especificação de uma nova competência técnica.  |  ... interage com o utilizador?	 |  EspecificarCompetenciaUI.           |  Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existente no Modelo de Domínio.  |
|    |  ... coordena o UC?  |  EspecificarCompetenciaController	  |  Controller.
|    |  ... cria/instancia Competencia?  |  Plataforma  |  Creator Regra 1  |
| 2. O sistema solicita os dados necessários (i.e. código, descrição breve, descrição detalhada). |    |    |    |
| 3. O administrativo introduz os dados solicitados.  |  ... guarda os dados introduzidos?  |  CompetenciaTecnica  |  Information Expert (IE) - instância criada no passo 1  |
| 4. O sistema apresenta as áreas de atividade e pede para selecionar uma.  |  ...conhece as áreas de atividade existentes a listar?  |  Plataforma  |  IE: A plataforma possui/agrega  todas as áreas de atividade  |
| 5. O administrativo seleciona a área de atividade pretendida.  |	... guarda a área de atividade selecionada?  |  CompetenciaTecnica  | IE: A CompetenciaTecnica possui/agrega a área de atividade selecionada  |
| 6. O sistema valida e apresenta todos os dados ao administrativo, pedindo que os confirme.  |	… valida os dados da Competencia (validação local)  |  CompetenciaTecnica   |    |
|    |  …valida os dados da Competencia (validação global)  |  Plataforma  |  IE: A Plataforma possui/agrega CompetenciaTecnica  |
| 7. O administrativo confirma.  |     |      |       |
| 8. O sistema regista os dados e informa o administrativo do sucesso da operação.  |  … guarda a CompetenciaTecnica criada?  |  Plataforma   |  IE: No MD a Plataforma possui CompetenciaTecnica  |


### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

  * Plataforma
  * CompetenciaTecnica

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * EspecificarCompetenciaUI  
 * EspecificarCompetenciaController


###	Diagrama de Sequência

![UC4_SD.svg](UC4_SD.svg)


###	Diagrama de Classes

![UC4_CD.svg](UC4_CD.svg)
