# UC5 - Especificar Colaborador de Organização

## 1. Engenharia de Requisitos

### Formato Breve

O gestor de organização especifica um novo colaborador de organização. O sistema solicita os dados necessários sobre o novo colaborador de organização(i.e. nome, função, contacto telefónico, endereço de email). O gestor da organização introduz os dados solicitados. O sistema valida os dados fornecidos e apresenta todos os dados, pedindo que os confirme. O gestor da organização confirma. O sistema regista os dados do novo colaborador e informa o gestor da organização do sucesso da operação.

### SSD
![UC5_SSD.svg](UC5_SSD.svg)

### Formato Completo

#### Ator principal

Gestor de organização

#### Partes interessadas e seus interesses

* **T4J:** Pretende que a plataforma tenha colaboradores registados para especificar tarefas.
* **Gestor de organização:** Pretende especificar colaboradores da sua organização.
* **Colaborador da organização:** Pretende usufruir das funcionalidades da plataforma.

#### Pré-condições

- Existe um gestor da organização registado no sistema.

#### Pós-condições

- A informação do colaborador da organização é registada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O gestor da organização inicia a especificação de um novo colaborador de organização.
2. O sistema solicita os dados necessários(i.e. nome,	
função,	contacto telefónico, endereço de email).
3. O gestor da organização introduz os dados solicitados.
4. O sistema valida e apresenta todos os dados ao gestor da organização, pedindo que os confirme.
5. O gestor da organização confirma os dados.
6. O sistema regista os dados e informa o gestor de organização do sucesso da operação.

#### Extensões (ou fluxos alternativos)

*a O gestor da organização cancela o registo.
>    O caso de uso termina. 

4a. Dados minimos obrigatorios em falta.
>   1. O sistema informa quais os dados em falta.
>   2. O utilizador introduz os dados em falta.
>
    >       a. O utilizador não fornece os dados (o caso de uso termina).

4b. Dados inseridos já existem no sistema.
>   1. O sistema informa do facto da existencia dos dados introduzidos no sistema.
>   2. O utilizador introduz dados diferentes.
>
    >       a. O utilizador não modifica os dados (o caso de uso termina).
    
#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados

\-

#### Frequência de Ocorrência 

\-

#### Questões em aberto

- Qual a frequencia deste caso de uso?
- Existem outros dados que são necessários?
- Todos os dados são obrigatórios?
- Como é feito o login do colaborador na plataforma?
- Quem define a PWD?
- Quais são as regras de segurança aplicaveis à palavra-passe?
- Quais os dados que em conjunto permitem detetar a duplicação de colaboradores?
- Para onde é enviada a PWD se esta for gerada automaticamente?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC5_MD.svg](UC5_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
|1. O utilizador não registado inicia o registo de um colaborador.|... interage com o utilizador?| RegistarColaboradorUI |Pure Fabrication|
| |... coordena o UC?| RegistarColaboradorController |Controller|
| |... cria instâncias de um Colaborador?|Organização|Creator(regra1)|
|2. O sistema solicita os dados necessários sobre o colaborador que o gestor está a proceder ao registo (i.e. nome, função, contacto telefónico, email e pwd).||||
|3. O Gestor introduz os dados solicitados. |... guarda os dados introduzidos?|Organizacao, Colaborador|IE: instância criada no passo 1|
| |... cria instâncias de Colaborador?|Organizacao|creator(regra1)|
|4. O sistema valida e apresenta os dados, pedindo que os confirme.|... valida os dados do colaborador? (validação local)|Colaborador|IE: possui os seus próprios dados|
||... valida os dados do Colaborador (validação global)|Organização|IE: A Organização tem registados Colaboradores.|
|5. O gestor confirma. ||||
|6. O sistema regista os dados do colaborador da organização, tornando este último um utilizador registado e informa o utilizador não registado do sucesso da operação.| ...guarda a informação do Colaborador Da Organização?|AutorizacaoFacade|IE. A gestão de utilizadores é responsabilidade do componente externo respetivo cujo ponto de interação é através da classe "AutorizacaoFacade"|           

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Organização
 * Colaborador

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * EspecificarColaboradorUI
 * EspecificarColaboradorController


###	Diagrama de Sequência

![UC5_SD.svg](UC5_SD.svg)


###	Diagrama de Classes

![UC5_CD.svg](UC5_CD.svg)

