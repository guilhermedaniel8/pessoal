/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.ipp.isep.dei.esoft.pot.model.Anuncio;

import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.RegistoAnuncio;
import pt.ipp.isep.dei.esoft.pot.model.RegistoOrganizacao;
import pt.ipp.isep.dei.esoft.pot.model.RegistoTarefa;
import pt.ipp.isep.dei.esoft.pot.model.RegistoTipoRegimento;
import pt.ipp.isep.dei.esoft.pot.model.Tarefa;
import pt.ipp.isep.dei.esoft.pot.model.TipoRegimento;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;



/**
 *
 * @author guilhermedaniel
 */
public class PublicarTarefaController {
    
    private Plataforma m_oPlataforma;
    private Anuncio anuncio;
    
    public PublicarTarefaController() {
        if (!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_COLABORADOR_ORGANIZACAO)) {
            throw new IllegalStateException("Colaborador não Autorizado");
        }
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }
    
    private RegistoTarefa rt = this.m_oPlataforma.getRegistoTarefa();
    private RegistoOrganizacao ro = this.m_oPlataforma.getRegistoOrganizacao();
    
    
    
    private String email = AplicacaoPOT.getInstance().getSessaoAtual().getEmailUtilizador();
    public List<Tarefa> getListaTarefasUtilizador(){
        
        return this.rt.getTarefasByEmailUtilizador(email);
    }
    
    
    private RegistoAnuncio ra = this.m_oPlataforma.getRegistoAnuncio();
    public boolean novoAnuncio(String refUnica){
        
        try{
            
            Organizacao org = this.ro.getOrganizacaoByEmailUtilizador(email);
            Tarefa tar = this.rt.getTarefa(refUnica, org);
            return this.ra.validaAnuncio(anuncio);
        }catch (RuntimeException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.anuncio = null;
            return false;
        }
        
    }
    
    public boolean addPeriodos(Date pTarIn, Date pTarFi, Date pAprIn, Date pAprFi, Date pSerIn, Date pSerFi){
        try{
            this.anuncio.setPer(pTarIn, pTarFi, pAprIn, pAprFi, pSerIn, pSerFi);
            
            
            return true;
        }catch (RuntimeException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.anuncio = null;
            return false;
        }
    }
    //fish
    private RegistoTipoRegimento rtr = this.m_oPlataforma.getRegistoTipoRegimento();
    
    public List<TipoRegimento> getTiposRegimento(){
        return this.rtr.getTR();
    }
    
    public boolean addTipoRegimento(String trId){
        try{
            TipoRegimento tr = this.rtr.getTrByID(trId);
            
            this.anuncio.setTR(tr);
            
            return true;
        }catch (RuntimeException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.anuncio = null;
            return false;
        }
    }
    
    
    
    public boolean registaAnuncio(){
        return this.ra.registaAnuncio(anuncio);
    }
    
    public String toString(){
        return this.anuncio.toString();
    }
    
    
    
    
    
    
    
}
