/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author guilhermedaniel
 */
public class RegistoTipoRegimento {
    
    private List<TipoRegimento> listaTipoRegimento = new ArrayList<TipoRegimento>();
    
    public List<TipoRegimento> getTR(){
        List<TipoRegimento> listaTiposRegimento = new ArrayList<>();
        listaTiposRegimento.addAll(this.listaTipoRegimento);
        return listaTiposRegimento;
    }
    
    public TipoRegimento getTrByID(String trId){
        for(TipoRegimento tr : this.listaTipoRegimento) {
            if (tr.hasIdentificador(trId)) {
                return tr;
            }
        }
        return null;
    }
    
}
