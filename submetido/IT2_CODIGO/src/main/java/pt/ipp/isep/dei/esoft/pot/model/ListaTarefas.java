package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;

public class ListaTarefas {

    private List<Tarefa> listaTarefas;

    public ListaTarefas() {
        listaTarefas = new ArrayList<>();
    }

    public Tarefa novaTarefa(String refUnica, String designacao, String dInformal, String dTecnica, Integer duracao, Double custo, Categoria categ) {

        return new Tarefa(refUnica, designacao, dInformal, dTecnica, duracao, custo, categ);
    }
    
    @Override
    public String toString() {
        List<Tarefa> copia = new ArrayList<>(listaTarefas);

        StringBuilder s = new StringBuilder();
        for (Tarefa tarefa : listaTarefas) {
            s.append(tarefa);
            s.append("\n");
        }
        return s.toString().trim();
    }

    public void eliminarTarefas() {
        listaTarefas.clear();
    }

    public boolean validaTarefa(Tarefa tarefa) {

        if (listaTarefas.contains(tarefa)) {
            return false;
        }
        return true;
    }


    public boolean addTarefa(Tarefa tarefa) {
        
        return listaTarefas.add(tarefa);
    }
    
    public boolean registaTarefa(Tarefa tarefa){
        if(validaTarefa(tarefa)){
            return addTarefa(tarefa);
        }
        return false;
    }

}
