
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Date;

public class Reconhecimento {
    
    private CompetenciaTecnica ct;
    
    private GrauProficiencia gp;
    
    private Date data;
    
    public Reconhecimento(CompetenciaTecnica ct, GrauProficiencia gp, Date data) {
        if ( (ct == null) || (gp == null) || (data == null) )
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.ct = new CompetenciaTecnica(ct);
        this.gp = new GrauProficiencia(gp);
        this.data = data;
    }
    
    public Reconhecimento(CompetenciaTecnica ct, GrauProficiencia gp){
        if ( (ct == null) || (gp == null))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        this.ct = new CompetenciaTecnica(ct);
        this.gp = new GrauProficiencia(gp);
    }
    public Reconhecimento(Reconhecimento rec) {
        this.ct = new CompetenciaTecnica(rec.ct);
        this.gp = new GrauProficiencia(rec.gp);;
        this.data = rec.data;
    }
    
    
    public CompetenciaTecnica getCompetenciaTecnica(){
        return this.ct;
    }
    
    public GrauProficiencia getGrauProficiencia(){
        return this.gp;
    }
    
    @Override
    public String toString()
    {
        return String.format("Reconhecimento: %s - %s - %s", this.ct, this.gp, this.data);
    }
    
}
