
package pt.ipp.isep.dei.esoft.pot.ui.console;

import java.util.List;
import pt.ipp.isep.dei.esoft.pot.controller.EfetuarCandidaturaController;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

public class EfetuarCandidaturaUI {
    
    private EfetuarCandidaturaController m_controller;
    
    public EfetuarCandidaturaUI(){
         System.out.println("\nEfetuar Candidatura:");
    }
     public void run(){
        System.out.println("\nEfetuar candidatura:");
        
         if (introduzDados()) {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaCandidatura()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possível concluir o registo com sucesso.");
                }
            }
        } else{
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }
    public boolean introduzDados(){
        List<Anuncio> listaAnuncio = m_controller.getAnElegiveis();
        
        String anuncioId = "";
        Anuncio anuncio = (Anuncio) Utils.apresentaESeleciona(listaAnuncio, "Seleciona a tarefa, por si criada, que pretende publicar");
        
        double valorReceber = Utils.readDoubleFromConsole("Valor a receber:");
        int diasRealizacao = Utils.readIntegerFromConsole("Número de dias necessário à realização:");
        String texto = Utils.readLineFromConsole("Texto de apresentação/ motivação:");
        
        return m_controller.addInfo(valorReceber, diasRealizacao, texto);
        
        
    }
    private void apresentaDados() {
        System.out.println("\nEfetuar Candidatura:\n" + m_controller.toString());
    }
    
    
}
