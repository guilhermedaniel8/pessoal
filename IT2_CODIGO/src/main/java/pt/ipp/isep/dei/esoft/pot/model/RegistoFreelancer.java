
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;


public class RegistoFreelancer implements AlgoritmoGeradorPasswords {

    private Plataforma m_oPlataforma;
    
    private List<Freelancer> listaFreelancer = new ArrayList<Freelancer>();
    
    public Freelancer getFreelancerByEmailUtilizador(String emailUtlz){
        for(Freelancer free : listaFreelancer){
            if(free.getEmail().equals(emailUtlz)){
                return free;
            }
        }
        return null;
    }
    
    public Freelancer novoFreelancer(String nome, String NIF, String contacto, String email, EnderecoPostal enderecoPostal)
    {
        return new Freelancer(nome,NIF,contacto,email,enderecoPostal);
    }

    public boolean registaFreelancer(Freelancer freelancer)
    {
        String pwd = geradorPassword(freelancer.getNome(),freelancer.getEmail());
        if (this.validaFreelancer(freelancer)) {
            if (this.m_oPlataforma.getAutorizacaoFacade().registaUtilizadorComPapel(freelancer.getNome(),freelancer.getEmail(), pwd, Constantes.PAPEL_FREELANCER)){
                return addFreelancer(freelancer);
            }
        }
        return false;
    }

    private boolean addFreelancer(Freelancer freelancer)
    {
        return this.listaFreelancer.add(freelancer);
    }
    
    public boolean validaFreelancer(Freelancer freelancer)
    {
       for(Freelancer free : this.listaFreelancer){
            if(free == freelancer){
                return false;
            }
        }
        return true;
    }
    
    public Freelancer procurarFreelancer(String emailFreelancer){
        for (Freelancer free : this.listaFreelancer) {
             if(free.getEmail().equalsIgnoreCase(emailFreelancer)){
                 return free;
             }
        }
        return null;
    }

    @Override
    public String geradorPassword(String nomeG,String emailG) {
        String pwd = "123456";
        return pwd;
    }
    
}