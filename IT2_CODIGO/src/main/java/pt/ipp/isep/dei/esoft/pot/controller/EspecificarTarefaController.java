
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.pot.model.Categoria;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Tarefa;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;


public class EspecificarTarefaController {
    
    private Plataforma m_oPlataforma;
    private Tarefa tarefa;

    
    public EspecificarTarefaController() {
        if (!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_COLABORADOR_ORGANIZACAO)) {
            throw new IllegalStateException("Colaborador não Autorizado");
        }
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }

    public List<Categoria> getCategoriasTarefa() {
        return this.m_oPlataforma.getRegistoCategoriaTarefa().getCategoriaTarefa();
    }

    public boolean novaTarefa(String refUnica, String designacao, String dInformal, String dTecnica, Double duracao, Double custo, String catId) {

        try {
            Categoria categ = this.m_oPlataforma.getRegistoCategoriaTarefa().getCategoriaTarefaById(catId);
            
            String emailUtilizador = AplicacaoPOT.getInstance().getSessaoAtual().getEmailUtilizador();
            Organizacao org = this.m_oPlataforma.getRegistoOrganizacao().getOrganizacaoByEmailUtilizador(emailUtilizador);
            
            this.tarefa = this.m_oPlataforma.getRegistoTarefa().novaTarefa(refUnica, designacao, dInformal, dTecnica, duracao, custo, categ, org, emailUtilizador);
            return this.m_oPlataforma.getRegistoTarefa().validaTarefa(this.tarefa);

        } catch (RuntimeException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.tarefa = null;
            return false;
        }
    }

    public boolean registaTarefa() {
        return this.m_oPlataforma.getRegistoTarefa().registaTarefa(this.tarefa);
    }
    
    public String getTarefaString(){
        return this.tarefa.toString();
    }

}