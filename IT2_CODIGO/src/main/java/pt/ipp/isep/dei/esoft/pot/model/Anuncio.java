/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;


import java.util.Date;
import java.util.List;
import pt.ipp.isep.dei.esoft.pot.model.AnuncioSeriado;

/**
 *
 * @author guilhermedaniel
 */
public class Anuncio {
    
    
    private Tarefa tarefa;
    private Date pTarIn;
    private Date pTarFi;
    private Date pAprIn;
    private Date pAprFi;
    private Date pSerIn;
    private Date pSerFi;
    private TipoRegimento tr;
    private List<Candidatura> listaCandidatura;
    private String identificador;
    private AnuncioSeriado anuncioSeriado;
    
    public Anuncio(Tarefa tarefa){
        if(tarefa == null)
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.tarefa = tarefa;
    }
    
    public Anuncio(Tarefa tarefa, Date pTarIn, Date pTarFi, Date pAprIn, Date pAprFi, Date pSerIn, Date pSerFi, TipoRegimento tr){ 
        if((tarefa == null) || (pTarIn == null) || (pTarFi == null) || (pAprIn == null) || (pAprFi == null) || (pSerIn == null) || (pSerFi == null) || (tr == null))
                throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.tarefa = tarefa;
        this.pTarIn = pTarIn;
        this.pTarFi = pTarFi;
        this.pAprIn = pAprIn;
        this.pAprFi = pAprFi;
        this.pSerIn = pSerIn;
        this.pSerFi = pSerFi;
        this.tr = tr;
        
    }
    
    public Tarefa getTarefa() {
        return tarefa;
    }

    public Date getpTarIn() {
        return pTarIn;
    }

    public Date getpTarFi() {
        return pTarFi;
    }

    public Date getpAprIn() {
        return pAprIn;
    }

    public Date getpAprFi() {
        return pAprFi;
    }

    public Date getpSerIn() {
        return pSerIn;
    }

    public Date getpSerFi() {
        return pSerFi;
    }
    
    public List<Candidatura> getCandidaturas(){
        return listaCandidatura;
    }

    public void setPer(Date pTarIn,Date pTarFi,Date pAprIn,Date pAprFi,Date pSerIn,Date pSerFi){
        this.pTarIn = pTarIn;
        this.pTarFi = pTarFi;
        this.pAprIn = pAprIn;
        this.pAprFi = pAprFi;
        this.pSerIn = pSerIn;
        this.pSerFi = pSerFi;
    }
    
    public void setTR(TipoRegimento tr){
        this.tr = tr;
    }
    
    public TipoRegimento getTR(){
        return this.tr;
    }
    
    public boolean equals(Object outroObjeto){
        if( this == outroObjeto){
            return true;
        }
        if(outroObjeto == null || this.getClass() != outroObjeto.getClass()){
            return false;
        }
        Anuncio anun = (Anuncio) outroObjeto;
        return this.tarefa.equals(anun.tarefa) && this.pTarIn.equals(anun.pTarIn) && this.pTarFi.equals(anun.pTarFi) &&
                this.pAprIn.equals(anun.pAprIn) && this.pAprFi.equals(anun.pAprFi) && this.pSerIn.equals(anun.pSerIn) &&
                this.pSerFi.equals(anun.pSerFi);
    }
    
    public boolean hasIdentificador(String identificador) {
        return this.identificador.equalsIgnoreCase(identificador);
    }
    
    private boolean validaCandidatura(Candidatura candidatura){
        boolean bRet = true;
        if (!this.listaCandidatura.isEmpty()) {
            for (Candidatura can : this.listaCandidatura) {
                if (listaCandidatura.equals(can)) {
                    bRet = false;
                }
            }
        }
        return bRet;
    }
    
    private boolean addCandidatura(Freelancer freelancer, Candidatura candidatura) {
        if (candidatura.getTexto() == null) {
            Candidatura a = new Candidatura(freelancer, candidatura.getValorReceber(), candidatura.getDiasRealizacao());
        }
        Candidatura a = new Candidatura(freelancer, candidatura.getValorReceber(), candidatura.getDiasRealizacao(), candidatura.getTexto());
        return this.listaCandidatura.add(a);
    }
    
    public String toString(){
        return String.format("Tarefa:%s - Período de Publicitação inicial e final:%s %s - Período de apresentação de candidaturas:%s %s - Período de seriação e atribuição:%s %s",tarefa.toString(),pAprIn,pAprFi,pTarIn,pTarFi,pSerIn,pSerFi);
    }
            
    public AnuncioSeriado novoAnuncioSeriado(){
        return new AnuncioSeriado();
    }

    
    public AnuncioSeriado getAnuncioSeriado() {
        return anuncioSeriado;
    }

    public void setAnuncioSeriado(AnuncioSeriado anuncioSeriado) {
        this.anuncioSeriado = anuncioSeriado;
    }
    
}
