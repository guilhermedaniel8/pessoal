
package pt.ipp.isep.dei.esoft.pot.ui.console;


import pt.ipp.isep.dei.esoft.pot.controller.EspecificarColaboradorController;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

public class EspecificarColaboradorUI {
    private EspecificarColaboradorController m_controller;

    public EspecificarColaboradorUI(){
        m_controller = new EspecificarColaboradorController();
    }
     public void run()
    {
        System.out.println("\nEspecificar colaborador:");

        if(introduzDados())
        {
            apresentaDados();
            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.criarColaborador()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        }
        else
        {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }
    private boolean introduzDados() {
        System.out.println("\nInformação do Colaborador:");  
        String srtNomeC = Utils.readLineFromConsole("Nome do Colaborador: ");
        String strFuncaoC = Utils.readLineFromConsole("Função Desempenhada: ");
        String strTelefoneC = Utils.readLineFromConsole("Telefone: ");
        String strEmailC = Utils.readLineFromConsole("Email: ");
        
        return m_controller.novoColaboradorOrganizacao(srtNomeC, strFuncaoC, strTelefoneC, strEmailC);
    }
    private void apresentaDados() 
    {
        System.out.println("\n Informação a Registar:\n" + m_controller.getColaboradorString());
    }
    
}

