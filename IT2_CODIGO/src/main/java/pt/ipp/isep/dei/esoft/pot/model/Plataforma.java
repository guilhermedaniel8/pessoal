package pt.ipp.isep.dei.esoft.pot.model;

import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;

public class Plataforma {

    private String m_strDesignacao;
    private final AutorizacaoFacade m_oAutorizacao;
    private final RegistoCompetenciaTecnica m_oRegistoCompetenciaTecnica;
    private final RegistoAreaAtividade m_oRegistoAreaAtividade;
    private final RegistoCategoriaTarefa m_oRegistoCategoriaTarefa;
    private final RegistoTarefa m_oRegistoTarefa;
    private final RegistoOrganizacao m_oOrganizacao;
    private final RegistoFreelancer m_oFreelancer;
    private final RegistoTipoRegimento m_oRegistoTipoRegimento;
    private final RegistoAnuncio m_oRegistoAnuncio;
    

    public Plataforma(String strDesignacao) {
        if ((strDesignacao == null)
                || (strDesignacao.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.m_strDesignacao = strDesignacao;

        this.m_oAutorizacao = new AutorizacaoFacade();
        
        this.m_oRegistoCompetenciaTecnica = new RegistoCompetenciaTecnica();
        
        this.m_oRegistoAreaAtividade = new RegistoAreaAtividade();
        
        this.m_oRegistoCategoriaTarefa = new RegistoCategoriaTarefa();
        
        this.m_oRegistoTarefa = new RegistoTarefa();
        
        this.m_oOrganizacao = new RegistoOrganizacao();
        
        this.m_oFreelancer = new RegistoFreelancer();
        
        this.m_oRegistoTipoRegimento = new RegistoTipoRegimento();
        
        this.m_oRegistoAnuncio = new RegistoAnuncio();
        
    }
    
    public RegistoAreaAtividade getRegistoAreaAtividade(){
        return this.m_oRegistoAreaAtividade;
    }

    public RegistoCompetenciaTecnica getRegistoCompetenciaTecnica(){
        return this.m_oRegistoCompetenciaTecnica;
    }
    
    public RegistoCategoriaTarefa getRegistoCategoriaTarefa(){
        return this.m_oRegistoCategoriaTarefa;
    }
    
    public RegistoTarefa getRegistoTarefa(){
        return this.m_oRegistoTarefa;
    }
    
    public RegistoOrganizacao getRegistoOrganizacao(){
        return this.m_oOrganizacao;
    }
    
    public RegistoFreelancer getRegistoFreelancer(){
        return this.m_oFreelancer;
    }
    
    public RegistoTipoRegimento getRegistoTipoRegimento(){
        return this.m_oRegistoTipoRegimento;
    }
    
    public RegistoAnuncio getRegistoAnuncio(){
        return this.m_oRegistoAnuncio;
    }
    
    public AutorizacaoFacade getAutorizacaoFacade() {
        return this.m_oAutorizacao;
    }
    
  
}
    
    
