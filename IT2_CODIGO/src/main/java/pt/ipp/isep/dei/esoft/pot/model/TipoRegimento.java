/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

/**
 *
 * @author guilhermedaniel
 */
public class TipoRegimento {
    
    private String identificador;
    private String tipoSeriacao;
    
    public String getIdentificador(){
        return this.identificador;
    }
    
    public String getTipoSeriacao(){
        return this.tipoSeriacao;
    }
    
    public void setIdentificador(String identificador){
        this.identificador = identificador;
    }
    
    public void setTipoSeriacao(String tipoSeriacao){
        this.tipoSeriacao = tipoSeriacao;
    }
    
    public boolean hasIdentificador(String identificador) {
        return this.identificador.equalsIgnoreCase(identificador);
    }
    
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
            }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
            }
        TipoRegimento tr = (TipoRegimento) outroObjeto;
        return this.identificador.equalsIgnoreCase(tr.identificador) && this.tipoSeriacao.equalsIgnoreCase(tr.tipoSeriacao);
    }
    public String toString(){
        return String.format("Identificador:%s; Tipo de seriação:%s", this.identificador, this.tipoSeriacao);
    }  
    
}
