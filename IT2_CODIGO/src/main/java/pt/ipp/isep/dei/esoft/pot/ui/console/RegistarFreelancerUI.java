
package pt.ipp.isep.dei.esoft.pot.ui.console;

import java.util.Date;
import java.util.List;
import pt.ipp.isep.dei.esoft.pot.controller.RegistarFreelancerController;
import pt.ipp.isep.dei.esoft.pot.model.CompetenciaTecnica;
import pt.ipp.isep.dei.esoft.pot.model.GrauProficiencia;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

public class RegistarFreelancerUI {
    
    private RegistarFreelancerController m_controller;

    public RegistarFreelancerUI() {
        m_controller = new RegistarFreelancerController();
    }

    public void run() {
        boolean resposta;
        System.out.println("\n Registar Freelancer:");

        if (introduzDados()) {
            do {
                if (introduzDados2()){
                    resposta = Utils.confirma("Deseja Introduzir mais Habilitações Académicas? (S/N)");
                }else{
                    System.out.println("Ocorreu um erro. Operação cancelada.");
                    break;
                }                
            } while (resposta == true);
            
            do {
                if (introduzDados3()){
                    resposta = Utils.confirma("Deseja Introduzir mais Reconhecimentos? (S/N)");
                }else{
                    System.out.println("Ocorreu um erro. Operação cancelada.");
                    break;
                }     
            } while (resposta == true);

            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaFreelancer()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        } else {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }

    }

    private boolean introduzDados() {
        
        String nome = Utils.readLineFromConsole("Nome: ");
        String NIF = Utils.readLineFromConsole("NIF: ");
        String contacto = Utils.readLineFromConsole("Contacto: ");
        String email = Utils.readLineFromConsole("Email: "); 
        String local = Utils.readLineFromConsole("Local: "); 
        String codPostal = Utils.readLineFromConsole("Codigo Postal: ");
        String localidade = Utils.readLineFromConsole("Localidade: ");

        return m_controller.novoFreelancer(nome, NIF, contacto, email, local, codPostal, localidade);
    }

    private boolean introduzDados2() {
        
        String grau = Utils.readLineFromConsole("Grau: ");
        String desigCurso = Utils.readLineFromConsole("Designação do Curso: ");
        String instituicao = Utils.readLineFromConsole("Instituição: ");
        Double media = Utils.readDoubleFromConsole("Média: ");
        
        return m_controller.addHabilitacaoAcademica(grau, desigCurso, instituicao, media);
    }
    
    private boolean introduzDados3() {
        List<CompetenciaTecnica> listaCompetenciasTecnicas = m_controller.getCompetenciaTecnica();
        
        String competenciaTecnicaId = "";
        CompetenciaTecnica competencia = (CompetenciaTecnica) Utils.apresentaESeleciona(listaCompetenciasTecnicas , "Seleciona a competência técnica que o Freelancer possui:");
        if (competencia != null) {
            competenciaTecnicaId = competencia.getCodigo();
        }
        
        GrauProficiencia grau = (GrauProficiencia) Utils.apresentaESeleciona(competencia.getListaGrauProficiencia() , "Seleciona o grau que o Freelancer possui:");
        
        Date data = Utils.readDateFromConsole("Data: ");
        
        return m_controller.addReconhecimento(competenciaTecnicaId, grau, data);
    }
    
    private void apresentaDados() {
        System.out.println("\nCategoria de Tarefa:\n" + m_controller.getFreelancerString());
    }


    
}
