/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
/**
 *
 * @author guilhermedaniel
 */
public class RegistoAnuncio {

    private List<Anuncio> listaAnuncio = new ArrayList<Anuncio>();

    public Anuncio novoAnuncio(Tarefa tar) {
        return new Anuncio(tar);
    }

    public boolean validaAnuncio(Anuncio anuncio) {
        boolean bRet = true;
        if (!this.listaAnuncio.isEmpty()) {
            for (Anuncio an : listaAnuncio) {
                if (anuncio.equals(an)) {
                    return false;
                }
                if (an.getTarefa().hasRefUnica(anuncio.getTarefa().getRefUnica())) {
                    return false;
                }
            }
        }
        return bRet;
    }

    public boolean registaAnuncio(Anuncio anuncio) {
        if (this.validaAnuncio(anuncio)) {
            return addAnuncio(anuncio);
        }
        return false;
    }

    private boolean addAnuncio(Anuncio anuncio) {
        return listaAnuncio.add(anuncio);
    }

    public Anuncio getAnuncioById(String anuncioId) {
        for (Anuncio an : this.listaAnuncio) {
            if (an.hasIdentificador(anuncioId)) {
                return an;
            }
        }
        return null;
    }

    private String geraAnuncioId() {
        return String.valueOf((listaAnuncio.size()) + 1);
    }

    public List<Anuncio> procuraAnunciosElegivel(List<Tarefa> te) {
        List<Anuncio> listaA = new ArrayList<>();

        for (Anuncio anuncio : listaAnuncio) {
            for (Tarefa tar : te) {
                if (anuncio.getTarefa().equals(tar)) {
                    listaA.add(anuncio);
                }
            }
        }
        return listaA;
    }
    public List<Anuncio> getListaAnunciosPorSeriarByEmail(String emailColab){
        Date data = new Date();
        List<Anuncio> anunciosPorSeriar = new ArrayList<>();
        for(Anuncio anuncio: listaAnuncio){
            if(anuncio.getTarefa().getEmailCriadorTarefa().equals(emailColab) 
                    && anuncio.getTR().getTipoSeriacao().equals("Manual") 
                    && anuncio.getpSerIn().compareTo(data)<0 
                    && anuncio.getpSerFi().compareTo(data)>0){
                            anunciosPorSeriar.add(anuncio);
                     }
        }
        return anunciosPorSeriar;    
    }
}
