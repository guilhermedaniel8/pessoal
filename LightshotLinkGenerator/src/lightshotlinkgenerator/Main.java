/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lightshotlinkgenerator;


import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/**
 *
 * @author guilhermedaniel
 */
public class Main {

    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException, MalformedURLException {

        

        Scanner ler = new Scanner(System.in);

        String base = "https://prnt.sc/";

        System.out.println("Quatro letras iniciais pretendidas:");
        String letras = ler.nextLine();

        List<LightshotLinkGenerator> array = new ArrayList<>();

        //-----NÚMEROS COMO DOIS ÚLTIMOS DIGITOS--------------------------------
        LightshotLinkGenerator novo0 = new LightshotLinkGenerator(base, letras, 0, 0);
        array.add(novo0);

        for (int i = 10; i < 100; i++) {
            LightshotLinkGenerator novo1 = new LightshotLinkGenerator(base, letras, i);
            array.add(novo1);
        }
        //----------------------------------------------------------------------

        //----CARACTERES DE A-Z APENAS NO ULTIMO DIGITO-------------------------
        for (int i = 0; i < 10; i++) {
            for (char k = 'a'; k <= 'z'; k++) {
                LightshotLinkGenerator novo2 = new LightshotLinkGenerator(base, letras, i, k);
                array.add(novo2);
            }
        }
        //----CARACTERES DE A-Z APENAS NO PENULTIMO DIGITO----------------------

        for (char k = 'a'; k <= 'z'; k++) {
            for (int i = 0; i < 10; i++) {
                LightshotLinkGenerator novo3 = new LightshotLinkGenerator(base, letras, k, i);
                array.add(novo3);
            }
        }

        //----DUAS LETRAS NOS DOIS ULTIMOS DIGITOS------------------------------
        for (char k = 'a'; k <= 'z'; k++) {
            for (char l = 'a'; l <= 'z'; l++) {
                LightshotLinkGenerator novo4 = new LightshotLinkGenerator(base, letras, k, l);
                array.add(novo4);
            }
        }

        String geral = "";
        for (int i = 0; i < array.size(); i++) {
            LightshotLinkGenerator frase = (LightshotLinkGenerator) array.get(i);

            geral += frase.toString() + "\n";
        }
        
        //COMENTAR PARA NÃO SER MOSTRADO NA CONSOLA
        System.out.println(geral);
        
        
        // DESCOMENTAR AS PROXIMAS DUAS PARA SER GUARDADO NUM FICHEIRO DE TEXTO
//        PrintWriter out = new PrintWriter("lightshotLinks.txt");
//        out.print(geral);

    }

//    public static void listar(List<LightshotLinkGenerator> arrays){
//        for(LightshotLinkGenerator array : arrays){
//            if(array != null){
//                System.out.println(array.toString());
//            }
//        }
//    }
}
