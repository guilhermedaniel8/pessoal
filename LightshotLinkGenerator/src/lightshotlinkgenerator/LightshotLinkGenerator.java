/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lightshotlinkgenerator;

import java.util.Scanner;

/**
 *
 * @author guilhermedaniel
 */
public class LightshotLinkGenerator {
    Scanner ler = new Scanner(System.in);

    private String base;
    private String x;
    private int digitos;
    private int digito0;
    private int dig;
    private String alfa;
    private char carac;
    private char carec2;
    private int contabilizarDoisZero = 0;
    private int contabilizarCaracUltimo = 0;
    private int contabilizarCaracPenUltimo = 0;
    private int contabilizarDoisCarec = 0;
    
    
    public LightshotLinkGenerator(String base, String x, int digitos, int digito0){
        this.base = base;
        this.x = x;
        this.digitos = digitos;
        this.digito0 = 0;
        contabilizarDoisZero++;
    }
    
    public LightshotLinkGenerator(String base, String x, int digitos){
        this.base = base;
        this.x = x;
        this.digitos = digitos;
    }
    
    public LightshotLinkGenerator(String base, String x, int dig, char carac){
        this.base = base;
        this.x = x;
        this.dig = dig;
        this.carac = carac;
        contabilizarCaracUltimo++;
    }
    public LightshotLinkGenerator(String base, String x, char carac, int dig){
        this.base = base;
        this.x = x;
        this.carac = carac;
        this.dig = dig;
        contabilizarCaracPenUltimo++;
    }
    public LightshotLinkGenerator(String base, String x, char carac, char carec2){
        this.base = base;
        this.x = x;
        this.carac = carac;
        this.carec2 = carec2;
        contabilizarDoisCarec++;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }
    
    public String getCarac() {
        return String.valueOf(carac);
    }
    public String toString(){
        if(contabilizarDoisZero != 0){
            return this.base + this.x + this.digitos + this.digito0;
        }
        if(contabilizarCaracUltimo != 0){
            return this.base + this.x + this.dig + this.getCarac();
            
        }
        if(contabilizarCaracPenUltimo !=0 ){
            return this.base + this.x + this.getCarac() + this.dig;
        }
        if(contabilizarDoisCarec != 0){
            return this.base + this.x + this.getCarac() + this.carec2;
        }
        return this.base + this.x + this.digitos;
    }
    
    
    
    
    
}
