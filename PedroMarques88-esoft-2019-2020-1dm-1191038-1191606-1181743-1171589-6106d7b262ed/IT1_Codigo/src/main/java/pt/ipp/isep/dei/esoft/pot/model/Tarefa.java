
package pt.ipp.isep.dei.esoft.pot.model;

public class Tarefa {
    private String refUnica;
    private String designacao;
    private String dInformal;
    private String dTecnica;
    private Double duracao;
    private Double custo; 
    private CategoriaDeTarefa categ;
    
    public Tarefa(String refUnica, String designacao, String dInformal, String dTecnica, Double duracao, Double custo, CategoriaDeTarefa categ){ //CategoriaDeTarefa categ
        if((refUnica == null) || (designacao == null) || (dInformal == null) || (dTecnica == null) || (duracao == null) || (custo == null) || (categ == null) ||
                (refUnica.isEmpty()) || (dInformal.isEmpty()) || (dTecnica.isEmpty()) || (duracao.isNaN()) || (custo.isNaN()))
                throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.refUnica = refUnica;
        this.designacao = designacao;
        this.dInformal = dInformal;
        this.dTecnica = dTecnica;
        this.duracao = duracao;
        this.custo = custo;
        this.categ = categ;
    }

    public String getRefUnica() {
        return refUnica;
    }
    
    public String getDesignacao() {
        return designacao;
    }

    public String getdInformal() {
        return dInformal;
    }

    public String getdTecnica() {
        return dTecnica;
    }

    public Double getDuracao() {
        return duracao;
    }

    public Double getCusto() {
        return custo;
    }

    public CategoriaDeTarefa getCateg() {
        return categ;
    }

    public void setRefUnica(String refUnica) {
        this.refUnica = refUnica;
    }
    
    public void setDesignacao(String designacao){
        this.designacao = designacao;
    }

    public void setdInformal(String dInformal) {
        this.dInformal = dInformal;
    }

    public void setdTecnica(String dTecnica) {
        this.dTecnica = dTecnica;
    }

    public void setDuracao(Double duracao) {
        this.duracao = duracao;
    }

    public void setCusto(Double custo) {
        this.custo = custo;
    }

    public void setCateg(CategoriaDeTarefa categ) {
        this.categ = categ;
    }
    
    public boolean hasRefUnica(String refUnica){
        return this.refUnica.equalsIgnoreCase(refUnica);
    }
    
    public String toString(){
        String str = String.format("%s - %s - %s - %s - %s - %s -%s", this.refUnica, this.designacao, this.dInformal, this.dTecnica, this.duracao, this.custo, this.categ.toString());
        return str;
    }
    
    public boolean equals(Object outroObjeto){
        if(this == outroObjeto){
            return true;
        }
        if(outroObjeto == null || this.getClass() != outroObjeto.getClass()){
            return false;
        }
        Tarefa tarefa = (Tarefa) outroObjeto;
        return this.refUnica.equalsIgnoreCase(tarefa.refUnica) &&
                this.designacao.equalsIgnoreCase(tarefa.designacao) &&
                this.dInformal.equalsIgnoreCase(tarefa.dInformal) &&
                this.dTecnica.equalsIgnoreCase(tarefa.dTecnica) &&
                this.duracao.equals(tarefa.duracao) &&
                this.custo.equals(tarefa.custo) &&
                this.categ.equals(tarefa.categ);
                
    }
}
