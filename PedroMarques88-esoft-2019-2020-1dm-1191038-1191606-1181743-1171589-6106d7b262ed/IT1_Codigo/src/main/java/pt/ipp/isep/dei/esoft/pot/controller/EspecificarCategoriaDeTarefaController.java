
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.pot.model.AreaAtividade;
import pt.ipp.isep.dei.esoft.pot.model.CategoriaDeTarefa;
import pt.ipp.isep.dei.esoft.pot.model.CompetenciaTecnica;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.TipoCompetencia;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

public class EspecificarCategoriaDeTarefaController {
    
    private Plataforma m_oPlataforma;
    private CategoriaDeTarefa m_oCategoria;
    private TipoCompetencia m_oTipo;

    public EspecificarCategoriaDeTarefaController() {
        if (!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_ADMINISTRATIVO)) {
            throw new IllegalStateException("Utilizador não Autorizado");
        }
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }

    public List<AreaAtividade> getAreasAtividade() {
        return this.m_oPlataforma.getAreasAtividade();
    }
    
    public List<CompetenciaTecnica> getCompetenciaTecnica() {
        return this.m_oPlataforma.getCompetenciasTecnicas();
    }

    public boolean novaCategoriaDeTarefa(String identificador, String descricao, String areaAtividadeId) {
        try {
            AreaAtividade areaAtividade = this.m_oPlataforma.getAreaAtividadeById(areaAtividadeId);
            this.m_oCategoria = this.m_oPlataforma.novaCategoriaDeTarefa(identificador, descricao, areaAtividade);
            return this.m_oPlataforma.validaCategoriaDeTarefa(this.m_oCategoria);
        } catch (RuntimeException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oCategoria = null;
            return false;
        }
    }

    public boolean novoTipoCompetencia(String competenciaId, boolean obrigatoriedade) {
        try {
            CompetenciaTecnica competencia = this.m_oPlataforma.getCompetenciaTecnicaById(competenciaId);
            this.m_oTipo = this.m_oPlataforma.novoTipoCompetencia(competencia, obrigatoriedade);
            return this.m_oPlataforma.validaTipoCompetencia(this.m_oTipo);
        } catch (RuntimeException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oTipo = null;
            return false;
        }
    }
 
    public boolean addTipoCompetencia(CompetenciaTecnica competenciaTecnica, boolean obrigatoriedade) {
        return this.m_oCategoria.addCompetenciaPorTipo(competenciaTecnica, obrigatoriedade);
    }
    
    public boolean registaCategoriaDeTarefa() {
        return this.m_oPlataforma.registaCategoriaDeTarefa(m_oCategoria);
    }

    public boolean registaTipoCompetencia() {
        return this.m_oPlataforma.registaTipoCompetencia(m_oTipo);
    }
    
    public String getCategoriaDeTarefaString() {
        return this.m_oCategoria.toString();
    }
}
