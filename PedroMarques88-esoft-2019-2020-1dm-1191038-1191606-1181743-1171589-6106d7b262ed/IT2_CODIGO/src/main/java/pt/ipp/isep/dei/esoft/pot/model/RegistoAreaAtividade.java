
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;

public class RegistoAreaAtividade {
    
    private List<AreaAtividade> listaAreaAtividade = new ArrayList<AreaAtividade>();
    
    public AreaAtividade getAreaAtividadeById(String AreaAtividadeById) {
        for(AreaAtividade area : this.listaAreaAtividade) {
            if (area.hasCodigo(AreaAtividadeById)) {
                return area;
            }
        }
        return null;
    }
    
    public AreaAtividade novaAreaAtividade(String strCodigo, String strDescricaoBreve, String strDescricaoDetalhada) {
        return new AreaAtividade(strCodigo, strDescricaoBreve,strDescricaoDetalhada);
    }

    public boolean registaAreaAtividade(AreaAtividade oArea) {
        if (this.validaAreaAtividade(oArea)) {
            return addAreaAtividade(oArea);
        }
        return false;
    }

    private boolean addAreaAtividade(AreaAtividade oArea) {
        return this.listaAreaAtividade.add(oArea);
    }
    
    public boolean validaAreaAtividade(AreaAtividade oArea) {
        boolean bRet = true;
        if (!this.listaAreaAtividade.isEmpty()) {
            for (AreaAtividade area : this.listaAreaAtividade) {
                if (oArea.equals(area)) {
                bRet = false;
                }
                if (area.hasCodigo(oArea.getCodigo())) {
                bRet = false;    
                }
            }
        }
        return bRet;
    }
    
    public List<AreaAtividade> getAreasAtividade(){
        List<AreaAtividade> listaAreasAtividade = new ArrayList<>();
        listaAreasAtividade.addAll(this.listaAreaAtividade);
        return listaAreasAtividade;
    }
    
    
}
