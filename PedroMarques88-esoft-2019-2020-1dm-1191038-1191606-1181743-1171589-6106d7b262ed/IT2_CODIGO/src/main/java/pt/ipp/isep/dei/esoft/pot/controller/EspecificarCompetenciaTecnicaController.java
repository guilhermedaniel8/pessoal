package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.pot.model.AreaAtividade;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.CompetenciaTecnica;
import pt.ipp.isep.dei.esoft.pot.model.GrauProficiencia;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

public class EspecificarCompetenciaTecnicaController {

    private Plataforma m_oPlataforma;
    private CompetenciaTecnica m_oCompetencia;
    private GrauProficiencia m_oGrauProficiencia;

    public EspecificarCompetenciaTecnicaController() {
        if (!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_ADMINISTRATIVO)) {
            throw new IllegalStateException("Utilizador não Autorizado");
        }
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }

    public List<AreaAtividade> getAreasAtividade() {
        return this.m_oPlataforma.getRegistoAreaAtividade().getAreasAtividade();
    }

    public boolean novaCompetenciaTecnica(String strId, String strDescricaoBreve, String strDescricaoDetalhada, String areaAtividadeId) {
        try {
            AreaAtividade area = this.m_oPlataforma.getRegistoAreaAtividade().getAreaAtividadeById(areaAtividadeId);
            this.m_oCompetencia = this.m_oPlataforma.getRegistoCompetenciaTecnica().novaCompetenciaTecnica(strId, strDescricaoBreve, strDescricaoDetalhada, area);
            return this.m_oPlataforma.getRegistoCompetenciaTecnica().validaCompetenciaTecnica(this.m_oCompetencia);
        } catch (RuntimeException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oCompetencia = null;
            return false;
        }
    }

    public boolean registaCompetenciaTecnica() {
        return this.m_oPlataforma.getRegistoCompetenciaTecnica().registaCompetenciaTecnica(this.m_oCompetencia);
    }

    public String getCompetenciaTecnicaString() {
        return this.m_oCompetencia.toString();
    }

    public boolean addGrauProficiencia(String designacao, Integer valor) {
        GrauProficiencia grauProficiencia = new GrauProficiencia(designacao,valor);
        return this.m_oCompetencia.addListaGrauProficiencia(grauProficiencia);
    }
    
}