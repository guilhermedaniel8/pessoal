package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;

public class Freelancer {
    
    private AutorizacaoFacade m_oAutorizacao;

    private String nome;

    private String NIF;

    private EnderecoPostal enderecoPostal;

    private String contacto;

    private String email;

    private List<HabilitacaoAcademica> listHabilitacaoAcademica;
    
    private List<Reconhecimento> listReconhecimento;

    public Freelancer(String nome, String NIF, String contacto, String email, EnderecoPostal enderecoPostal) {
        if ((nome == null) || (NIF == null) || (contacto == null)
                || (email == null) || (enderecoPostal == null)
                || (nome.isEmpty()) || (NIF.isEmpty()) || (contacto.isEmpty())
                || (email.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.nome = nome;
        this.NIF = NIF;
        this.enderecoPostal = new EnderecoPostal(enderecoPostal);
        this.contacto = contacto;
        this.email = email;
        this.listHabilitacaoAcademica = new ArrayList<>();
        this.listReconhecimento = new ArrayList<>();
    }

    public String getNome() {
        return this.nome;
    }
    
    public String getEmail() {
        return this.email;
    }
    
    public List<Reconhecimento> getListaReconhecimento(){
        return this.listReconhecimento;
    }
    
    public List<Reconhecimento> getCompetenciasEGPReconhecimento(){
        List<Reconhecimento> lista = new ArrayList<>();
        
        for(Reconhecimento r : listReconhecimento){
            Reconhecimento novo = new Reconhecimento(r.getCompetenciaTecnica(), r.getGrauProficiencia());
            lista.add(novo);
        }
        
        return lista;
    }
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.NIF);
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
            }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
            }
        Freelancer free = (Freelancer) o;
        return this.NIF.equalsIgnoreCase(free.NIF) && this.contacto.equalsIgnoreCase(free.contacto) && this.email.equalsIgnoreCase(free.email) && this.enderecoPostal.equals(free.enderecoPostal) && this.nome.equalsIgnoreCase(free.nome);
    }

    @Override
    public String toString() {
        String str = String.format("Freelancer: %s - %s - %s - %s - %s - %s", this.NIF, this.contacto, this.email, this.enderecoPostal.toString(), this.nome, this.listHabilitacaoAcademica);
        return str;
    }

    public static EnderecoPostal novoEnderecoPostal(String strLocal, String strCodPostal, String strLocalidade) {
        return new EnderecoPostal(strLocal, strCodPostal, strLocalidade);
    }

    public boolean addHabilitacaoAcademica(HabilitacaoAcademica ha){ 
    if(this.listHabilitacaoAcademica.contains(ha) || (!this.listHabilitacaoAcademica.isEmpty())){
        return false;  
    }   
     return this.listHabilitacaoAcademica.add(ha);   
    }
    
    public boolean addReconhecimento(Reconhecimento rec){ 
    if(this.listReconhecimento.contains(rec) || (!this.listReconhecimento.isEmpty())){
        return false;  
    }   
     return this.listReconhecimento.add(rec);   
    }
    
    public List<CompetenciaTecnica> getListaCompetenciaTecnica(Freelancer fr){
        List<CompetenciaTecnica> lista = new ArrayList<>();
        
        List<CompetenciaTecnica> l = new ArrayList<>();
        
        for(Reconhecimento re : listReconhecimento){
            lista.add(re.getCompetenciaTecnica());
        }
        
        return lista;
    }

}
