
package pt.ipp.isep.dei.esoft.pot.ui.console;

import java.util.Date;
import java.util.List;
import pt.ipp.isep.dei.esoft.pot.controller.SeriarAnuncioController;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.Freelancer;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

public class SeriarAnuncioUI {
    private SeriarAnuncioController m_controller;

    public SeriarAnuncioUI() {
        m_controller = new SeriarAnuncioController();
    }
    
    public void run(){
        System.out.println("\n Seriar Anuncio:");
        List<Anuncio> listaAnunciosSeriar;
        listaAnunciosSeriar = m_controller.getListaAnunciosPorSeriar();
        if(!(listaAnunciosSeriar == null)){
            int anuncio = Utils.apresentaESelecionaIndex(listaAnunciosSeriar, "Selecione um anuncio para seriar:");
            List<Freelancer> listaFreelancerAnuncio = m_controller.getListaCandidaturas(anuncio);
            Utils.apresentaLista(listaFreelancerAnuncio, "Lista freelancer candidatos:");
            Utils.confirma("Iniciar seriação?");
            Freelancer fr;
            int classificacao;
            for (int i = 0; i < listaFreelancerAnuncio.size(); i++) {
                fr = m_controller.getFreelancerListaCandidaturas(i);
                System.out.println(fr);
                classificacao = Utils.readIntegerFromConsole("Introduza classificação:");
                while(!(m_controller.introduzirClassificação(fr, classificacao))){
                    classificacao = Utils.readIntegerFromConsole("Classificação invalida! Introduza novamente.");
                }
            }
            System.out.println("Introduza os emails dos outros colaboradores envolvidos. (Enter para terminar)");
            String emailColab;
            do{
                emailColab = Utils.readLineFromConsole("Introduza o email do colaborador:(Enter para terminar)");
            }while(m_controller.addColab(emailColab));
            Date data;
            data = Utils.readDateFromConsole("Insira a data de realização do processo.");
            System.out.println(m_controller.definirDataHoraProceeso(data));
            if(Utils.confirma("Deseja confirmar?")){
                m_controller.adicionarAnuncioSeriado();
            }
            System.out.println("Operação concluida com sucesso.");
        }else{
            System.out.println("Não existem anuncios por seriar.");
        }
        
    }
}
