
package pt.ipp.isep.dei.esoft.pot.controller;

import pt.ipp.isep.dei.esoft.pot.model.Colaborador;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;

public class EspecificarColaboradorController {
    private final AplicacaoPOT m_oApp;
    private final Plataforma m_oPlataforma;
    private Organizacao m_oOrganizacao;
    private Colaborador m_oColaborador;
    
    public EspecificarColaboradorController()
    {   
        this.m_oApp = AplicacaoPOT.getInstance();
        if (!m_oApp.getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_GESTOR_ORGANIZACAO)) {
            throw new IllegalStateException("Utilizador não Autorizado");
        }    
        this.m_oPlataforma = m_oApp.getPlataforma();
    }
    
    public boolean novoColaboradorOrganizacao(String nomeC, String funcaoC, String tlfC, String EmailC){    
        String emailGestor = m_oApp.getSessaoAtual().getEmailUtilizador();
        m_oOrganizacao = m_oPlataforma.getRegistoOrganizacao().procurarOrganizacao(emailGestor);
        if(m_oOrganizacao == null){
            throw new IllegalStateException("Organização não encontrada.");
        }       
        m_oColaborador = m_oOrganizacao.novoColaboradorOrganizacao(nomeC, funcaoC, tlfC, EmailC);   
        return(m_oOrganizacao.validarColaboradorOrganizacao(m_oColaborador));
    }
    
    public boolean criarColaborador(){
        return (m_oOrganizacao.criarColaborador(m_oColaborador, m_oPlataforma.getAutorizacaoFacade()));
    }
    
    public String getColaboradorString() {
        return m_oColaborador.toString();
    }
}
