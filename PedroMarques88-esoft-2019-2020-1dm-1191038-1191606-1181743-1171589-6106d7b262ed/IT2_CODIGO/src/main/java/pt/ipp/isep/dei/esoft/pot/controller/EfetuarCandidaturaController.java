
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.List;
import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.Candidatura;
import pt.ipp.isep.dei.esoft.pot.model.CompetenciaTecnica;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Freelancer;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.RegistoCategoriaTarefa;
import pt.ipp.isep.dei.esoft.pot.model.RegistoFreelancer;
import pt.ipp.isep.dei.esoft.pot.model.RegistoTarefa;

public class EfetuarCandidaturaController {
    private Plataforma m_oPlataforma;
    
    
    public EfetuarCandidaturaController() {
        if (!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_FREELANCER)) {
            throw new IllegalStateException("Freelancer não Autorizado");
        }
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }
    
    private RegistoFreelancer rf = this.m_oPlataforma.getRegistoFreelancer();
    private RegistoTarefa rt = this.m_oPlataforma.getRegistoTarefa();
    private RegistoCategoriaTarefa rct = this.m_oPlataforma.getRegistoCategoriaTarefa();
    
    AplicacaoPOT app = AplicacaoPOT.getInstance();
    SessaoUtilizador sessao = app.getSessaoAtual();
    String emailUtilizador = sessao.getEmailUtilizador();
    
    
    public List<Anuncio> getAnElegiveis() {
        Freelancer fr = this.rf.getFreelancerByEmailUtilizador(emailUtilizador);
        List<CompetenciaTecnica> lct = fr.getListaCompetenciaTecnica(fr);
        
        return null;
        
    }
    
    public Candidatura novaCandidatura(String anuncioId){
        return null;
    }
    
    public boolean addInfo(double valorReceber, int diasRealizacao, String texto){
        return false;
    }
    
    public boolean registaCandidatura(){
        return false;
    }
}
