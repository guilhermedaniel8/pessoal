package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;

public class RegistoCompetenciaTecnica {
    
    private CompetenciaTecnica m_oCompetenciaTecnica;
    
    private List<CompetenciaTecnica> listaCompetenciaTecnica = new ArrayList<CompetenciaTecnica>();

    public CompetenciaTecnica getCompetenciaTecnicaById(String CompetenciaTecnicaById) {
        
        for (CompetenciaTecnica oCompTecnica : this.listaCompetenciaTecnica) {
            if (oCompTecnica.hasCodigo(CompetenciaTecnicaById)) {
                return oCompTecnica;
            }
        }
        return null;
    }

    public List<CompetenciaTecnica> listaCTcomAA(AreaAtividade areaAtividade ){
        List<CompetenciaTecnica> listaCTcomAA = new ArrayList<CompetenciaTecnica>();
        for (int i = 0; i < listaCompetenciaTecnica.size(); i++) {
            if (listaCompetenciaTecnica.get(i).getAreaAtividade().equals(areaAtividade)) {
                listaCTcomAA.add(listaCompetenciaTecnica.get(i));
            }
        }
        return listaCTcomAA;
    }
    
    public CompetenciaTecnica novaCompetenciaTecnica(String cod, String dsBreve, String dsDet, AreaAtividade areaAtividade) {
        return new CompetenciaTecnica(cod, dsBreve, dsDet, areaAtividade);
    }

    public GrauProficiencia novaGrauProficiencia(String designacao, Integer valor) {
        return new GrauProficiencia(designacao, valor);
    }

    public boolean registaGrauProficiencia(GrauProficiencia grauProficiencia) {
        if (this.m_oCompetenciaTecnica.addListaGrauProficiencia(grauProficiencia)) {
            return addGrauProficiencia(grauProficiencia);
        }
        return false;
    }
    
    public boolean registaCompetenciaTecnica(CompetenciaTecnica oCompTecnica) {
        if (this.validaCompetenciaTecnica(oCompTecnica)) {
            return addCompetenciaTecnica(oCompTecnica);
        }
        return false;
    }

    private boolean addGrauProficiencia(GrauProficiencia grauProficiencia) {
        return this.m_oCompetenciaTecnica.getListaGrauProficiencia().add(grauProficiencia);
    }    
    
    private boolean addCompetenciaTecnica(CompetenciaTecnica oCompTecnica) {
        return this.listaCompetenciaTecnica.add(oCompTecnica);
    }
    
    public boolean validaGrauProficiencia(GrauProficiencia grauProficiencia) {
        boolean bRet = true;
        if (!this.m_oCompetenciaTecnica.addListaGrauProficiencia(grauProficiencia)) {
             return false;
        }
        return bRet;
    }

    public boolean validaCompetenciaTecnica(CompetenciaTecnica oCompTecnica) {
        boolean bRet = true;
        if (!this.listaCompetenciaTecnica.isEmpty()) {
            for (CompetenciaTecnica ct : this.listaCompetenciaTecnica) {
                if (oCompTecnica.equals(ct)) {
                    bRet = false;
                }
                if (ct.hasCodigo(oCompTecnica.getCodigo())) {
                    bRet = false;
                }
            }
        }
        return bRet;
    }

    public List<CompetenciaTecnica> getCompetenciasTecnicas() {
        List<CompetenciaTecnica> listaCompetenciasTecnicas = new ArrayList<>();
        listaCompetenciasTecnicas.addAll(this.listaCompetenciaTecnica);
        return listaCompetenciasTecnicas;
    }

}
