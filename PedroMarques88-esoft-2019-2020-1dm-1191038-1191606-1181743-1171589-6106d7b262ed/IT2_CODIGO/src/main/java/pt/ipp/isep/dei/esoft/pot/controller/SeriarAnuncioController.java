package pt.ipp.isep.dei.esoft.pot.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.Candidatura;
import pt.ipp.isep.dei.esoft.pot.model.Freelancer;
import pt.ipp.isep.dei.esoft.pot.model.Colaborador;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.RegistoAnuncio;


import java.util.List;
import pt.ipp.isep.dei.esoft.pot.model.AnuncioSeriado;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.RegistoOrganizacao;

public class SeriarAnuncioController {
    private Plataforma m_oPlataforma;
    private AplicacaoPOT m_oApp;
    
    

    public SeriarAnuncioController(){
        if (!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_COLABORADOR_ORGANIZACAO)) {
            throw new IllegalStateException("Utilizador não Autorizado");
        }
        this.m_oApp = AplicacaoPOT.getInstance();
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();        
    }
    
    private String emailColab;
    private List<Anuncio> lstAnunciosSeriar;
    RegistoAnuncio ra;
    RegistoOrganizacao ro;
    
    public List<Anuncio> getListaAnunciosPorSeriar(){
        SessaoUtilizador sessao = m_oApp.getSessaoAtual();
        emailColab = sessao.getEmailUtilizador();
        ra = m_oPlataforma.getRegistoAnuncio();
        ro = m_oPlataforma.getRegistoOrganizacao();
        lstAnunciosSeriar = ra.getListaAnunciosPorSeriarByEmail(emailColab);
        return lstAnunciosSeriar;
    }
    
    private List<Freelancer> lstFreelancer;
    private Anuncio anuncioParaSeriar;
    private AnuncioSeriado anuncioSeriado;
    private List<Candidatura> lstCandidatos;

    public List<Freelancer> getListaCandidaturas(int i){
        anuncioParaSeriar = lstAnunciosSeriar.get(i);
        anuncioSeriado = anuncioParaSeriar.novoAnuncioSeriado();
        lstCandidatos = anuncioParaSeriar.getCandidaturas();
        for(Candidatura candidatura : lstCandidatos){
            lstFreelancer.add(candidatura.getFr());
        }
        return lstFreelancer;
    }
    
    public Freelancer getFreelancerListaCandidaturas(int i){
        if(i < lstFreelancer.size()){
            return lstFreelancer.get(i);
        }
        else{
            List<Freelancer> lstFreelancerSeriado =  Arrays.asList(arrFreelancerSeriado);
            anuncioSeriado.setCandidatosSeriados(lstFreelancerSeriado);
            return null;
        }
    }
    private Freelancer[] arrFreelancerSeriado = new Freelancer[lstFreelancer.size()];
    
    public boolean introduzirClassificação(Freelancer freelancer, int i){
        if(arrFreelancerSeriado[i] == null){
            arrFreelancerSeriado[i] = freelancer;
            return true;
        }
        return false;
    }
     
    private List<Colaborador> lstColab;
    
    public boolean addColab(String emailColab){
        if(!emailColab.isEmpty()){
        Colaborador colab;
        colab = ro.getColabByEmail(emailColab);
        if(colab == null){
            System.out.println("Colaborador não encontrado!");
            return true;
        }
        lstColab.add(colab);
        return true;
        }else{
            anuncioSeriado.setColaboradoresEnvoldidos(lstColab);
            return false;
        }
    }    
    
    public AnuncioSeriado definirDataHoraProceeso(Date dataHora){
        anuncioSeriado.setDataHoraSeriacao(dataHora);
        return anuncioSeriado;
    }
    
    public void adicionarAnuncioSeriado(){
        anuncioParaSeriar.setAnuncioSeriado(anuncioSeriado);
    }
}
