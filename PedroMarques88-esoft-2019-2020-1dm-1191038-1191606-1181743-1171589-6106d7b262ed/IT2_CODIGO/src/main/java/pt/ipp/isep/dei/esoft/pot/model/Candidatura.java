/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Objects;

/**
 *
 * @author guilhermedaniel
 */
public class Candidatura {

    private Freelancer fr;
    private Double valorReceber;
    private int diasRealizacao;
    private String texto;
    private Anuncio anuncio;

    public Candidatura(Freelancer fr) {
        if ((fr == null)) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.fr = fr;

    }
    
    public Candidatura(Freelancer fr, Double valorReceber, Integer diasRealizacao, String texto) {
        if ((fr == null) || (valorReceber == null) || (diasRealizacao == null) || texto.isEmpty()) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.fr = fr;
        this.valorReceber = valorReceber;
        this.diasRealizacao = diasRealizacao;
        this.texto = texto;
    }

    public Candidatura(Freelancer fr, Double valorReceber, Integer diasRealizacao) {
        if ((fr == null) || (valorReceber == null) || (diasRealizacao == null)) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.fr = fr;
        this.valorReceber = valorReceber;
        this.diasRealizacao = diasRealizacao;
    }


    public Candidatura(Freelancer fr, Double valorReceber, Integer diasRealizacao, Anuncio anuncio) {
        if ((fr == null) || (valorReceber == null) || (diasRealizacao == null) || texto.isEmpty() || (anuncio == null)) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }
        this.fr = fr;
        this.valorReceber = valorReceber;
        this.diasRealizacao = diasRealizacao;
        this.anuncio = anuncio;

    }

    public Candidatura(Freelancer fr, Double valorReceber, Integer diasRealizacao, String texto, Anuncio anuncio) {
        if ((fr == null) || (valorReceber == null) || (diasRealizacao == null) || texto.isEmpty() || (anuncio == null)) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.fr = fr;
        this.valorReceber = valorReceber;
        this.diasRealizacao = diasRealizacao;
        this.texto = texto;
        this.anuncio = anuncio;
    }

    public void setInfo(Double valorReceber, int diasRealizacao, String texto) {
        this.valorReceber = valorReceber;
        this.diasRealizacao = diasRealizacao;
        this.texto = texto;

    }
    
    public void setInfo(Double valorReceber, int diasRealizacao){
        this.valorReceber = valorReceber;
        this.diasRealizacao = diasRealizacao;
    }

    public Freelancer getFr() {
        return fr;
    }

    public Double getValorReceber() {
        return valorReceber;
    }

    public int getDiasRealizacao() {
        return diasRealizacao;
    }

    public String getTexto() {
        return texto;
    }

    public void setValorReceber(Double valorReceber) {
        this.valorReceber = valorReceber;
    }

    public void setDiasRealizacao(int diasRealizacao) {
        this.diasRealizacao = diasRealizacao;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
    
    public Freelancer getFreelancer(){
        return fr;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Candidatura other = (Candidatura) obj;
        if (this.diasRealizacao != other.diasRealizacao) {
            return false;
        }
        if (!Objects.equals(this.texto, other.texto)) {
            return false;
        }
        if (!Objects.equals(this.fr, other.fr)) {
            return false;
        }
        if (!Objects.equals(this.valorReceber, other.valorReceber)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Candidatura{" + "fr=" + fr + ", valorReceber=" + valorReceber + ", diasRealizacao=" + diasRealizacao + ", texto=" + texto + '}';
    }

}
