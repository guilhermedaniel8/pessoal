package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Date;
import java.util.List;

public class ListaCandidaturas {

    public List<Candidatura> listaCandidaturas;

    public Candidatura novaCandidatura(Freelancer fr, Date candt, Double valorReceber, Integer diasRealizacao, String texto, String texto2) {
        Candidatura c = new Candidatura(fr, candt, valorReceber, diasRealizacao, texto, texto2);

        return c;
    }

    public boolean existeCandidaturaRepetida(Freelancer freel) {
        for (Candidatura c : listaCandidaturas) {
            if (c.getFreelancer().equals(freel)) {
                return true;
            }
        }
        return false;
    }

    public boolean validaCandidatura(Candidatura cand) {
        if (cand != null) {
            return true;
        }
        return false;
    }

    public boolean addCandidatura(Candidatura cand) {
        return listaCandidaturas.add(cand);
    }

    public boolean isCandidaturaAoAnuncio(Freelancer free) {
        for (Candidatura c : listaCandidaturas) {
            if (c.getFreelancer().equals(free)) {
                return true;
            }
        }
        return false;
    }

    public Candidatura getCandidaturaByFree(Freelancer free) {
        for (Candidatura c : listaCandidaturas) {
            if (c.getFreelancer().equals(free)) {
                return c;
            }
        }
        return null;
    }

    public Candidatura temIdCandidatura(String candidaturaId) {
        for (Candidatura c : listaCandidaturas) {
            if (c.getCandidaturaId().equals(candidaturaId)) {
                return c;
            }
        }
        return null;
    }

    public boolean temCandidatura(Candidatura cand) {
        for (Candidatura c : listaCandidaturas) {
            if (c.equals(cand)) {
                return true;
            }
        }
        return false;
    }

    Candidatura candAtual;

    public boolean atualizaCandidatura(Candidatura cand) {
        return atualizaDadosCand(cand);

    }

    public boolean validaCandAtual(Candidatura candAtual) {
        if (candAtual != null) {
            return true;
        }
        return false;
    }

    public boolean atualizaDadosCand(Candidatura candAtual) {
        if (validaCandAtual(candAtual)) {
            return true;
        }
        return false;
    }

    public boolean removerCandidatura(Candidatura cand) {
        remove(cand);
        return validaRemocao(cand);
    }

    public boolean remove(Candidatura cand) {
        for (Candidatura c : listaCandidaturas) {
            if (c.equals(cand)) {
                return listaCandidaturas.remove(c);
            }
        }
        return false;
    }

    public boolean validaRemocao(Candidatura cand) {
        for (Candidatura c : listaCandidaturas) {
            if (c.equals(cand) == false) {
                return true;
            }
        }
        return false;
    }
    
    public List<Candidatura> getCandidaturas(){
        return this.listaCandidaturas;
    }
    
    public Candidatura getCandidatura(String candId){
        return null;
    }
}
