
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.List;
import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.Candidatura;
import pt.ipp.isep.dei.esoft.pot.model.Freelancer;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.RegistoAnuncio;
import pt.ipp.isep.dei.esoft.pot.model.RegistoFreelancer;

public class RetirarCandidaturaController {
    private Plataforma m_oPlataforma;
    RegistoFreelancer rfr;
    RegistoAnuncio regA;
    List<Candidatura> lstCandFreelancer;
    
    
    public List<Candidatura> getCandidFree(){
        AplicacaoPOT app = AplicacaoPOT.getInstance();
        SessaoUtilizador sessao = app.getSessaoAtual();
        String email = sessao.getEmailUtilizador();
        rfr = m_oPlataforma.getRegistoFreelancer();
        Freelancer free = rfr.getFreelancerByEmailUtilizador(email);
        regA = m_oPlataforma.getRegistoAnuncio();
        lstCandFreelancer = regA.getCandidaturasFreelancer(free);
        return lstCandFreelancer;
    }
    
    public String getCandidFreeToString(){
        String s = "";
        for(Candidatura c : lstCandFreelancer){
            s = s + c.toString() + "\n";
        }
        return s;
    }
    
    Candidatura cand;
    public Candidatura selecionaCandidatura(String candidaturaId){
        cand =regA.getCandSelecionada(candidaturaId);
        return cand;
    }
    
    public boolean removerCandidatura(){
        Anuncio anuncioCand = regA.getAnuncioCand(cand);
        return anuncioCand.removerCandidatura(cand);
    }
}
