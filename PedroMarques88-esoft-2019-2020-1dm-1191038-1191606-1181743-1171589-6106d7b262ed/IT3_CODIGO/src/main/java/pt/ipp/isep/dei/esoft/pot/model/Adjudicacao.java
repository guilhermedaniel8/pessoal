/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Date;


public class Adjudicacao {
    private String numSeq;
    
    private Date dataAdjudi;
    
    private String descTarefa;
    
    private Integer periodoReali;
    
    private Double valorRemu;
    
    private String anuncioId;
    
    public Adjudicacao(String numSeq, Date dataAdjudi, String descTarefa, Integer periodoReali, Double valorRemu, String anuncioId){
        if(numSeq == null || dataAdjudi == null || descTarefa == null || periodoReali == null || valorRemu == null || anuncioId == null){
            throw new IllegalArgumentException("Nenhum dos paremetros pode ser nulo!");
        }
        this.numSeq = numSeq;
        this.dataAdjudi = dataAdjudi;
        this.descTarefa = descTarefa;
        this.periodoReali = periodoReali;
        this.valorRemu = valorRemu;
        this.anuncioId = anuncioId;
    }

    /**
     * @return the numSeq
     */
    public String getNumSeq() {
        return numSeq;
    }

    /**
     * @param numSeq the numSeq to set
     */
    public void setNumSeq(String numSeq) {
        this.numSeq = numSeq;
    }

    /**
     * @return the dataAdjudi
     */
    public Date getDataAdjudi() {
        return dataAdjudi;
    }

    /**
     * @param dataAdjudi the dataAdjudi to set
     */
    public void setDataAdjudi(Date dataAdjudi) {
        this.dataAdjudi = dataAdjudi;
    }

    /**
     * @return the descTarefa
     */
    public String getDescTarefa() {
        return descTarefa;
    }

    /**
     * @param descTarefa the descTarefa to set
     */
    public void setDescTarefa(String descTarefa) {
        this.descTarefa = descTarefa;
    }

    /**
     * @return the periodoReali
     */
    public Integer getPeriodoReali() {
        return periodoReali;
    }

    /**
     * @param periodoReali the periodoReali to set
     */
    public void setPeriodoReali(Integer periodoReali) {
        this.periodoReali = periodoReali;
    }

    /**
     * @return the valorRemu
     */
    public Double getValorRemu() {
        return valorRemu;
    }

    /**
     * @param valorRemu the valorRemu to set
     */
    public void setValorRemu(Double valorRemu) {
        this.valorRemu = valorRemu;
    }

    /**
     * @return the anuncioId
     */
    public String getAnuncioId() {
        return anuncioId;
    }

    /**
     * @param anuncioId the anuncioId to set
     */
    public void setAnuncioId(String anuncioId) {
        this.anuncioId = anuncioId;
    }
}
