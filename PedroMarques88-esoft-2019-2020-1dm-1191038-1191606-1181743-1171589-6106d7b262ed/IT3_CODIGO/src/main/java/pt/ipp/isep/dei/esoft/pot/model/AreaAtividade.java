
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Objects;

public class AreaAtividade {

    private String codigo;

    private String descBreve;

    private String descDetalhada;

    public AreaAtividade(String cod, String dsBreve, String dsDet) {
        if ((cod == null) || (dsBreve == null) || (dsDet == null) || (cod.isEmpty()) || (dsBreve.isEmpty()) || (dsDet.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }
        this.codigo = cod;
        this.descBreve = dsBreve;
        this.descDetalhada = dsDet;
    }

    public AreaAtividade(AreaAtividade area) {
        this.codigo = area.codigo;
        this.descBreve = area.descBreve;
        this.descDetalhada = area.descDetalhada;
    }

    AreaAtividade() {
        this.codigo = "0";
        this.descBreve = "N/A";
        this.descDetalhada = "N/A";
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescBreve() {
        return descBreve;
    }

    public String getDescDetalhada() {
        return descDetalhada;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setDescBreve(String descBreve) {
        this.descBreve = descBreve;
    }

    public void setDescDetalhada(String descDetalhada) {
        this.descDetalhada = descDetalhada;
    }

    public boolean hasCodigo(String codigo) {
        return this.codigo.equalsIgnoreCase(codigo);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.codigo);
        return hash;
    }
    
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }
        AreaAtividade outraArea = (AreaAtividade) outroObjeto;
        return this.codigo.equalsIgnoreCase(outraArea.codigo) && this.descBreve.equalsIgnoreCase(outraArea.descBreve) && this.descDetalhada.equalsIgnoreCase(outraArea.descDetalhada);
    }

    @Override
    public String toString() {
        return String.format("%s - %s - %s ", this.codigo, this.descBreve, this.descDetalhada);
    }

}