
package pt.ipp.isep.dei.esoft.pot.ui.console;

import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import static java.time.temporal.TemporalQueries.localTime;
import java.util.Date;
import java.util.List;
import pt.ipp.isep.dei.esoft.pot.controller.EfetuarCandidaturaController;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

public class EfetuarCandidaturaUI {
    
    private EfetuarCandidaturaController m_controller;
    
    public EfetuarCandidaturaUI(){
         System.out.println("\nEfetuar Candidatura:");
    }
     public void run(){
        System.out.println("\nEfetuar candidatura:");
        
         if (introduzDados()) {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaCandidatura()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possível concluir o registo com sucesso.");
                }
            }
        } else{
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }
    public boolean introduzDados(){
        List<Anuncio> listaAnuncio = m_controller.getAnunciosElegiveisDoFreelancer();
        
        String anuncioId = "";
        Anuncio anuncio = (Anuncio) Utils.apresentaESeleciona(listaAnuncio, "Seleciona a tarefa, por si criada, que pretende publicar");
        
        double valorReceber = Utils.readDoubleFromConsole("Valor a receber:");
        Date dataCandidatura = toDate(LocalTime.now());
        int diasRealizacao = Utils.readIntegerFromConsole("Número de dias necessário à realização:");
        String texto = Utils.readLineFromConsole("Texto de apresentação/ motivação:");
        
        return m_controller.novaCandidatura(anuncioId, dataCandidatura, valorReceber, diasRealizacao, texto, texto);
        
        
    }
    private void apresentaDados() {
        System.out.println("\nEfetuar Candidatura:\n" + m_controller.toString());
    }
    
    public static Date toDate(LocalTime localTime) {
    Instant instant = localTime.atDate(LocalDate.now())
        .atZone(ZoneId.systemDefault()).toInstant();
    return toDate(instant);
  }
    
    public static Date toDate(Instant instant) {
    BigInteger milis = BigInteger.valueOf(instant.getEpochSecond()).multiply(
        BigInteger.valueOf(1000));
    milis = milis.add(BigInteger.valueOf(instant.getNano()).divide(
        BigInteger.valueOf(1_000_000)));
    return new Date(milis.longValue());
  }
    
    
}
