/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.Date;
import java.util.List;
import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.Adjudicacao;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.Candidatura;
import pt.ipp.isep.dei.esoft.pot.model.Colaborador;
import pt.ipp.isep.dei.esoft.pot.model.Freelancer;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.RegistoAdjudicacoes;
import pt.ipp.isep.dei.esoft.pot.model.RegistoAnuncio;
import pt.ipp.isep.dei.esoft.pot.model.RegistoOrganizacao;
import pt.ipp.isep.dei.esoft.pot.model.Tarefa;

/**
 *
 * @author rui3m
 */
public class AdjudicarAnuncioController {
    private Plataforma m_oPlataforma;
    private AplicacaoPOT m_oApp;
    private Adjudicacao m_oAdjudicacao;
    
    public AdjudicarAnuncioController(){
        this.m_oPlataforma = m_oApp.getPlataforma();
    }
        
    public List<Anuncio> getListaAnunciosPorAdjudicar(){
        RegistoOrganizacao ro = m_oPlataforma.getRegistoOrganizacao();
        this.m_oApp = AplicacaoPOT.getInstance();
        SessaoUtilizador sessao = m_oApp.getSessaoAtual();
        String email = sessao.getEmailUtilizador();
        Organizacao org = ro.getOrganizacaoByEmailUtilizador(email);
        Colaborador colab = org.getColaboradorByEmail(email);
        RegistoAnuncio ra = m_oPlataforma.getRegistoAnuncio();
        RegistoAdjudicacoes radj = m_oPlataforma.getM_oRegistoAdjudicacoes();
        List<Adjudicacao> listAdju = radj.getListaAdjudicacao();
        return ra.getAnunciosPorAdjudicar(colab, listAdju);
    }
    
    public boolean novaAdjudicacao(String anuncioId){
        try{
            Anuncio anu = m_oPlataforma.getRegistoAnuncio().getAnuncioById(anuncioId);
            Candidatura cnd = anu.getCandidatoAdjudicar();
            Tarefa tar = anu.getTarefa();
            String descTarefa =tar.getDesignacao();
            int periodoRealizacao = cnd.getDiasRealizacao();
            double valorRemu = cnd.getValorReceber();
            String refAnu = anu.getAnunId();
            RegistoAdjudicacoes radj = m_oPlataforma.getM_oRegistoAdjudicacoes();
            Freelancer fr = cnd.getFr();
            Date dataAdjudi = radj.getDataAdjudicacao();
            m_oAdjudicacao =  radj.novaAdjudicacao(refAnu, dataAdjudi, descTarefa, periodoRealizacao, valorRemu, anuncioId);
            return true;
        }catch(RuntimeException ex){
            return false;
        }
    }
    
    public boolean adjudicarAnuncio(){
        RegistoAdjudicacoes radj = m_oPlataforma.getM_oRegistoAdjudicacoes();
        if(radj.validaAdjudicacao(m_oAdjudicacao)){
            return radj.addAdjudicacao(m_oAdjudicacao);
        }
        return false;
    }
    
    @Override
    public String toString(){
        return m_oAdjudicacao.toString();
    }
}
