/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author guilhermedaniel
 */
public class RegistoTarefa {

    private List<Tarefa> listaTarefa = new ArrayList<Tarefa>();
    

    public Tarefa novaTarefa(String refUnica, String designacao, String dInformal, String dTecnica, Integer duracao, Double custo, Categoria categ, Organizacao org, Colaborador colab) {
        return new Tarefa(colab, refUnica, designacao, dInformal, dTecnica, duracao, custo, categ);
    }

    public boolean validaTarefa(Tarefa tarefa) {
        boolean bRet = true;
        if (!this.listaTarefa.isEmpty()) {
            for (Tarefa tf : listaTarefa) {
                if (tarefa.equals(tf)) {
                    return false;
                }
                if (tf.hasRefUnica(tarefa.getRefUnica())) {
                    return false;
                }
            }
        }
        return bRet;
    }

    public boolean registaTarefa(Tarefa tarefa) {
        if (this.validaTarefa(tarefa)) {
            return addTarefa(tarefa);
        }
        return false;
    }

    private boolean addTarefa(Tarefa tarefa) {
        return listaTarefa.add(tarefa);
    }

    public List<Tarefa> getTarefasByEmailUtilizador(String email) {
        List<Tarefa> tarefasDoUtilizador = new ArrayList<Tarefa>();
        for (Tarefa tarefa : listaTarefa) {
            if (tarefa.getEmailCriadorTarefa().equals(email)) {
                tarefasDoUtilizador.add(tarefa);
            }
        }
        if (tarefasDoUtilizador.size() == 0) {
            return null;
        }
        return tarefasDoUtilizador;
    }

    public Tarefa getTarefa(String refUnica, Organizacao org) {
        for (Tarefa tarefa : listaTarefa) {
            if (tarefa.getOrg().equals(org)) {
                if (tarefa.getRefUnica().equals(refUnica)) {
                    return tarefa;
                }
            }
        }
        return null;
    }

    public List<Tarefa> procuraTarefasElegivel(List<Categoria> ctgE) {
        List<Tarefa> listaT = new ArrayList<>();
        
        for (Tarefa tar : listaTarefa) {
            
            
        }
        return listaT;
    }

}
