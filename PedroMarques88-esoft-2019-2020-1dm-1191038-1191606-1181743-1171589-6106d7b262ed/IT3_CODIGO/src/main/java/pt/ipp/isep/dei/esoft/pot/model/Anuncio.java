/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;


import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author guilhermedaniel
 */
public class Anuncio {
    
    private String anunId;
    private Colaborador c;
    private Tarefa tarefa;
    private Date pTarIn;
    private Date pTarFi;
    private Date pAprIn;
    private Date pAprFi;
    private Date pSerIn;
    private Date pSerFi;
    private TipoRegimento tr;
    private String desTr;
    private ListaCandidaturas listaCandidatura;
    private String identificador;
    private ProcessoSeriacao ps;
    
    public Anuncio(Tarefa tarefa){
        if(tarefa == null)
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.tarefa = tarefa;
    }
    public Anuncio(Tarefa tarefa, Date pTarIn, Date pTarFi, Date pAprIn, Date pAprFi, Date pSerIn, Date pSerFi, String desTr){
        this.tarefa = tarefa;
        this.pTarIn = pTarIn;
        this.pTarFi = pTarFi;
        this.pAprIn = pAprIn;
        this.pAprFi = pAprFi;
        this.pSerIn = pSerIn;
        this.pSerFi = pSerFi;
        this.desTr = desTr;
    }
    public Anuncio( Colaborador c, Tarefa tarefa, String anunId,Date pTarIn, Date pTarFi, Date pAprIn, Date pAprFi, Date pSerIn, Date pSerFi, TipoRegimento tr){ 
        if((tarefa == null) || (pTarIn == null) || (pTarFi == null) || (pAprIn == null) || (pAprFi == null) || (pSerIn == null) || (pSerFi == null) || (tr == null))
                throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        this.anunId = anunId;
        this.c = c;
        this.tarefa = tarefa;
        this.pTarIn = pTarIn;
        this.pTarFi = pTarFi;
        this.pAprIn = pAprIn;
        this.pAprFi = pAprFi;
        this.pSerIn = pSerIn;
        this.pSerFi = pSerFi;
        this.tr = tr;
        
    }
    
    public Anuncio(Colaborador c, Tarefa tarefa, Date pTarIn, Date pTarFi, Date pAprIn, Date pAprFi, Date pSerIn, Date pSerFi, TipoRegimento tr){ 
        if((tarefa == null) || (pTarIn == null) || (pTarFi == null) || (pAprIn == null) || (pAprFi == null) || (pSerIn == null) || (pSerFi == null) || (tr == null))
                throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
    
        this.c = c;
        this.tarefa = tarefa;
        this.pTarIn = pTarIn;
        this.pTarFi = pTarFi;
        this.pAprIn = pAprIn;
        this.pAprFi = pAprFi;
        this.pSerIn = pSerIn;
        this.pSerFi = pSerFi;
        this.tr = tr;
        
    }
    
    public boolean aceitaCandidaturas(){
        return true;
    }
    
    public boolean eFreelancerElegivel(Freelancer free){
        return true;
    }
    
    public Date getDataInicialCand(){
        return this.pAprIn;
    }
    
    public long duracaoPeriodoCand(){
      long difEmMilisegundos = Math.abs(pAprFi.getTime() - pAprFi.getTime());
      long diferenca = TimeUnit.DAYS.convert(difEmMilisegundos, TimeUnit.MILLISECONDS);
        
        return diferenca;  
    }
    
    public Candidatura novaCandidatura(Freelancer freel, Date dataCand, Double valorReceber, Integer nrDias,String txtApres, String txtMotiv){
        Candidatura c = new Candidatura(freel,dataCand,valorReceber, nrDias,txtApres,txtMotiv);
        if(aceitaCandidaturas()){
            if(eFreelancerElegivel(freel)){
                listaCandidatura.novaCandidatura(freel, dataCand, valorReceber, nrDias, desTr, desTr);
            }
        }
        return c;
    }
    
    public Colaborador getColaborador(){
        return c;
    }
    
    public Tarefa getTarefa() {
        return tarefa;
    }

    public Date getpTarIn() {
        return pTarIn;
    }

    public Date getpTarFi() {
        return pTarFi;
    }

    public Date getpAprIn() {
        return pAprIn;
    }

    public Date getpAprFi() {
        return pAprFi;
    }

    public Date getpSerIn() {
        return pSerIn;
    }

    public Date getpSerFi() {
        return pSerFi;
    }
    
    public ListaCandidaturas getListaCandidaturas(){
        return listaCandidatura;
    }
    
    
    public boolean isCandidatoAoAnuncio(Freelancer free){
        return listaCandidatura.isCandidaturaAoAnuncio(free);
    }
    
    public boolean registaCandidatura(Candidatura cand){
        Freelancer free = cand.getFreelancer();
        if(aceitaCandidaturas() && eFreelancerElegivel(free)){
            return listaCandidatura.addCandidatura(cand);
        }
        return false;
    }

    public void setPer(Date pTarIn,Date pTarFi,Date pAprIn,Date pAprFi,Date pSerIn,Date pSerFi){
        this.pTarIn = pTarIn;
        this.pTarFi = pTarFi;
        this.pAprIn = pAprIn;
        this.pAprFi = pAprFi;
        this.pSerIn = pSerIn;
        this.pSerFi = pSerFi;
    }
    
    public void setTR(TipoRegimento tr){
        this.tr = tr;
    }
    
    public TipoRegimento getTR(){
        return this.tr;
    }
    
    public boolean equals(Object outroObjeto){
        if( this == outroObjeto){
            return true;
        }
        if(outroObjeto == null || this.getClass() != outroObjeto.getClass()){
            return false;
        }
        Anuncio anun = (Anuncio) outroObjeto;
        return this.tarefa.equals(anun.tarefa) && this.pTarIn.equals(anun.pTarIn) && this.pTarFi.equals(anun.pTarFi) &&
                this.pAprIn.equals(anun.pAprIn) && this.pAprFi.equals(anun.pAprFi) && this.pSerIn.equals(anun.pSerIn) &&
                this.pSerFi.equals(anun.pSerFi);
    }
    
    public boolean hasIdentificador(String identificador) {
        return this.identificador.equalsIgnoreCase(identificador);
    }
    
   
    
    public String toString(){
        return String.format("Tarefa:%s - Período de Publicitação inicial e final:%s %s - Período de apresentação de candidaturas:%s %s - Período de seriação e atribuição:%s %s",tarefa.toString(),pAprIn,pAprFi,pTarIn,pTarFi,pSerIn,pSerFi);
    }
            
    

    
    
    public long getPeriodo(Date dtInS,Date dtFimS){
        long difEmMilisegundos = Math.abs(dtFimS.getTime() - dtInS.getTime());
        long diferenca = TimeUnit.DAYS.convert(difEmMilisegundos, TimeUnit.MILLISECONDS);
        
        return diferenca;
    }

    public boolean atualizaCandidatura(Candidatura cand){
        return listaCandidatura.atualizaCandidatura(cand);
    }
    
    public boolean removerCandidatura(Candidatura cand){
        return listaCandidatura.removerCandidatura(cand);
    }
    public boolean isSeriadio(){
        return (!(ps == null));
    }

    /**
     * @return the anunId
     */
    public String getAnunId() {
        return anunId;
    }
    
    public Candidatura getCandidatoAdjudicar(){
        for(Classificacao cla : ps.getLstclassi()){
            if(cla.getOrdem() == 1){
                return cla.getCandidatura();
            }
        }
        return null;
    }

    /**
     * @return the ps
     */
    public ProcessoSeriacao getProcessoSeriacao() {
        return ps;
    }

    /**
     * @param processoSeriacao the ps to set
     */
    public void setProcessoSeriacao(ProcessoSeriacao processoSeriacao) {
        this.ps = processoSeriacao;
    }
    
    public ListaCandidaturas getCandidaturas(){
        return this.getCandidaturas();
    } 
    
    public boolean novoProcessorSeriacao(Colaborador colab){
        return true;
    }
    
    public boolean registaProcessoSeriacao(ProcessoSeriacao ps){
        if(validaProcessoSeriacao(ps)){
            setProcessoSeriacao(ps);
            return true;
        }
        return false;
    }
    
    public boolean validaProcessoSeriacao(ProcessoSeriacao ps){
        if(ps != null){
            return true;
        }
        return false;
    }
   
    public boolean isAtribuicaoObrigatoria(){
        return true;
    }
    
    public void TimerTask(){
        
    }
    
    public boolean isPrimeiraSeriacao(){
        if (this.ps == null) {
            return true;
        }
        return false;
    }
    
    
    
    public boolean validaProcessoSeriacao(){
        if(this.ps == null){
           return true;
        }
        return false;
    }
    
    
}
