package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Objects;

public class GrauProficiencia implements Comparable<GrauProficiencia>{

    private String designacao;

    private Integer valor;

    public GrauProficiencia(String designacao, Integer valor) {
        if ((designacao == null) || (designacao.isEmpty()) || valor == null) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }
        this.designacao = designacao;
        this.valor = valor;
    }

    public GrauProficiencia(GrauProficiencia gp) {
        this.designacao = gp.designacao;
        this.valor = gp.valor;
    }

    public String getDesignacao() {
        return designacao;
    }

    public Integer getValor() {
        return valor;
    }

    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    public void setDescBreve(int valor) {
        this.valor = valor;
    }

    public boolean hasValor(int valor) {
        return this.valor == valor;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.valor);
        return hash;
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }
        GrauProficiencia gp = (GrauProficiencia) outroObjeto;
        return this.designacao.equalsIgnoreCase(gp.designacao) && this.valor == gp.valor;
    }

    @Override
    public String toString() {
        return String.format("%s - %s", this.valor, this.designacao);
    }
    
    @Override
    public int compareTo(GrauProficiencia grauProficiencia) {
        int valor1 = this.valor;
        int valor2 = grauProficiencia.valor;
        
        if(valor1<valor2)
            return -1;
        else if(valor1>valor2)
            return 1;
        else
            return 0;
    }

}
