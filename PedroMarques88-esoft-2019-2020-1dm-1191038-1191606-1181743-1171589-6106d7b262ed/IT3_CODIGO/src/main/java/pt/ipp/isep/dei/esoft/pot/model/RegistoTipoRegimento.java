
package pt.ipp.isep.dei.esoft.pot.model;

import pt.ipp.isep.dei.esoft.pot.controller.*;
import java.util.ArrayList;
import java.util.List;
import pt.ipp.isep.dei.esoft.pot.model.TipoRegimento;

public class RegistoTipoRegimento {
    private List<TipoRegimento> listaTR;
    
    public TipoRegimento getTipoRegimentoByDesc(String descTR){
        TipoRegimento tr = null;
        for(TipoRegimento t : listaTR){
            if(t.getDescricao().equals(descTR)){
                tr = t;
            }
        }
        return tr;
    }
    
    private List<TipoRegimento> listaTipoRegimento = new ArrayList<TipoRegimento>();
    
    public List<TipoRegimento> getTR(){
        List<TipoRegimento> listaTiposRegimento = new ArrayList<>();
        listaTiposRegimento.addAll(this.listaTipoRegimento);
        return listaTiposRegimento;
    }
    
}
