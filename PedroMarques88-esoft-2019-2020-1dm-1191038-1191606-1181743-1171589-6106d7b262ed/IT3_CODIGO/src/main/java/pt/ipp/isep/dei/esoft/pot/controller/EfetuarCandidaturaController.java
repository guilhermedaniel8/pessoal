
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.Date;
import java.util.List;
import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.Candidatura;
import pt.ipp.isep.dei.esoft.pot.model.CompetenciaTecnica;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Freelancer;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.RegistoAnuncio;
import pt.ipp.isep.dei.esoft.pot.model.RegistoCategoriaTarefa;
import pt.ipp.isep.dei.esoft.pot.model.RegistoFreelancer;
import pt.ipp.isep.dei.esoft.pot.model.RegistoTarefa;

public class EfetuarCandidaturaController {
    private Plataforma m_oPlataforma;
    
    
    public EfetuarCandidaturaController() {
        if (!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_FREELANCER)) {
            throw new IllegalStateException("Freelancer não Autorizado");
        }
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }
    
    
    
    private RegistoFreelancer rf = this.m_oPlataforma.getRegistoFreelancer();
    private RegistoTarefa rt = this.m_oPlataforma.getRegistoTarefa();
    private RegistoCategoriaTarefa rct = this.m_oPlataforma.getRegistoCategoriaTarefa();
    private RegistoAnuncio ranu = this.m_oPlataforma.getRegistoAnuncio();
    
    AplicacaoPOT app = AplicacaoPOT.getInstance();
    SessaoUtilizador sessao = app.getSessaoAtual();
    String emailUtilizador = sessao.getEmailUtilizador();
    
    Freelancer freel;
    public List<Anuncio> getAnunciosElegiveisDoFreelancer() {
        freel = this.rf.getFreelancerByEmailUtilizador(emailUtilizador);
        List<CompetenciaTecnica> lct = freel.getListaCompetenciaTecnica(freel);
        
        return null;
        
    }
    
    Anuncio anu;
    public boolean verificarSeCandidaturaRepetida(String anuncioId){
        anu = ranu.getAnuncioById(anuncioId);
        return anu.getListaCandidaturas().existeCandidaturaRepetida(freel);
    }
    Candidatura cand;
    public boolean novaCandidatura(String anuncio, Date data, Double valorPrt, Integer nrDias, String txtApres, String txtMotiv){
    
    cand = anu.novaCandidatura(freel, data, valorPrt, nrDias, txtApres, txtMotiv);
    if(cand!= null){
        return false;
    }
    return true;
    }
    
    public boolean registaCandidatura(){
        return true;
    }
    
}
