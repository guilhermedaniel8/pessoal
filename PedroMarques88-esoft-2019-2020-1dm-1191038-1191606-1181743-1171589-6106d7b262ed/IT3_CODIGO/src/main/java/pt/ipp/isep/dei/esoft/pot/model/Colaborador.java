
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

/**
 *
 * @author paulomaio
 */
public class Colaborador
{
    private String m_strNome;
    private String m_strFuncao;
    private String m_strTelefone;
    private String m_strEmail;
            
    
    public Colaborador(String strNome, String strFuncao, String strTelefone, String strEmail)
    {
        if ( (strNome == null) || (strFuncao == null) || (strTelefone == null) || (strEmail == null) ||
                (strNome.isEmpty())|| (strFuncao.isEmpty())|| (strTelefone.isEmpty())|| (strEmail.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.m_strNome = strNome;
        this.m_strFuncao = strFuncao;
        this.m_strTelefone = strTelefone;
        this.m_strEmail = strEmail;
    }
    
    public boolean hasId(String strId)
    {
        return this.m_strEmail.equalsIgnoreCase(strId);
    }
    
    public String getNome()
    {
        return this.m_strNome;
    }
    
    public String getEmail()
    {
        return this.m_strEmail;
    }
   
    
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.m_strEmail);
        return hash;
    }
    
    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        Colaborador obj = (Colaborador) o;
        return (Objects.equals(m_strEmail, obj.m_strEmail));
    }
    
    public String gerarPWD(){
        final String chars_b = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String chars_s = "abcdefghijklmnopqrstuvwxyz"; //26
        final String nums = "0123456789";
        final String special_char = "!#$%&/()=?<>«»*+"; 
        Random rand = new Random(); 
        int length = rand.nextInt(6) + 12;
        char[] arrPWD = new char[length];
        int c;
        char s;
        for (int i = 0; i < length; i++) {
            if(i%2==0){
                c = rand.nextInt(2) + 1;
            }else{
                c = 3 + rand.nextInt(2);
            }
            switch (c){
                    case 1:
                        c = rand.nextInt(25);
                        s = chars_b.charAt(c);
                        arrPWD[i] = s;
                        break;
                    case 2:
                        c = rand.nextInt(25);
                        s = chars_s.charAt(c);
                        arrPWD[i] = s;
                        break;
                    case 3:
                        c = rand.nextInt(9);
                        s = nums.charAt(c);
                        arrPWD[i] = s;
                        break;
                    case 4:
                        c = rand.nextInt(15);
                        s = special_char.charAt(c);
                        arrPWD[i] = s;
                        break;
            }
        }
        return new String(arrPWD);
    }
    
    @Override
    public String toString()
    {
        return String.format("%s - %s - %s - %s", this.m_strNome, this.m_strFuncao, this.m_strTelefone, this.m_strEmail);
    }
}
