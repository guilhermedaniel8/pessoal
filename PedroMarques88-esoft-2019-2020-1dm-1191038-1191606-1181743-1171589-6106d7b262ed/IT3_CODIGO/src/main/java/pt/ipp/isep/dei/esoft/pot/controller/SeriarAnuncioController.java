package pt.ipp.isep.dei.esoft.pot.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.Candidatura;
import pt.ipp.isep.dei.esoft.pot.model.Freelancer;
import pt.ipp.isep.dei.esoft.pot.model.Colaborador;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.RegistoAnuncio;

import java.util.List;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.ListaCandidaturas;
import pt.ipp.isep.dei.esoft.pot.model.ListaColaboradores;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.ProcessoSeriacao;
import pt.ipp.isep.dei.esoft.pot.model.RegistoOrganizacao;

public class SeriarAnuncioController {

    private Plataforma m_oPlataforma;
    private AplicacaoPOT m_oApp = AplicacaoPOT.getInstance();
    private SessaoUtilizador sessao;
    private SeriarAnuncioController m_oController;
    private String email;
    private Organizacao org;
    private ListaColaboradores lstColabs;
    private RegistoOrganizacao rorgs;
    Colaborador colab;
    private RegistoAnuncio ra;

    private List<Freelancer> lstFreelancer;
    private Anuncio anuncioParaSeriar;
    private ListaCandidaturas lstCandidatos;
    private ProcessoSeriacao ps;
    private Anuncio anu;

    public List<Anuncio> getAnunciosPorSeriarNaoAutomaticos() {
        sessao = m_oApp.getSessaoAtual();
        email = sessao.getEmailUtilizador();
        rorgs = m_oPlataforma.getRegistoOrganizacao();
        org = rorgs.getOrganizacaoByEmailUtilizador(email);
        lstColabs = org.getListaColaboradores();
        colab = lstColabs.getColaboradorByEmail(email);
        ra = m_oPlataforma.getRegistoAnuncio();
        return ra.getAnunciosPorSeriarNaoAutomaticos(colab);

    }
    Anuncio an;
    public List<Candidatura> getCandidaturas(String anuncioId) {
        an = ra.getAnuncioPublicadoPor(colab, email);
        return null;
    }

    public boolean classifica(String candId, Integer ordem, String justificacao) {
        Candidatura cand;
        cand = lstCandidatos.getCandidatura(candId);
        return ps.addClassificacao(cand, ordem, justificacao);

    }
    
    public List<Colaborador> getColaboradores(){
        return lstColabs.getColaboradores();
    }
    
    public boolean addTextoConclusao(String textoConclusao){
        return ps.addTextoConclusao(textoConclusao);
    }

    public boolean confirmaProcessoSeriacao(){
        an.registaProcessoSeriacao(ps);
        return an.isAtribuicaoObrigatoria();
        
    }
}
