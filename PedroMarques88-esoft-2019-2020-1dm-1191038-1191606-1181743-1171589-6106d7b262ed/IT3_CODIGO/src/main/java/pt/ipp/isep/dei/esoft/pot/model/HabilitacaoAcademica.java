
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Objects;

public class HabilitacaoAcademica {
    
    private String grau;
    private String desigCurso;
    private String instituicao;
    private Double media;
    
    public HabilitacaoAcademica(String grau, String desigCurso, String instituicao, Double media) {
        if ( (grau == null) || (desigCurso == null) || (instituicao == null) || (media == null) ||
                (grau.isEmpty())|| (desigCurso.isEmpty()) || (instituicao.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.grau = grau;
        this.desigCurso = desigCurso;
        this.instituicao = instituicao;
        this.media = media;
    }
    
    public HabilitacaoAcademica(HabilitacaoAcademica ha) {
        this.grau = ha.grau;
        this.desigCurso = ha.desigCurso;
        this.instituicao = ha.instituicao;
        this.media = ha.media;
    }
    
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 23 * hash + Objects.hash(this.grau, this.desigCurso, this.instituicao, this.media);
        return hash;
    }
    
    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        HabilitacaoAcademica obj = (HabilitacaoAcademica) o;
        return this.grau.equalsIgnoreCase(obj.grau) && this.desigCurso.equalsIgnoreCase(obj.desigCurso) && this.instituicao.equalsIgnoreCase(obj.instituicao) && this.media == obj.media;
    }
    
    @Override
    public String toString()
    {
        return String.format("Habilitação Académica: %s - %s - %s - %s", this.grau, this.desigCurso, this.instituicao, this.media);
    }
    
}
