# UC 11 - Atualizar Candidatura

## 1. Engenharia de Requisitos

### Formato Breve

O freelancer inicia a atualização de uma candidatura previamente efetuada por si. O sistema solicita a escolha da candidatura de uma lista de candidaturas efetuadas por si que estejam **dentro do período de apresentanção de candidaturas**. O freelancer identifica a candidatura que pretende atualizar. O sistema apresenta a candidatura selecionada para ser atualizada e solicita a atualização dos dados que pretende modificar. O freelancer atualiza os dados solicitados. O sistema valida e apresenta todos os dados da candidatura ao freelancer e solicita confirmação. O freelancer confirma. O sistema regista os dados, atualiza a informação da candidatura e informa o freelancer do sucesso da operação.


### SSD
![UC11-SSD.svg](UC11_SSD.svg)


### Formato Completo

#### Ator principal

* Freelancer

#### Partes interessadas e seus interesses

* **Colaborador de Organização**: pretende receber candidaturas devidamente atualizadas aos Anúncios publicados.
* **Freelancer:** pretende atualizar a Candidatura previamente efetuada por si .
* **Organização:** pretende receber Candidaturas para as Tarefas publicadas para a execuação por freelancer.
* **T4J:** pretende receber as Candidaturas devidamente atualizadas para posterior atribuição das tarefas a freelancers.

#### Pré-condições

* O freelancer deve ter efetuado, pelo menos, uma candidatura.

#### Pós-condições

* A Candidatura que o freelancer pretende atualizar, é atualizada.

#### Cenário de sucesso principal (ou fluxo básico)

1. O freelancer inicia a atualização de uma candidatura previamente efetuada por si. 
2. O sistema solicita a escolha da candidatura de uma lista de candidaturas efetuadas por si que estejam dentro do período de aprestanção de candidaturas. 
3. O freelancer identifica a candidatura que pretende atualizar. 
4. O sistema apresenta a candidatura selecionada para ser atualizada e solicita a atualização dos dados que pretende modificar.
5. O freelancer atualiza os dados solicitados. 
6. O sistema valida e apresenta todos os dados da candidatura ao freelancer e solicita confirmação.
7. O freelancer confirma.
8. O sistema regista os dados, atualiza a informação da candidatura e informa o freelancer do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O freelancer solicita o cancelamento da atualização da candidatura.
> O caso de uso termina.

#### Requisitos especiais

-

#### Lista de Variações de Tecnologias e Dados

-

#### Frequência de Ocorrência

-

#### Questões em aberto

(lista de questões em aberto, i.e. sem uma resposta conhecida.)

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC11-MDsvg](UC11_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O freelancer inicia a atualização de uma candidatura previamente efetuada por si          |    ...interage com o utilizador?                         |     AtualizarCandidaturaUI        |      Pure Fabrication                        |
|    |  ...coordena a UC?   |  AtualizarCandidaturaController  |  Controller   |
|    |   ...conhece o utilizador/Freelancer a usar o sistema?       |    SessaoUtilizador         |    IE: cf. documentação do componente de gestão de utilizadores.      |
|    |   ...conhece o freelancer?  |  Plataforma  |  Conhece todos os Freelancers.  |
|    |      |   RegistoFreelancer  |  Por aplicação de HC+LC delega a RegistoFreelancer |
| 2. O sistema solicita a escolha da candidatura de uma lista de candidaturas efetuadas por si que estejam dentro do período de aprestanção de candidaturas.            |      ...conhece as candidaturas?       |    Anúncio    |     IE: no MD o Anúncio possui Candidaturas.     |
|    |      |   ListaCandidaturas   |   IE: no MD o Anúncio possui Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas.  |
|    |  ...conhece os períodos de apresentação de candidaturas? | Anúncio | O anúncio agrega diversos períodos de uma tarefa.  |
| 3. O freelancer identifica a candidatura que pretende atualizar |   |   |   |
| 4. O sistema apresenta a candidatura selecionada para ser atualizada e solicita a atualização dos dados que pretende modificar.      |             |             |             |
| 5. O freelancer atualiza os dados solicitados.    |     ...guarda os dados atualizados?      |  Anúncio           |  No MD é o Anúncio que contém Candidaturas         |
|       |    | ListaCandidaturas  |  Por aplicação de HC+LC delega a ListaCandidaturas |
|    |     | Candidatura  | IE: A Candidatura pode alterar os seus dados.  |
| 6. O sistema valida e apresenta todos os dados da candidatura ao freelancer e solicita confirmação.   | ... valida os dados atualizados da Candidatura (validação local)?     |  Candidatura    |  IE: possui os seus próprios dados.  |  
|    |  ... valida os dados atualizados da Candidatura (validação global)?   |  ListaCandidaturas |  IE: no MD o Anúncio recebe Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas |
| 7. O freelancer confirma. |  |  |  |
| 8. O sistema regista os dados, atualiza a informação da candidatura e informa o freelancer do sucesso da operação. |   ...guarda a Candidatura com os dados atualizados? | Anúncio | IE: no MD o Anúncio recebe Candidaturas. |
|       |       |  ListaCandidaturas  |  IE: no MD o Anúncio recebe Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas.  |
|       |  ...informa o colaborador?    |  AtualizarCandidaturaUI |     |

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Candidatura
 * Freelancer
 * Anúncio

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * AtualizarCandidaturaUI  
 * AtualizarCandidaturaController
 * RegistoFreelancer
 * RegistoAnuncios
 * ListaCandidaturas
 
 Outras classes de sistemas/componentes externos:
 
 * SessaoUtilizador


###    Diagrama de Sequência

![UC11-SD.svg](UC11_SD.svg)

![UC11-SD-getCandidaturasFree.svg](UC11_SD_getCandidaturasFree.svg)


###    Diagrama de Classes

![UC11-CD.svg](UC11_CD.svg)
