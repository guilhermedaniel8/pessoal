# UC9 - Efetuar Candidatura

## 1. Engenharia de Requisitos

### Formato Breve

O freelancer inicia o processo de candidatura. O sistema apresenta todos os anúncios elegíveis com o GP mínimo às CT obrigatórias para o freelancer e pede para selecionar um destes. O freelancer seleciona o anúncio pretendido para efetuar candidatura. O sistema solicita os dados necessários para efetuar a candidatura a um anúncio (i.e. valor pretendido pela realização da tarefa, o número de dias para realização da tarefa após atribuição da mesma e um texto de apresentação). O freelancer introduz os dados solicitados. O sistema valida e apresenta todos os dados, pedindo que os confirme. O freelancer confirma. O sistema regista os dados da candidatura efetuada e informa o freelancer do sucesso da operação.

### SSD

![UC9_SSD.svg](UC9_SSD.svg)

### Formato Completo

#### Ator principal

* Freelancer

#### Partes interessadas e seus interesses

* **Freelancer:** pretende candidatar-se ao anúncio que resulta da publicação da tarefa.
* **Organização:** pretende que os seus colaboradores possam publicar tarefas para posterior seriação de freelancers.
* **T4J:** pretende que os freelancers se candidatem às tarefas anunciadas.

#### Pré-condições

* O freelancer deve possuir o grau de proficiência mínimo exigido a todas as competência técnicas obrigatórias para o anúncio da tarefa que efetua candidatura.
* No sistema devem existir anúncios de tarefas para candidatura por parte do freelancer.

#### Pós-condições

* A candidatura do freelancer, ao anúncio é efetuada.

#### Cenário de sucesso principal (ou fluxo básico)


1. O freelancer inicia o processo de candidatura.
2. O sistema apresenta todos os anúncios elegíveis com o GP mínimo às CT obrigatórias para o freelancer e pede para selecionar um.
3. O freelancer seleciona o anúncio pretendido para efetuar candidatura.
4. O sistema solicita os dados necessários para efetuar a candidatura a um anúncio (i.e. valor pretendido pela realização da tarefa, o número de dias para realização da tarefa após atribuição da mesma e um texto de apresentação). 
5. O freelancer introduz os dados solicitados.
6. O sistema valida e apresenta todos os dados, pedindo que os confirme. 
7. O freelancer confirma.
8. O sistema regista os dados da candidatura efetuada e informa o freelancer do sucesso da operação.


#### Extensões (ou fluxos alternativos)


*a. O freelancer solicita o cancelamento de candidatura ao anúncio.
> O caso de uso termina.

3a. O sistema deteta que não existem anúncios elegíveis para candidatura por parte do freelancer no sistema.
>   1. O sistema alerta o freelancer que não existem anúncios elegíveis para candidatura.
>
    >   1a. O caso de uso termina.

6a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3 e 5).
>
	>   2a. O freelancer não altera os dados. O caso de uso termina.

6b. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o freelancer para o facto. 
> 2. O sistema permite a sua alteração (passo 3 e 5).
> 
	>   2a. O freelancer não altera os dados. O caso de uso termina.


#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados

\-

#### Frequência de Ocorrência

\-

#### Questões em aberto

* Existem outros dados que são necessários?
* Todos os dados são obrigatórios?
* Existe um número máximo de candidaturas efetuadas?
* Qual a frequência de ocorrência deste caso de uso?
* Qual o formato do texto da apresentação e /ou motivação?
* Quem verifica se o freelancer possui o grau de proficiência mínimo exigido a todas as competência técnicas obrigatórias para o anúncio da tarefa?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC9_MD.svg](UC9_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O freelancer inicia o processo de candidatura.  |  ...interage com o utilizador?	 |  EfetuarCandidaturaUI.           |  Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existente no Modelo de Domínio.  |
|    |  ...coordena o UC?  |  EfetuarCandidaturaController  |  Controller.  |
|    |  ...cria/instancia Candidatura?  |  RegistoCandidatura  |  As responsabilidades da plataforma passarão para o RegistoCandidatura (HC + LC) + Creator com HC + LC: RegistoCandidatura possui Candidatura.  |
|    |  ...cria/instancia Candidatura?  |  RegistoCandidatura  |  HC + LC: o RegistoCandidatura contém/agrega Candidatura  |
|    |   ...possui RegistoCandidatura? | Plataforma | IE: a Plataforma contém/agrega o registo de todas as candidaturas. |
| 2. O sistema apresenta todos os anúncios elegíveis com o GP mínimo às CT obrigatórias para o freelancer e pede para selecionar um.  |  ...conhece os anúncios existentes a listar?  |  RegistoAnuncio  |  HC + LC: O RegistoAnuncio possui/agrega  todos os anúncios elegíveis  |
|    |   ...possui RegistoAnuncio?   |  Plataforma   | IE: a Plataforma contém/agrega o registo de todos os anúncios.  |
| 3. O freelancer seleciona o anúncio pretendido para efetuar candidatura.  |	...guarda o anúncio selecionado?  |  Candidatura  | IE: A Candidatura possui/agrega o anúncio selecionado  |   
| 4. O sistema solicita os dados necessários (i.e. valor pretendido pela realização da tarefa, nº de dias para realização e texto de apresentação). |    |    |    |
| 5. O freelancer introduz os dados solicitados.  |  ...guarda os dados introduzidos?  |  Candidatura  |  Information Expert (IE) - instância criada no passo 1  |
| 6. O sistema valida e apresenta todos os dados ao freelancer, pedindo que os confirme.  |  ...valida os dados da Candidatura (validação local)  |  Candidatura   |  IE: possui os seus próprios dados.  |
|    |  ...valida os dados da Candidatura (validação global)  |  RegistoCandidatura  |  IE: O RegistoCandidatura possui/agrega Candidatura  |
|    |  ...valida os dados da Candidatura (validação global)  |  Anuncio  |  IE: O Anuncio possui/agrega Candidatura  |
| 7. O freelancer confirma.  |     |      |       |
| 8. O sistema regista os dados e informa o freelancer do sucesso da operação.  |  ...guarda a Candidatura criada?  |  RegistoCandidatura   |  HC + LC: o RegistoCandidatura contém/agrega Candidatura.  |
|    |  ...guarda a Candidatura criada?  |  Anuncio   |  HC + LC: o Anuncio contém/agrega Candidatura.  |

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

  * Plataforma
  * Candidatura

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * EfetuarCandidaturaUI
 * EfetuarCandidaturaController
 * Anúncio
 * Candidatura
 * Freelancer
 * RegistoAnuncio
 * RegistoCandidatura
 * RegistoFreelancer
 * RegistoTarefa
 * RegistoCategoriaTarefa

 Outras classes de sistema/componentes externos:
 
 * SessaoUtilizador
 * AplicacaoPOT


###	Diagrama de Sequência

![UC9_SD.svg](UC9_SD.svg)


###	Diagrama de Classes

![UC9_CD.svg](UC9_CD.svg)
